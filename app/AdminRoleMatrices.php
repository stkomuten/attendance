<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRoleMatrices extends Model
{
	protected $table = "admin_role_matrices";

	const ID_ROLE_SYSTEM_ADMINISTRATOR = '1000';
}
