<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendances extends Model
{
	const CREATED_AT = 'reg_date';
	const UPDATED_AT = 'upd_date';

	const DUPLICATE_MINUTE      = 1;
	
	const UPDATE_INTERVAL_MINUTE = 1;
	
	const COLUMN_ATTENDANCE_IN  = "start_date";
	const COLUMN_ATTENDANCE_OUT = "end_date";
	const COLUMN_BREAK_IN       = "start_break_date";
	const COLUMN_BREAK_OUT      = "end_break_date";

	protected $table = 'attendance';


	/**
	 * 管理画面からデータ削除(論理削除)
	 */
	public function execDelete($targetId, $adminId)
	{
		return $this->deleteAttendance($targetId, $adminId);
	}


	/**
	 * 管理画面から保存の実行
	 */
	public function execSave($targetId, $adminId, $empNo, $startDate, $endDate, $targetDate, $divisionId, $paidHoliday, $note)
	{
		$startDate     = empty($startDate) ? null : date("Y-m-d H:i:s", strtotime($startDate));
		$endDate       = empty($endDate)   ? null : date("Y-m-d H:i:s", strtotime($endDate));
		$initStartDate = empty($startDate) ? null : date("Y-m-d H:i:s", strtotime($startDate) - 1);
		$initEndDate   = empty($endDate)   ? null : date("Y-m-d H:i:s", strtotime($endDate) - 1);
		$targetDate    = date("Y-m-d H:i:s", strtotime($targetDate));
		$nowDate    = date("Y-m-d H:i:s", strtotime("now"));

		// 新規登録
		if (!$targetId) {

			if (!empty($startDate)) {
				$regDate = $startDate;
				$updDate = $nowDate;
			} elseif (!empty($endDate)) {
				$regDate = $endDate;
				$updDate = $nowDate;
			} else {
				$regDate = $targetDate;
				$updDate = $nowDate;
			}

			$result = $this->insertAttendance(array(
				'emp_no'            => $empNo
				, 'start_date'      => $startDate
				, 'end_date'        => $endDate
				, 'init_start_date' => $initStartDate
				, 'init_end_date'   => $initEndDate
				, 'division_id'     => $divisionId
				, 'paid_holiday'    => $paidHoliday
				, 'note'            => $note
				, 'reg_emp_no'      => $adminId
				, 'reg_date'        => $regDate
				, 'upd_date'        => $updDate
			));
		// 更新
		} else {
			if (!$attendance = self::find($targetId)) {
				return false;
			}

			$saveData = array(
				'emp_no'       => $empNo
				, 'note'       => $note
				, 'upd_emp_no' => $adminId
				, 'division_id'=> $divisionId
				, 'paid_holiday'=> $paidHoliday
			);

			if (!empty($startDate)) {
				$saveData['start_date'] = $startDate;
			}

			if (!empty($endDate)) {
				$saveData['end_date'] = $endDate;
			}

			if(empty($attendance->init_start_date) && isset($startDate))
			{
				$saveData['init_start_date'] = date("Y-m-d H:i:s", strtotime($startDate) - 1);
			}

			if(empty($attendance->init_end_date) && isset($endDate))
			{
				$saveData['init_end_date'] = date("Y-m-d H:i:s", strtotime($endDate) - 1);
			}
			// 更新
			$result = $this->updateAttendance($targetId, $saveData);
		}

		return $result;
	}



	/**
	 * ユーザーの日別勤務記録を取得
	 */
	public function findDailyUserAttendance($selectStartTimestamp, $selectEndTimestamp, $userId = null)
	{
		$startDate = date('Y-m-d 00:00:00', $selectStartTimestamp);
		$endDate   = date('Y-m-d 23:59:59', $selectEndTimestamp);

		$builder = self::join('employees', 'employees.emp_no', '=', 'attendance.emp_no');
		$builder = $builder->where('attendance.reg_date', '>=', $startDate);
		$builder = $builder->where('attendance.reg_date', '<=', $endDate);
		$builder = $builder->where('attendance.del_flg', 0);

		if (!empty($userId)) {
			$builder = $builder->where('attendance.emp_no', $userId);
		}

		$builder = $builder->groupBy('attendance.reg_date');
		$builder = $builder->select(
			'attendance.id'
			, 'attendance.emp_no'
			, 'attendance.start_date'
			, 'attendance.init_start_date'
			, 'attendance.end_date'
			, 'attendance.init_end_date'
			, 'attendance.reg_date'
			, 'attendance.division_id'
			, 'attendance.paid_holiday'
			, 'attendance.note'
			, 'employees.last_name'
			, 'employees.first_name'
		);

		return $builder->get();
	}


	/**
	 * 指定日範囲の勤務記録を取得
	 */
	public function findDailyAttandanceData($startDate, $endDate, array $empNoList)
	{
		$attendanceList = self::select('id', 'emp_no', 'start_date', 'end_date', 'init_start_date', 'init_end_date', 'division_id', 'paid_holiday', 'note')
			->where('reg_date', '>=', $startDate)
			->where('reg_date', '<=', $endDate)
			->where('del_flg', 0)
			->whereIn('emp_no', $empNoList)
			->get();

		$result = array();
		foreach ($attendanceList as $val) {
			$result[$val->emp_no] = $val;
		}

		return $result;
	}


	/**
	 * 出退勤のどちらか片方だけ登録があるデータを取得
	 */
	public function findOneOfAPairRegisterAttendance($startDate, $endDate, array $empNoList = array())
	{
	        $builder = self::where('del_flg', 0);
		
		$date = date("Y-m-d", strtotime($startDate));
		
		$start   = date("Y-m-d 00:00:00", strtotime($date));
		$end     = date("Y-m-d 23:59:59", strtotime($date));


	        if (count($empNoList) > 0) {
	        	$builder = $builder->whereIn('emp_no', $empNoList);
	        }
		\Log::debug('出退勤のどちらか片方だけ:'. $date);
		$result = \DB::select("select * from `attendance` where (`reg_date` BETWEEN ? AND ?) AND (`start_date` is Null OR `end_date` is Null)",[$start, $end]);


	    return $result;
	}


	/**
	 * 本日のデータを取得
	 */
	public function findTodayData($empNo, $date)
	{
		$column  = self::CREATED_AT;
		$date    = empty($date) ? date("Y-m-d H:i:s") : $date;
		$start   = date("Y-m-d 00:00:00", strtotime($date));
		$end     = date("Y-m-d 23:59:59", strtotime($date));

		//$result = \DB::select("select * from `attendance` where (`reg_date` BETWEEN ? AND ?) AND `emp_no` = ? AND `del_flg` = 0;", [$start, $end, $empNo])->first();
		
		// その日のデータしかないはずなので、firstで
		$result = self::where('emp_no', $empNo)
			->where('del_flg', 0)
			->whereNotNull($column)
			->where($column, '>=', $start)
			->where($column, '<=', $end)
			->first();
		
		return $result;
	}


	/**
	 *	最新のデータを取得
	 */
	public function findLatestData($empNo)
	{
		// その日のデータしかないはずなので、firstで
		$tomorrow = date("Y-m-d", strtotime("+1 day"));
		//
		$result = self::where('emp_no', $empNo)
			->where('del_flg', 0)
			->where('reg_date', '<', $tomorrow)
			->orderBy('reg_date', 'DESC')
			->first();
		//
		return $result;
	}


	/**
	 * 本日すでに出勤データがあるか
	 */
	public function isExistsTodayDataAttendanceIn($employeeId, $date)
	{
		return $this->isExistsTodayData($employeeId, $date, self::COLUMN_ATTENDANCE_IN);
	}


	/**
	 * 本日すでに退勤データがあるか
	 */
	public function isExistsTodayDataAttendanceOut($employeeId, $date)
	{
		return $this->isExistsTodayData($employeeId, $date, self::COLUMN_ATTENDANCE_OUT);
	}


	/**
	 * 本日すでに休憩入データがあるか
	 */
	public function isExistsTodayDataBreakIn($employeeId, $date)
	{
		return $this->isExistsTodayData($employeeId, $date, self::COLUMN_BREAK_IN);
	}


	/**
	 * 本日すでに休憩戻データがあるか
	 */
	public function isExistsTodayDataBreakOut($employeeId, $date)
	{
		return $this->isExistsTodayData($employeeId, $date, self::COLUMN_BREAK_OUT);
	}


	/**
	 * 出勤データ保存
	 */
	public function saveAttendanceIn($employeeId, $startDate, $location = null)
	{
		$startDate = empty($startDate) ? date("Y-m-d H:i:s") : $startDate;
		/*
		return $this->insertAttendance([
			'emp_no'            => $employeeId
			, 'start_date'      => $startDate
			, 'init_start_date' => $startDate
			, 'reg_emp_no'      => $employeeId
			, 'start_location'    => $location
		]);
		*/
		// 本日のattendanceレコードを取得
		$todayData = $this->findTodayData($employeeId, $startDate);
		//
		$employee = Employees::find($employeeId);

		// 基本は本日のattendanceレコードを使い、アップデートを行う
		$saveType   = 'update';
		$saveObject = $todayData;

		if (empty($todayData))
		{
			// 本日のデーがなかった場合
			$saveType = 'insert';
		}

		switch ($saveType) {
			// 新規作成
			case 'insert':
				
				$result = $this->insertAttendance([
					'emp_no'		=> $employeeId
					, 'start_date'		=> $startDate
					, 'init_start_date'	=> $startDate
					, 'reg_emp_no'		=> $employeeId
					, 'start_location'	=> $location
					, 'division_id'		=> $employee->default_division_id
				]);
				
				break;

			// 更新
			case 'update':
				
				$saveData = array('start_date' => $startDate);
				
				if (isset($location))
				{
					$saveData['start_location'] = $location;
				}
				
				if (empty($saveObject->init_start_date)) {
					$saveData['init_start_date'] = $startDate;
				}

				$result = $this->updateAttendance($saveObject->id, $saveData);
				
				break;

			default:
				$result = false;
		}

		return $result;
	}


	/**
	 *	直近の勤怠データの検証
	 *
	 *	退勤記録が空で、出退記録が48時間以内
	 */
	public function validateLatestData($data)
	{
		$result = false;

		if (empty($data)) {
			return $result;
		}

		if(empty($data->end_date) && isset($data->start_date))
		{
			if(strtotime($data->start_date) >= strtotime("-2 day"))
			{
				$result = true;
			}
		}

		return $result;
	}


	/**
	 * 退勤データ保存
	 */
	public function saveAttendanceOut($employeeId, $endDate, $location)
	{
		$endDate   = empty($endDate) ? date("Y-m-d H:i:s") : $endDate;

		// 本日のattendanceレコードを取得
		$todayData = $this->findTodayData($employeeId, $endDate);

		// 直近のattendanceレコードを取得
		$latestData = $this->findLatestData($employeeId);
		//
		$employee = Employees::find($employeeId);
		// 基本は本日のattendanceレコードを使い、アップデートを行う
		$saveType   = 'update';
		$saveObject = $todayData;

		if ($this->validateLatestData($latestData)) {
			// 直近のデータに退勤記録が空で48時間以内のものがある場合
			$saveObject = $latestData;
		} elseif (empty($todayData)) {
			// 本日のデーがなかった場合
			$saveType = 'insert';
		}

		switch ($saveType) {
			// 新規作成
			case 'insert':
				$result = $this->insertAttendance([
					'emp_no'		=> $employeeId
					, 'end_date'		=> $endDate
					, 'init_end_date'	=> $endDate
					, 'reg_emp_no'		=> $employeeId
					, 'end_location'	=> $location
					, 'division_id'		=> $employee->default_division_id
				]);
				break;

			// 更新
			case 'update':
				$saveData = array('end_date' => $endDate, 'end_location' => $location);
				if (empty($saveObject->init_end_date)) {
					$saveData['init_end_date'] = $endDate;
				}

				$result = $this->updateAttendance($saveObject->id, $saveData);
				break;
			default:
				$result = false;
		}

		return $result;
	}


	/**
	 * 休憩入データ保存
	 */
	public function saveBreakIn($employeeId, $startBreakDate)
	{
		$startBreakDate = empty($startBreakDate) ? date("Y-m-d H:i:s") : $startBreakDate;
		$todayData = $this->findTodayData($employeeId, $startBreakDate);

		// 本日のデータがない場合は登録しない
		if (empty($todayData)) {
			throw new Exception("TODAY_DATA_UNREGISTERED", 200);
		}

		// 退勤データが登録されている場合は、退勤しているとみなして休憩データ入れられない
		if (!empty($todayData->end_date)) {
			throw new Exception("END_DATE_REGISTERED", 200);
		}

		// データ更新
		return $this->updateAttendance($todayData->id, array(
			'start_break_date' => $startBreakDate
		));
	}


	/**
	 * 休憩戻データ保存
	 */
	public function saveBreakOut($employeeId, $endBreakDate)
	{
		$endBreakDate = empty($endBreakDate) ? date("Y-m-d H:i:s") : $endBreakDate;
		$todayData = $this->findTodayData($employeeId, $endBreakDate);

		// 本日のデータがない場合は登録しない
		if (empty($todayData)) {
			throw new Exception("TODAY_DATA_UNREGISTERED", 200);
		}

		// 退勤データが登録されている場合は、退勤しているとみなして休憩データ入れられない
		if (!empty($todayData->end_date)) {
			throw new Exception("END_DATE_REGISTERED", 200);
		}

		// データ更新
		return $this->updateAttendance($todayData->id, array(
			'end_break_date' => $endBreakDate
		));
	}


	/**
	 * 重複時間内にデータがあるか
	 */
	public function isDuplicateData($employeeId, $date)
	{
		//$date      = empty($date) ? date("Y-m-d H:i:s") : $date;
		$date      = empty($date) ? date("Y-m-d") : date("Y-m-d", strtotime($date));
		$nowTime   = strtotime($date);

		$checkTime = date("Y-m-d H:i:s", $nowTime - 60 * self::DUPLICATE_MINUTE);
		
	
		//	登録日が存在するか
		//$count     = self::where('emp_no', $employeeId)->where(self::CREATED_AT, '>=', $checkTime)->count();
		$dateInfo = self::where('emp_no', $employeeId)
					->where("del_flg", "=", 0)
					->whereBetween("reg_date", array($date . " 00:00:00", $date . " 23:59:59"))
					->first();
					
		$result = false;
		
		if(isset($dateInfo))
		{
			if(strtotime($dateInfo->upd_date) >= strtotime("-" . self::DUPLICATE_MINUTE . "minutes"))
			{
				$result = true;
			}	
			
		}
		
		return $result;

		//return (bool)$count;
	}


	/**
	 * 時間内にデータ更新があるか
	 */
	public function isInUpdateInterval($employeeId)
	{	
		$tomorrow = date("Y-m-d", strtotime("+1 day"));
	
		$dateInfo = self::where('emp_no', $employeeId)
					->where("del_flg", "=", 0)
					->where('reg_date', '<', $tomorrow)
					->orderBy('upd_date', 'desc')
					->first();
					
		$result = false;
		
		if(isset($dateInfo))
		{	
			if(strtotime($dateInfo->upd_date) > strtotime("-" . self::UPDATE_INTERVAL_MINUTE . "minutes"))
			{
				$result = true;
			}	
			
		}
		return $result;
	}



	/**
	 * 今日中のデータが有るか
	 */
	private function isExistsTodayData($empNo, $date, $column)
	{
		if (!$todayData = $this->findTodayData($empNo, $date))
		{
			return false;
		}

		return (strlen($todayData->$column) >= 1);
	}


	/**
	 * 勤怠情報登録
	 */
	private function insertAttendance(array $saveData)
	{
		$attendances = new Attendances();

		foreach ($saveData as $key => $val) {
			$attendances->$key = $val;
		}

		return $attendances->save();
	}


	/**
	 * 勤怠情報更新
	 */
	private function updateAttendance($targetId, array $saveData)
	{
		if (!$attendance = self::find($targetId)) {
			return false;
		}

		foreach ($saveData as $key => $val) {
			$attendance->$key = $val;
		}

		return $attendance->save();
	}


	/**
	 * 勤怠情報削除
	 */
	private function deleteAttendance($targetId, $adminId)
	{
		if (!$attendance = self::find($targetId)) {
			return false;
		}

		$attendance->del_date   = date("Y-m-d H:i:s");
		$attendance->del_emp_no = $adminId;
		$attendance->del_flg    = true;

		return $attendance->save();
	}
}
