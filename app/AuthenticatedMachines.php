<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthenticatedMachines extends Model
{
    protected $table = 'authenticated_machines';
}
