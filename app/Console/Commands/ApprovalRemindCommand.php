<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use App\Employees;
use App\AttendanceApproval;
use App\Departments;
use App\DepartmentRelations;

class ApprovalRemindCommand extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'approval
                            {--simulateDate=}
                            {--dry-run}';

    /**
     * The console command description.
     */
    protected $description = '月初から数えて2営業日目に、先月分の未承認の社員が存在する場合、その部長・上長と管理部にメールを送る';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        if (!$this->isExecuteDay()) {
            echo "not working";
            return false;
        }

        $targets = $this->getSendTargetUsers();

        // テスト実行
        if ($this->option('dry-run')) {
            $this->execDryRun($targets);
            exit;
        }

        $this->execSendMail($targets);
    }


    /**
     * メール送信
     */
    public function execSendMail(array $targets)
    {
        foreach ($targets as $val) {
            \Mail::plain('email.remind', ['attach_str' => $val['attach_str']], function($message) use($val) {
                $message->from('attendance@starttoday.jp', 'STK ATTENDANCE SYSTEM');
                $message->subject('【勤怠（未承認）】');
                $message->to($val['to']);

                if (isset($val['cc'])) {
                    foreach ($val['cc'] as $mail) {
                        $message->cc($mail);
                    }
                }
            });
        }
    }


    /**
     * テスト実行
     */
    private function execDryRun(array $targets)
    {
        $this->info('start dry run!');
        $this->line("===================================");

        $count = 1;
        $max   = count($targets);
        foreach ($targets as $val) {

            foreach ($val['to'] as $empNo => $mail) {
                $this->comment('to: ' . $mail);
            }

            if (isset($val['cc'])) {
                foreach ($val['cc'] as $mail) {
                    $this->comment('cc: ' . $mail);
                }
            }

            $this->comment("[target-user] \n" . str_replace("<br />", "\n", $val['attach_str']));

            if ($count != $max) {
                $this->line("------------------------------");
            }

            $count++;
        }

        $this->line("===================================");
        $this->info('end dry run...');
    }


    /**
     * 送信対象のユーザーを取得
     */
    private function getSendTargetUsers()
    {
        $employees = new Employees;
        $empList   = $employees->findAvailableUserList();
        $dateKey   = date("Ym", strtotime("-1 month"));

        if ($simulateDate = $this->getSimulateDate()) {
            $simulateDate = date("Y-m-1", strtotime($simulateDate));
            $dateKey      = date("Ym", strtotime($simulateDate . "-1 month"));
        }

        $approvals = AttendanceApproval::findListData($dateKey);

        // 勤怠記録をつけないユーザーの場合はメールを送信しない
		/*
        foreach ($empList as $empNo => $list) {
            if ($list->attendance_flg != 1) {
                unset($empList[$empNo]);
            }
        }*/

        // すでに承認済みの場合はメールしない
        foreach ($approvals as $approval) {
            if ($approval->status == 0 || $approval->status == 2) {
                // 未承認または、承認取り消しの場合はスルー
                continue;
            }

            if (isset($empList[$approval->emp_no])) {
                unset($empList[$approval->emp_no]);
            }
        }

        // 所属した部署情報
        $departments = DepartmentRelations::whereIn('emp_no', array_keys($empList))->get();
        foreach ($departments as $department) {
            $empList[$department->emp_no]->department_id = $department->department_id;
            $empList[$department->emp_no]->position_id = $department->position_id;
        }

        // 上長以上の人たちを取得
        $managerList = [];
		
        foreach ($empList as $empNo => $list)
		{
            if ($list->position_id === DepartmentRelations::ID_POSITION_NORMAL)
			{
                continue;
            }

            $managerList[$list->department_id][$list->emp_no] = $list->mail;
        }

        $targets = [];
        foreach ($empList as $empNo => $list) {
			 // 勤怠記録をつけないユーザーの場合はメールを送信しない
			if ($list->attendance_flg != 1) {
                continue;
            }
			
			
            // 上司がいない場合はなにもしない
            if (!isset($managerList[$list->department_id])) {
                continue;
            }

            // 初期設定
            if (!isset($targets[$list->department_id]['attach_str'])) {
                $targets[$list->department_id]['attach_str'] = "";
            }

            $targets[$list->department_id]['attach_str'] .= $list->last_name . " " . $list->first_name . "<br />";
            $targets[$list->department_id]['to']          = $managerList[$list->department_id];
        }

        // 管理部の情報を取得
        $managementTeams = $employees->findManagementTeams();
        foreach ($managementTeams as $val) {
            foreach ($targets as $departmentId => $target) {
                $targets[$departmentId]['cc'][$val->emp_no] = $val->mail;

                // 管理部の場合は、toの対象が管理部の人かどうかを調べて、ccから抜けるようにする
                if ($departmentId === Departments::ID_DEPARTMENT_MANAGEMENT) {
                    if (isset($target['to'][$val->emp_no])) {
                        unset($targets[$departmentId]['cc'][$val->emp_no]);
                    }
                }
            }
        }

        return $targets;
    }


    /**
     * このバッチを実行していいかどうか
     */
    private function isExecuteDay()
    {
        $simulateDate = $this->getSimulateDate();
        $year   = ($simulateDate) ? date("Y", strtotime($simulateDate))         : date("Y");
        $month  = ($simulateDate) ? date("m", strtotime($simulateDate))         : date("m");
        $day    = ($simulateDate) ? intval(date("j", strtotime($simulateDate))) : intval(date("j"));
        $result = false;
        $count  = 0;

        for ($i = 1; $i <= $day; $i++) {
            $timestamp = mktime(0, 0, 0, $month, $i, $year);

            // 休日・祝日であれば次のループ
            if ($this->isHoliday($timestamp)) {
                continue;
            }

            $count++;

            // カウントが2であれば、2営業日と判断
            if ($count === 2 && $i === $day) {
                $result = true;
            }
        }

        return $result;
    }


    /**
     * 土日、祝日だったかどうか
     */
    private function isHoliday($timestamp = null)
    {
        $timestamp = is_null($timestamp) ? time() : $timestamp;
        $result    = in_array(date('w', $timestamp), array(0, 6));

        if (!$result) {
            // 土日ではない場合、祝日かどうかを調べる
            $result = \Holiday::isHoliday($timestamp);
        }

        return $result;
    }


    /**
     * 偽装した日付を取得
     */
    private function getSimulateDate()
    {
        $simulateDate = $this->option('simulateDate');

        if(!strptime($simulateDate, '%Y-%m-%d')) {
            return false;
        }

        return $simulateDate;
    }
}