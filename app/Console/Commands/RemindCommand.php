<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Attendances;
use App\Employees;
use App\DepartmentRelations;
use App\PaidHolidays;
use League\Csv\Reader;

class RemindCommand extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'remind
                            {--simulateDate=}
                            {--dry-run}';

    /**
     * The console command description.
     */
    protected $description = '前日の勤怠未記入リマインドバッチ(月〜金の「平日」に実行)';


    private $employees;
    private $attendances;
    private $vacationData = null;


    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->employees   = new Employees;
        $this->attendances = new Attendances;
        $this->departmentRelations = new DepartmentRelations;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($this->isHoliday()) {
            // 実行当日が休日の場合は実行しない
            return ;
        }

        $yesterday = strtotime("-1 day");
        if ($simulateDate = $this->getSimulateDate()) {
            $yesterday = strtotime($simulateDate);
        }

        if ($this->isHoliday($yesterday)) {
            // 前日が休日だった場合
            $targets = $this->getMultipleRemindData($yesterday);

        } else {
            // 前日が平日だった場合
            $targets = $this->getSingleRemindData($yesterday);
        }

        // 自分の上司のメールを取得
		if($targets)
		{
		
			foreach ($targets as $empNo => $val) {
	
				// 指定の期間の間
				/*if (time() < strtotime('2016-02-01 00:00:00')) {
	
					// システム部の場合はCCはたくさんと西さんに絞る
					if ($relations = $this->departmentRelations->isSystemTeam($empNo)) {
						$highOfficerMailList = array(
							"takumi.ito@starttoday.jp"
							, "hiroaki.nishi@starttoday.jp"
						);
	
						// たくさん・西さんの場合は二重に送らないように細工する
						foreach ($highOfficerMailList as $_mail) {
							if ($_mail !== $val['mail']) {
								$targets[$empNo]['cc'][] = $_mail;
							}
						}
					} else {
						// 自分の上官の情報を取得
						if ($officer = $this->employees->findMyHighOfficerData($empNo)) {
							$targets[$empNo]['cc'][] = $officer->mail;
						}
					}
	
				} else {*/
					// 自分の上司の情報を取得
					if ($officer = $this->employees->findMyHighOfficerData($empNo)) {
						$targets[$empNo]['cc'][] = $officer->mail;
					}
				//}
			}
	
        	$this->execSendMail($targets);
		}
    }


    /**
     * 1日分のリマインドを飛ばす対象データを取得
     */
    public function getSingleRemindData($targetDay)
    {
        $targets    = array();
        $targetDate = date('Y年m月d日', $targetDay);
        $startDate  = date('Y-m-d 00:00:00', $targetDay);
        $endDate    = date('Y-m-d 23:59:59', $targetDay);

        if (!$users = $this->findRemindTargetUsers($targetDay)) {
            // 送信対象がいない
            return ;
        }

        // 指定日にどちらか片方しか登録されていない勤怠データを取得
        $attendances = $this->attendances->findOneOfAPairRegisterAttendance($startDate, $endDate, array_keys($users));
        foreach ($attendances as $val)
	{
		
		if(isset($users[$val->emp_no]))	
		{
			$targets[$val->emp_no]['last_name']  = $users[$val->emp_no]->last_name;
            		$targets[$val->emp_no]['name']       = $users[$val->emp_no]->last_name . " " . $users[$val->emp_no]->first_name;
            		$targets[$val->emp_no]['mail']       = $users[$val->emp_no]->mail;
            		$targets[$val->emp_no]['attach_str'] = $this->getRemindTargetString($targetDate, $val->start_date, $val->end_date);
		}
	}

        // 指定日にどちらも登録されていないユーザーデータを取得
        $employees = $this->employees->findUnRegisterAttendanceUsers($startDate, $endDate, array_keys($users));
        foreach ($employees as $val) {

            $targets[$val->emp_no]['last_name']  = $users[$val->emp_no]->last_name;
            $targets[$val->emp_no]['name']       = $users[$val->emp_no]->last_name . " " . $users[$val->emp_no]->first_name;
            $targets[$val->emp_no]['mail']       = $users[$val->emp_no]->mail;
            $targets[$val->emp_no]['attach_str'] = $this->getRemindTargetString($targetDate, null, null);
        }

        return $targets;
    }


    /**
     * 複数日分のリマインドを飛ばす対象データを取得
     */
    public function getMultipleRemindData($targetDay)
    {
        $targets     = array();
        $_targetDay  = $this->getMultipleRemindStartDate($targetDay);
        $range       = ($targetDay - $_targetDay) / (60 * 60 * 24);

        for ($i = 0; $i <= $range; $i++) {
            $targetDay  = $_targetDay + (60 * 60 * 24 * $i);
            $targetDate = date('Y年m月d日', $targetDay);
            $startDate  = date('Y-m-d 00:00:00', $targetDay);
            $endDate    = date('Y-m-d 23:59:59', $targetDay);

            if ($this->isHoliday($targetDay)) {
                // 休日の場合

                if (!$users = $this->findRemindTargetUsers($targetDay)) {
                    // 送信対象がいない
                    continue ;
                }

                // 指定日にどちらか片方しか登録されていない勤怠データを取得
                $attendances = $this->attendances->findOneOfAPairRegisterAttendance($startDate, $endDate, array_keys($users));
                $_targets    = array();
                foreach ($attendances as $val) {
                    $_targets[$val->emp_no]['last_name']  = $users[$val->emp_no]->last_name;
                    $_targets[$val->emp_no]['name']       = $users[$val->emp_no]->last_name . " " . $users[$val->emp_no]->first_name;
                    $_targets[$val->emp_no]['mail']       = $users[$val->emp_no]->mail;
                    $_targets[$val->emp_no]['attach_str'] = $this->getRemindTargetString($targetDate, $val->start_date, $val->end_date);
                }

            } else {
                // 最初の日は、金曜や祝日の前日など平日になる
                $_targets = $this->getSingleRemindData($targetDay);
            }

            foreach ($_targets as $empNo => $val) {
                $targets[$empNo]['last_name'] = $val['last_name'];
                $targets[$empNo]['name']      = $val['name'];
                $targets[$empNo]['mail']      = $val['mail'];

                // 追加文章
                if (!isset($targets[$empNo]['attach_str'])) {
                    $targets[$empNo]['attach_str'] = "";
                } else {
                    $targets[$empNo]['attach_str'] .= "<br />";
                }

                $targets[$empNo]['attach_str'] .= $val['attach_str'];
            }
        }

        return $targets;
    }


    /**
     * メール送信実行
     */
    private function execSendMail(array $targets)
    {
        // テスト実行
        if ($this->option('dry-run')) {
            $this->execDryRun($targets);
            exit;
        }

        foreach ($targets as $val) {
            \Mail::plain('email.remind', ['attach_str' => $val['attach_str']], function($message) use($val) {

                $message->from('attendance@starttoday.jp', 'STK ATTENDANCE SYSTEM');
                $message->subject('【勤怠（未記録）】' . $val['last_name']);
                $message->to($val['mail'], $val['name']);

                if (isset($val['cc'])) {
                    foreach ($val['cc'] as $mail) {
                        $message->cc($mail);
                    }
                }
            });
        }
    }


    /**
     * テスト実行
     */
    private function execDryRun(array $targets)
    {
        $this->info('start dry run!');
        $this->line("===================================");

        $count = 1;
        $max   = count($targets);
        foreach ($targets as $empNo => $val) {
            $this->comment('emp_no: ' . $empNo);
            $this->comment('name: '   . $val['name']);
            $this->comment('to: '     . $val['mail']);
            $this->comment('detail: ' . $val['attach_str']);

            if (isset($val['cc'])) {
                foreach ($val['cc'] as $mail) {
                    $this->comment('cc: ' . $mail);
                }
            }

            if ($count != $max) {
                $this->line("------------------------------");
            }

            $count++;
        }

        $this->line("===================================");
        $this->info('end dry run...');
    }


    /**
     * 土日、祝日だったかどうか
     */
    private function isHoliday($timestamp = null)
    {
        $timestamp = is_null($timestamp) ? time() : $timestamp;
        $result    = in_array(date('w', $timestamp), array(0, 6));

        if (!$result) {
            // 土日ではない場合、祝日かどうかを調べる
            $result = \Holiday::isHoliday($timestamp);
        }

        return $result;
    }


    /**
     * 前日が休暇だったかどうか
     */
    private function isVacation($targetEmpNo, $yesterday)
    {
        $targetDate = date('Y-m-d', $yesterday);
	
	$isFindPaidHoliday = false;
	
	$attandanceInfo = $this->attendances->findTodayData($targetEmpNo, $targetDate);
	
	if(isset($attandanceInfo) && isset($attandanceInfo->paid_holiday))
	{
		$paidHolidays = unserialize($attandanceInfo->paid_holiday);
		
		for($i=0;$i<count($paidHolidays);$i++)
		{
			$phID = $paidHolidays[$i];	
			if(
				$phID == PaidHolidays::PAID_HOLIDAY_WHOLE ||
				$phID == PaidHolidays::PAID_HOLIDAY_SUMMER ||
				$phID == PaidHolidays::PAID_HOLIDAY_WINTER ||
				$phID == PaidHolidays::PAID_HOLIDAY_CELEBRATION_OR_CONDOLENCE ||
				$phID == PaidHolidays::PAID_HOLIDAY_COMPENSATORY ||
				$phID == PaidHolidays::PAID_HOLIDAY_SUBSTITUTE ||
				$phID == PaidHolidays::PAID_HOLIDAY_ABSENCE ||
				$phID == PaidHolidays::PAID_HOLIDAY_NURSING ||
				$phID == PaidHolidays::PAID_HOLIDAY_CARE ||
				$phID == PaidHolidays::PAID_HOLIDAY_PHYSIOLOGY
			){
				
				$isFindPaidHoliday = true;
				break;
				\Log::debug('有給休暇取得している');
			}
		}
	}
	
	return $isFindPaidHoliday;
	/*
        if (is_null($this->vacationData)) {
            $filename = 'holiday_' . date('Ym', $yesterday) . '.csv';
            $filepath = storage_path() . '/files/holiday/' . $filename;

            // 休暇データがない場合はfalse
            if (!file_exists($filepath)) {
                return false;
            }

            $reader = Reader::createFromPath($filepath, 'r');
            foreach ($reader->fetchAll() as $key => $val) {
                if ($key === 0) {
                    continue;
                }

                list($date, $empNo, $vacationId) = $val;
                if ($date !== $targetDate) {
                    continue;
                }

                $this->vacationData[$empNo] = $vacationId;
            }
        }
        // ターゲットがその日休暇ではない
        if (!isset($this->vacationData[$targetEmpNo])) {
            return false;
        }

        // 本来はなんか処理をするけど、今のところはとりあえず休暇対象だったらOKという形
        // $vacationId   = $this->vacationData[$targetEmpNo];
        // $vacationType = \Config::get('app.vacationType');

        return true;
	*/
    }

	/**
	* 指定日が休暇ではないユーザー一覧を取得
	*/
	private function findRemindTargetUsers($yesterday)
	{
		$users = $this->employees->findAvailableUserList();
		foreach ($users as $empNo => $user) {
		
		//for ($i = count($users) - 1; $i > -1; $i--){

			//$empNo = $users[$i];
			//
			
			//
			if ($this->isVacation($empNo, $yesterday))
			{
				// 前日が休暇だった場合は配列から削除
				unset($users[$empNo]);
				continue;
			}
			//
			if ($user->emp_type != 1)
			{
				// 正社員以外はメールを送信しない
				unset($users[$empNo]);
				continue;
			}
			//
			if ($user->attendance_flg != 1)
			{
				// 勤怠記録をつけないユーザーの場合はメールを送信しない
				unset($users[$empNo]);
				continue;
			}
			
			if ($user->send_mail_flg != 1)
			{
				//	ユーザーの場合はメールを送信しない
				unset($users[$empNo]);
				continue;
			}
		}
			
		return empty($users) ? false : $users;
	}


    /**
     * リマインドバッチに追加する文字列を返す
     */
    private function getRemindTargetString($targetDate, $startDate, $endDate)
    {
        $str = "";

        if (empty($startDate)) {
            $str .= "出勤";
        }

        if (empty($endDate)) {

            if (strlen($str)) {
                $str .= " + ";
            }

            $str .= "退勤";
        }

        $targetDate .= ":   " . $str;

        return $targetDate;
    }


    /**
     * 偽装した日付を取得
     */
    private function getSimulateDate()
    {
        $simulateDate = $this->option('simulateDate');

        if(!strptime($simulateDate, '%Y-%m-%d')) {
            return false;
        }

        return $simulateDate;
    }


    /**
     * 複数リマインドの起算日を取得
     */
    private function getMultipleRemindStartDate($targetDay)
    {
        if (!$this->isHoliday($targetDay)) {
            return $targetDay;
        }

        return $this->getMultipleRemindStartDate($targetDay - 86400);
    }
}
