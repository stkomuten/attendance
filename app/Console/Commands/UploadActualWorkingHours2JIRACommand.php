<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Csv\Reader;

use App\Attendances;
use App\Employees;
use App\DepartmentRelations;
use App\Lib\AMS2JIRAUtility;
use App\Lib\CurlUtility;
use App\Lib\TempoAPI;
use App\PaidHolidays;
use App\TimeSheet;

class UploadActualWorkingHours2JIRACommand extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'upload2JIRA
                            {--v}';

    /**
     * The console command description.
     */
    protected $description = '前日の勤怠リマインドバッチ(月〜金の「平日」に実行)';


    private $employees;


    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->employees   = new Employees;
        $this->attendances = new Attendances;
        $this->departmentRelations = new DepartmentRelations;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $yesterday = strtotime("-1 day");
       
        if ($this->isHoliday($yesterday)) {
            //	前日が休日だった場合実行しない
			\Log::debug('前日が休日だった場合');
           return;
        }else{
			//	それ以外は実行
			\Log::debug('JIRAへアップロード実行');
			return $this->upload2JIRA(TempoAPI::PROD_ENV, $this->option('v') ? true : false);
        }
    }
	//================================================================================
	//
	//	アップロード実行
	//
	//================================================================================
	public function upload2JIRA($env, $isLog = false)
	{
		$employees = new Employees;
		$empList  = $employees->findUserListWithoutObservationUser();
		
		$ams2JIRA = new AMS2JIRAUtility();
		
		$timeSheet	= TimeSheet::all();
		
		//	昨日を取得		
		$yesterday = date('Y-m-d', strtotime('-1 day'));
		
		$period = TempoAPI::getStart2End($yesterday);
		
		TempoAPI::getJSESSIONID($env);
		$curls = [];
		
		if($isLog === true) $logs = [];
		
		foreach($empList as $empInfo)
		{
			$emp_no = $empInfo->emp_no;
			//
			$name = substr($empInfo->mail, 0, strpos($empInfo->mail, "@"));
			//
			$curlsInfo = $ams2JIRA->getCURLsByCompareExistPlans($env, $emp_no, $name, $empInfo->default_division_id, $period["start"], $period["end"], $timeSheet, true);
			//
			$curls = array_merge($curls, $curlsInfo["curls"]); 
			//
			if($isLog === true)
			{
				array_unshift($curlsInfo["logs"], ("\n■■■■■■■■■■ ID:". $emp_no . "	名前:" . $empInfo->last_name . " " . $empInfo->first_name));
				//
				$logs = array_merge($logs, $curlsInfo["logs"]); 
				//
			}
		}
		
		if(count($curls) > 0)
		{
			$result = CurlUtility::asyncExecCurls($curls);		
		}
		
		if($isLog === true) \Log::debug("\n" . implode("\n", $logs));
		\Log::debug("curls count:". count($curls));
		
		return $isLog === true ? implode("\n", $logs) : "complete";
		//
	}





    /**
     * テスト実行
     */
    private function execDryRun(array $targets)
    {
        $this->info('start dry run!');
        $this->line("===================================");

        $count = 1;
        $max   = count($targets);
        foreach ($targets as $empNo => $val) {
            $this->comment('emp_no: ' . $empNo);
            $this->comment('name: '   . $val['name']);
            $this->comment('to: '     . $val['mail']);
            $this->comment('detail: ' . $val['attach_str']);

            if (isset($val['cc'])) {
                foreach ($val['cc'] as $mail) {
                    $this->comment('cc: ' . $mail);
                }
            }

            if ($count != $max) {
                $this->line("------------------------------");
            }

            $count++;
        }

        $this->line("===================================");
        $this->info('end dry run...');
    }


    /**
     * 土日、祝日だったかどうか
     */
    private function isHoliday($timestamp = null)
    {
        $timestamp = is_null($timestamp) ? time() : $timestamp;
        $result    = in_array(date('w', $timestamp), array(0, 6));

        if (!$result) {
            // 土日ではない場合、祝日かどうかを調べる
            $result = \Holiday::isHoliday($timestamp);
        }

        return $result;
    }

}
