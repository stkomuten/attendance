<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    const UPDATED_AT    = null;
    const ID_DEPARTMENT_SYSTEM     = 1;
    const ID_DEPARTMENT_MANAGEMENT = 3;
	const ID_DEPARTMENT_MANAGER = 21;

    protected $table = "departments";
	protected $fillable = ["id", "name", "created_at"];
	
	/**
	 * 部署IDを指定して複数取得
	 */
	public static function allOrderByName()
	{
		return Departments::orderBy("name","desc")->get();
	}	
}


