<?php

namespace App;

use App\Departments;
use App\DepartmentRelations;
use App\AdminRoleMatrices;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Employees extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

	const CREATED_AT = "reg_date";
	const UPDATED_AT = "upd_date";

	protected $primaryKey  = "emp_no";
	protected $table       = "employees";
	protected $fillable    = ["emp_no", "last_name", "first_name", "felica_id", "password", "mail", "emp_type", "status", "attendance_flg", "send_mail_flg", "default_division_id'", "reg_date", "resign_date", "upd_date", "del_date","del_flg"];
    protected $hidden      = ['password', 'remember_token'];

    private $adminRole    = null;
    private $departmentId = null;
    private $positionId   = null;


    /**
     * 自分の部署情報をセット
     */
    public function setMyDepartmentInfo($departmentId, $positionId)
    {
        $this->departmentId = $departmentId;
        $this->positionId   = $positionId;
    }


    /**
     * 自分の管理画面での権限をセット
     */
    public function addAdminRole($adminRoleId)
    {
        $this->adminRole = $adminRoleId;
    }


    /**
     * 自分の部署IDを取得
     */
    public function getMyDepartmentId()
    {
        return $this->departmentId;
    }


    /**
     * 自分の部署のポジションを取得
     */
    public function getMyPositionId()
    {
        return $this->positionId;
    }


    /**
     * 自分の管理画面の権限を取得
     */
    public function getMyAdminRole()
    {
        return $this->adminRole;
    }


    /**
     * システム管理者かどうか
     */
    public function haveSystemAdministratorRole()
    {
        return ($this->adminRole === AdminRoleMatrices::ID_ROLE_SYSTEM_ADMINISTRATOR);
    }


    /**
     * 管理部かどうか
     */
    public function haveManagimentTeamRole()
    {
        return ($this->adminRole === Departments::ID_DEPARTMENT_MANAGEMENT);
    }


    /**
     * 一般社員かどうか
     */
    public function haveNormalRole()
    {
        return ($this->positionId === DepartmentRelations::ID_POSITION_NORMAL);
    }
	
    /**
     * 部長グループかどうか
     */
    public function belongDeptManager()
    {
        return ($this->departmentId === Departments::ID_DEPARTMENT_MANAGER);
    }

	/**
	 * 最新のデータを1件取得
	 */
	public function findOneNewest()
	{
		return self::join('employment_status', 'employment_status.id', '=', 'employees.status')
		->join('employment_type', 'employment_type.id', '=', 'employees.emp_type')
		->select('employees.*', 'employment_status.name as status_name', 'employment_type.name as type_name')
		->orderBy('employees.reg_date', 'DESC')
		->first();
	}


	/**
	 * 指定の1件を取得
	 */
	public function findOneByPK($id)
	{
		return self::join('employment_status', 'employment_status.id', '=', 'employees.status')
		->join('employment_type', 'employment_type.id', '=', 'employees.emp_type')
		->select('employees.*', 'employment_status.name as status_name', 'employment_type.name as type_name')
		->where('employees.emp_no', '=', $id)
		->first();
	}


	
	//================================================================================
	//
	//	名前のリスト取得 外部管理者の除外
	//
	//================================================================================	
	public function findListIdAndNameWithoutObservationUser()
	{
		$result = array();
		$data = $this->findUserListWithoutObservationUser();

		foreach ($data as $v) {
			$result[$v->emp_no] = $v->last_name . " " . $v->first_name;
		}

		return $result;
	}
	
	
	//================================================================================
	//
	//	外部管理者の除外した社員一覧取得
	//
	//================================================================================	
	public function findUserListWithoutObservationUser($isKeySetEmpNo = true)
	{
		
		$systemObservationUserList = \Config::get('app.systemObservationUser');
		
		$systemObservationUserList[] = "010003";
		
		$result = self::select(['emp_no', 'last_name', 'first_name', 'mail', 'emp_type', 'attendance_flg', 'send_mail_flg', 'default_division_id', 'reg_date', 'resign_date'])
			->where('status', 1)
			->where('del_flg', 0)
			->where('attendance_flg', 1)
			->whereNotIn('emp_no', $systemObservationUserList)
			->get();

		if ($isKeySetEmpNo) {
			$tmp = array();
			// keyにempNoを入れる指定であれば、そのように組み替える

			foreach ($result as $val) {
				$tmp[$val->emp_no] = $val;
			}

			$result = $tmp;
		}

		return $result;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * ID 名前のリストを取得
	 */
	public function findListIdAndName()
	{
		$result = array();
		$data = $this->findAvailableUserList();

		foreach ($data as $v) {
			$result[$v->emp_no] = $v->last_name . " " . $v->first_name;
		}

		return $result;
	}


	/**
	 * 有効なユーザーリストを取得
	 */
	public function findAvailableUserList($isKeySetEmpNo = true)
	{
		$result = self::select(['emp_no', 'last_name', 'first_name', 'mail', 'emp_type', 'attendance_flg', 'send_mail_flg', 'default_division_id', 'reg_date', 'resign_date'])
			->where('status', 1)
			->where('del_flg', 0)
			->get();

		if ($isKeySetEmpNo) {
			$tmp = array();
			// keyにempNoを入れる指定であれば、そのように組み替える

			foreach ($result as $val) {
				$tmp[$val->emp_no] = $val;
			}

			$result = $tmp;
		}

		return $result;
	}



	/**
	 * 全てのユーザーリストを取得
	 */
	public function getAllUserNameList()
	{
		$result = self::select(['emp_no', 'last_name', 'first_name', 'mail', 'emp_type', 'attendance_flg', 'send_mail_flg', 'reg_date', 'resign_date'])
				->where('del_flg', 0)
				->get();

		$employees = array();
			// keyにempNoを入れる指定であれば、そのように組み替える

		foreach ($result as $val)
		{
			$employees[$val->emp_no] = $val->last_name . " " . $val->first_name;
		}

		return $employees;
	}
	/**
	 * 全てのユーザーリストを取得
	 */
	public function getAllList()
	{
		$result = self::select(['emp_no', 'last_name', 'first_name', 'mail', 'emp_type', 'attendance_flg', 'send_mail_flg', 'reg_date', 'resign_date', 'status'])
				->where('del_flg', 0)
				->get();

		$employees = array();
			// keyにempNoを入れる指定であれば、そのように組み替える

		foreach ($result as $val)
		{
			$employees[$val->emp_no]				= $val;
			$employees[$val->emp_no]->name	= $val->last_name . " " . $val->first_name;
		}

		return $employees;
	}


    /**
     * 管理部のユーザー情報を取得
     */
    public function findManagementTeams()
    {
        $data = self::select(['employees.emp_no', 'last_name', 'first_name', 'mail', 'position_id'])
        	->join('department_relations', 'department_relations.emp_no', '=', 'employees.emp_no')
        	->join('departments', 'departments.id', '=', 'department_relations.department_id')
        	->where('departments.id', Departments::ID_DEPARTMENT_MANAGEMENT)
        	->where('employees.status', 1)
        	->where('employees.del_flg', 0)
        	->get();

        return $data;
    }


    /**
     * 自分の上司の情報を取得
     */
    public function findMyHighOfficerData($empNo)
    {
    	$departmentRelations = new DepartmentRelations;
    	$relationData = $departmentRelations->findOneByEmpNo($empNo);

    	$result = null;
		// 最上位の役職ならばスルー
		if ($relationData->position_id === DepartmentRelations::ID_POSITION_GENERAL_MANAGER) {
			return $result;
		}

        // 所属部署の、自分の上のポジションの人を一件取得
        $result = self::join('department_relations', 'department_relations.emp_no', '=', 'employees.emp_no')
            ->join('departments', 'departments.id', '=', 'department_relations.department_id')
            ->select(['employees.emp_no', 'employees.mail', 'department_relations.position_id'])
            ->where('department_relations.position_id', '>', $relationData->position_id)
            ->where('departments.id', $relationData->department_id)
            ->where('employees.status', 1)
            ->where('employees.del_flg', 0)
            ->orderBy('department_relations.position_id', 'ASC')
            ->orderBy('employees.emp_no', 'ASC')
            ->first();

        return $result;
    }


	/**
	 * 指定日にどちらも登録されていないユーザーデータを取得
	 */
	public function findUnRegisterAttendanceUsers($startDate, $endDate, array $empNoList = array())
	{
		$builder = self::where('status', 1);

		if (count($empNoList) > 0) {
			$builder = $builder->whereIn('emp_no', $empNoList);
		}

		$result = $builder->where('del_flg', 0)
            ->whereNotIn('emp_no', function($_builder) use($startDate, $endDate) {
                $_builder->from('attendance')
                    ->select('emp_no')
                    ->where('reg_date', '>=', $startDate)
                    ->where('reg_date', '<=', $endDate)
                    ->get();
            })->get();

        return $result;
	}


	/**
	 * 勤務記録ページャ取得(すべて)
	 */
	public function findPagerAllDailyAttandance($limit, $startDate, $endDate, array $empNoList = [])
	{
        $limit = empty($limit) ? $this->perPage : $limit;

		$builder = self::select('emp_no', 'last_name', 'first_name', 'emp_type');
		$builder = $builder->where('status', 1)->where('attendance_flg', 1)->where('del_flg', 0);

        if (count($empNoList) >= 1) {
            $builder = $builder->whereIn('emp_no', $empNoList);
        }

		$empNoList = array();
		$listPager = $builder->paginate($limit);
		foreach ($listPager as $val) {
			$empNoList[] = $val->emp_no;
		}

		$attendances = new Attendances;
		$attendanceList = $attendances->findDailyAttandanceData($startDate, $endDate, $empNoList);
		foreach ($listPager as &$val) {
			$val->start_date = null;
			$val->end_date   = null;
			$val->isNew      = true;
			if (isset($attendanceList[$val->emp_no]))
			{
				$val->id         = $attendanceList[$val->emp_no]->id;
				$val->start_date = $attendanceList[$val->emp_no]->start_date;
				$val->end_date   = $attendanceList[$val->emp_no]->end_date;
				$val->init_start_date = $attendanceList[$val->emp_no]->init_start_date;
				$val->init_end_date   = $attendanceList[$val->emp_no]->init_end_date;
				$val->division_id   = $attendanceList[$val->emp_no]->division_id;
				$val->paid_holiday   = $attendanceList[$val->emp_no]->paid_holiday;
				$val->note       = $attendanceList[$val->emp_no]->note;
				$val->isNew      = false;
			}
		}

		return $listPager;
	}


	/**
	 * 勤務記録ページャ取得(保存済)
	 */
	public function findPagerSavedDailyAttandance($limit, $startDate, $endDate, array $empNoList = [])
	{
        $limit = empty($limit) ? $this->perPage : $limit;

		$builder = self::join('attendance', 'attendance.emp_no', '=', 'employees.emp_no');

        if (count($empNoList) >= 1) {
            $builder = $builder->whereIn('employees.emp_no', $empNoList);
        }

		$builder = $builder->where('employees.status', 1);
		$builder = $builder->where('employees.attendance_flg', 1);
		$builder = $builder->where('attendance.reg_date', '>=', $startDate);
		$builder = $builder->where('attendance.reg_date', '<=', $endDate);
		$builder = $builder->where('attendance.del_flg', 0);
		$builder = $builder->whereNotNull('attendance.start_date');
		$builder = $builder->whereNotNull('attendance.end_date');
		$listPager = $builder->paginate($limit);

		foreach ($listPager as &$val) {
			$val->isNew = false;
		}

		return $builder->paginate($limit);
	}


	/**
	 * 勤務記録ページャ取得(未記録)
	 */
	public function findPagerLostDailyAttandance($limit, $startDate, $endDate, array $empNoList = [])
	{
        $limit = empty($limit) ? $this->perPage : $limit;

		$attendances = new Attendances;
		$builder = self::select('emp_no', 'last_name', 'first_name', 'emp_type');
		$builder = $builder->where('status', 1)->where('attendance_flg', 1)->where('del_flg', 0);

        if (count($empNoList) >= 1) {
            $builder = $builder->whereIn('emp_no', $empNoList);
        }

		$builder = $builder->whereNotIn('emp_no', function($_builder) use($startDate, $endDate) {
			$_builder->from('attendance')
				->select('emp_no')
				->where('start_date', '>=', $startDate)
				->whereNotNull('start_date')
				->where('end_date', '<=', $endDate)
				->whereNotNull('end_date')
				->get();
		});

		$empNoList = array();
		$listPager = $builder->paginate($limit);
		foreach ($listPager as $val) {
			$empNoList[] = $val->emp_no;
		}

		$attendances = new Attendances;
		$attendanceList = $attendances->findDailyAttandanceData($startDate, $endDate, $empNoList);
		foreach ($listPager as &$val) {
			$val->start_date = null;
			$val->end_date   = null;
			$val->isNew      = true;
			if (isset($attendanceList[$val->emp_no])) {
				$val->id         = $attendanceList[$val->emp_no]->id;
				$val->start_date = $attendanceList[$val->emp_no]->start_date;
				$val->end_date   = $attendanceList[$val->emp_no]->end_date;
				$val->init_start_date = $attendanceList[$val->emp_no]->init_start_date;
				$val->init_end_date   = $attendanceList[$val->emp_no]->init_end_date;
				$val->division_id   = $attendanceList[$val->emp_no]->division_id;
				$val->paid_holiday   = $attendanceList[$val->emp_no]->paid_holiday;
				$val->note       = $attendanceList[$val->emp_no]->note;
				$val->isNew      = false;
			}
		}

		return $listPager;
	}
}
