<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentTypes extends Model
{
    //
	protected $table = "employment_type";
	//
	protected $fillable = ["id", "name"];
}
