<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\AttendanceApproval;
use App\Employees;
use App\Departments;
use App\DepartmentRelations;

class ApprovalController extends Controller
{
	public function __construct()
	{
		if(!$user = \Auth::user())
		{
			return redirect('/admin');
		}
	
		if($user->haveNormalRole())
		{
			return redirect('/admin');
		}
	}


	/**
	* 承認・非承認リスト一覧ページ
	*/
	public function index(Request $request, $selectDate = null, $filter = null)
	{
		$selectDate    = empty($selectDate) ? date("Ym", strtotime("-1 month")) : $selectDate;
		$filter        = is_null($filter) ? -1 : $filter;
		$user          = \Auth::user();
		$empNoList     = array();
		$listData      = array();
		$employees     = new Employees;
		$employeesList = $employees->getAllUserNameList();
		$selectYear    = date("Y", strtotime($selectDate . "01"));
		$selectMonth   = date("m", strtotime($selectDate . "01"));
		$allEmployeesList = $employeesList;
		$availableUserList  = $employees->getAllList();
		
		$firstDayOfMonth  = strtotime($selectDate . "01");
		$lastDayOfMonth  = strtotime(date('Y-m-t', strtotime($selectDate . "01")));
		
		$departments =  Departments::allOrderByName();
		$relations   = new DepartmentRelations;
		
		$start = strtotime("2016/01/01");
		$end = strtotime("now - 1 month");
		//$end = strtotime("now");
		$yearMonth = array();
		
		$yearMonth[] = array("year" => date("Y", $start), "month" => date("m", $start));
		
		$isAdmin = TRUE;
	
		$systemObservationUserList = \Config::get('app.systemObservationUser');
		
		do
		{
			$start = strtotime("+1 month", $start);
			
			$yearMonth[] = array("year" => date("Y", $start), "month" => date("m", $start));
		}
		while($start < $end);
		//================================================================================
		//
		//	勤怠管理しない社員の削除
		//
		//================================================================================	
		if(!$user->haveSystemAdministratorRole() && !$user->haveManagimentTeamRole() && !$user->belongDeptManager())
		{
			// システム管理者でも管理部でもない場合
			
			$isAdmin = FALSE;
			
			$departmentRelate = $relations->findByDepartmentId($user->getMyDepartmentId());
			
			foreach ($departmentRelate as $val)
			{
				$empNoList[] = $val->emp_no;
			}
			
			foreach ($employeesList as $empNo => $name)
			{	
				if(in_array($empNo, $empNoList) === false)
				{
					unset($employeesList[$empNo]);
				}
			}
		}
		//================================================================================
		//
		//	勤怠管理しない社員の削除
		//
		//================================================================================
		
		//	STの管理部社員削除
		foreach($systemObservationUserList as $empNo)
		{
			unset($employeesList[$empNo]);
		}
		//	上田さんの削除(一時的)
		unset($employeesList["010003"]);
		
		foreach($availableUserList as $emp)
		{
			//	システム登録日入社
			$regDate = strtotime($emp->reg_date);	
			
			if($regDate > $lastDayOfMonth)
			{
				unset($employeesList[$emp->emp_no]);
			}
				
			if(!is_null($emp->resign_date))
			{
				//	退職日
				$resignDate = strtotime($emp->resign_date);	
			
				if($resignDate < $firstDayOfMonth)
				{
					//var_dump($emp->emp_no . "	resign_date=" . $emp->resign_date);
					unset($employeesList[$emp->emp_no]);
				}
			}
			//	休職者を除く
			if($emp->status == 2)
			{
				unset($employeesList[$emp->emp_no]);
			}
		}
		
		$data  = array();
		$_data = AttendanceApproval::findListData($selectDate, $filter, $empNoList);
		
		foreach ($_data as $val)
		{			
			$data[$val->emp_no] = $val;
		}

	
		// 承認済み、承認取り消し
		if ($filter == 1 || $filter == 2)
		{
			foreach ($data as $empNo => $val)
			{	
				$relationInfo = $relations->findOneByEmpNo($empNo);
			
				$listData[$empNo] = new \stdClass();
				$listData[$empNo]->emp_no		= $empNo;
				$listData[$empNo]->name			= $availableUserList[$empNo]->name;
				$listData[$empNo]->upd_date		= $val->upd_date->toDateTimeString();
				$listData[$empNo]->upd_name		= $availableUserList[$val->upd_emp_no]->last_name  . " " .  $availableUserList[$val->upd_emp_no]->first_name;
				$listData[$empNo]->upd_mail		= $availableUserList[$val->upd_emp_no]->mail;
				$listData[$empNo]->mail			= $availableUserList[$empNo]->mail;
				$listData[$empNo]->note			= $val->note;
				$listData[$empNo]->status			= $val->status;
				$listData[$empNo]->status_name	= ($val->status == 1) ? "承認済" : "承認取消";
				$listData[$empNo]->status_color	= ($val->status == 1) ? "steelblue" : "white";
				$listData[$empNo]->department_id = $relationInfo->department_id;
				$listData[$empNo]->position_id = $relationInfo->position_id;
			}
			
		}else{
			foreach ($employeesList as $empNo => $name)
			{
				$relationInfo = $relations->findOneByEmpNo($empNo);
				// 未承認
				if($filter == 0 && isset($data[$empNo]) || is_null($relationInfo))
				{
					continue;
				}
				//var_dump($empNo . "		" . $relationInfo->department_id);
				// それ以外
				$listData[$empNo] = new \stdClass();
				$listData[$empNo]->emp_no			= $empNo;
				$listData[$empNo]->name				= $name;
				$listData[$empNo]->mail				= $availableUserList[$empNo]->mail;
				$listData[$empNo]->upd_date			= "--";
				$listData[$empNo]->upd_name			= "--";
				$listData[$empNo]->note				= "--";
				$listData[$empNo]->status				= 0;
				$listData[$empNo]->status_name		= "未承認";
				$listData[$empNo]->status_color		= "coral";
				
				if(!is_null($relationInfo)) 
				{
					$listData[$empNo]->department_id	= $relationInfo->department_id;
					$listData[$empNo]->position_id		= $relationInfo->position_id;
				}
				
				if (isset($data[$empNo]))
				{
					$listData[$empNo]->upd_date		= $data[$empNo]->upd_date->toDateTimeString();
					$listData[$empNo]->upd_name		= $allEmployeesList[$data[$empNo]->upd_emp_no];
					$listData[$empNo]->upd_mail		= $availableUserList[$data[$empNo]->upd_emp_no]->mail;
					$listData[$empNo]->note			= is_null($data[$empNo]->note) ? "--" : $data[$empNo]->note;
					$listData[$empNo]->status			= $data[$empNo]->status;
					$listData[$empNo]->status_name	= ($data[$empNo]->status == 1) ? "承認済" : "承認取消";
					$listData[$empNo]->status_color	= ($data[$empNo]->status == 1) ? "steelblue" : "white";
				}
			}
		}
		
		$blade = $isAdmin === true ? 'admin.approval.index_admin' : 'admin.approval.index';
	
	return view($blade, compact('yearMonth', 'listData', 'selectYear', 'selectMonth', 'selectDate', 'filter', 'employeesList', 'departments', 'availableUserList'));
	}


	/**
	* 承認ステータス変更
	*/
	public function postUpdate(Request $request)
	{
		$approvalMonth	= $request->get('approval_month');
		$empNo				= $request->get('emp_no');
		$status				= $request->get('status');
		$note					= $request->get('note');
		$attendanceApproval = new AttendanceApproval;
		
		if($status != 1 && $status != 2)
		{
			return new JsonResponse('STATUS_ERROR', 400);
		}
		
		// 新規登録
		if(!$approval = $attendanceApproval->findOneByPKs($empNo, $approvalMonth))
		{
			$approval                 = new AttendanceApproval;
			$approval->emp_no         = $empNo;
			$approval->approval_month = $approvalMonth;
			$approval->reg_emp_no     = \Auth::user()->emp_no;
			$approval->reg_date       = date('Y-m-d H:i:s');
		}
		
		if($note !== NULL) $approval->note = $note;
		$approval->status			= $status;
		$approval->upd_emp_no	= \Auth::user()->emp_no;
		$approval->upd_date		= date('Y-m-d H:i:s');
		
		if(!$approval->save())
		{
			return new JsonResponse('SAVE_ERROR', 500);
		}
		
		return new JsonResponse('OK', 200);
	}
}
