<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Auth\Guard;
use App\Attendances;
use App\Employees;
use App\DepartmentRelations;
use App\AttendanceApproval;
use App\TimeSheet;
use App\PaidHolidays;

class AttendanceController extends Controller
{
	public function __construct(Router $router)
	{
		$this->adminBeforeFilter();
	}
	/**
	* インデックスページ
	*/
	public function index(Request $request)
	{
		$user = \Auth::user();
		// 一般社員の場合は社員別勤怠がデフォルト
		if ($user->haveNormalRole())
		{
			return $this->employees($request);
		// それ以外の社員は日別勤怠がデフォルト
		} else {
			return $this->daily($request);
		}
	}
	/**
	* 日別
	*/
	public function daily(Request $request, $selectDate = null, $filter = null)
	{
		$employees   = new Employees;
		$selectDate  = empty($selectDate) ? date('Y/m/d') : date('Y/m/d', strtotime($selectDate));
		$formDate    = date('Ymd', strtotime($selectDate));
		$filter      = empty($filter) ? 1 : $filter;
		$startDate   = date('Y-m-d 00:00:00', strtotime($selectDate));
		$endDate     = date('Y-m-d 23:59:59', strtotime($selectDate));
		$yearMonth 	 = date('Ym', strtotime($selectDate));

		$empNoList   = array();
		$user        = \Auth::user();
		$paidHolidays	= PaidHolidays::all();
		$timeSheet	= TimeSheet::all();
		//
		if (!$user->haveSystemAdministratorRole() && !$user->haveManagimentTeamRole() && !$user->belongDeptManager())
		{
			// システム管理者でも管理部でもない場合
			$relations   = new DepartmentRelations;
			$departments = $relations->findByDepartmentId($user->getMyDepartmentId());

			if ($user->haveNormalRole())
			{
				// 一般社員であれば自分しか出さない
				$deparment   = $relations->findOneByEmpNo($user->emp_no);
				$departments = array($deparment);
			}

			foreach ($departments as $val)
			{
				$empNoList[] = $val->emp_no;
			}
		}

		switch ($filter)
		{
			// 全て
			case 1:
				$listPager = $employees->findPagerAllDailyAttandance($this->page_count, $startDate, $endDate, $empNoList);
			break;

			// 記録済
			case 2:
				$listPager = $employees->findPagerSavedDailyAttandance($this->page_count, $startDate, $endDate, $empNoList);
			break;

			// 未記録
			case 3:
				$listPager = $employees->findPagerLostDailyAttandance($this->page_count, $startDate, $endDate, $empNoList);
			break;

			default:
				$listPager = $employees->findPagerAllDailyAttandance($this->page_count, $startDate, $endDate, $empNoList);
		}

		// 過去月以前かどうか
		if(intval($yearMonth) <= intval(date("Ym", strtotime("-1 month"))))
		{
			// その月に承認テーブルにデータが存在しているか
			/*if($approval = AttendanceApproval::findOneByPKs($userId, $date))
			{
			$approvalFooterStatus = $approval->status;
			}*/

			//$approvalList = AttendanceApproval::findListData($yearMonth);

			//var_dump(count($approvalList));
			foreach($listPager as $data)
			{

				$resultApproval = AttendanceApproval::findOneByPKs($data->emp_no, $yearMonth);

				if(isset($resultApproval))
				{
					//var_dump($resultApproval->status);
					$data->approval = $resultApproval;
				}
			}
		}

		// 現在のURLをセッションに保存
		\Session::set('attendance_list_url', $request->getRequestUri());

		return view('admin.attendance.daily', compact('listPager', 'selectDate', 'formDate', 'filter', 'approvalList', 'timeSheet', 'paidHolidays'));
	}
	//================================================================================
	//
	//	社員別
	//
	//================================================================================
	public function employees(Request $request, $userId = null, $date = null)
	{
		$user = \Auth::user();
		
		if(empty($userId))
		{
			$userId = $user->emp_no;
		}
		
		// ユーザーのデータと、ユーザーリストを取得
		$employees = new Employees;
		$userList  = $employees->findListIdAndName();
		$userData  = $employees->findOneByPK($userId);
		$availableUserList  = $employees->findAvailableUserList();
		//$paidHolidays	= PaidHolidays::all();
		$phd    = new PaidHolidays;
		$paidHolidays	= $phd->getAllSortCompensatory();
		$timeSheet	= TimeSheet::all();
		
		if(!$user->haveSystemAdministratorRole() && !$user->haveManagimentTeamRole() && !$user->belongDeptManager())
		{
			// システム管理者でも管理部でもない場合
			$relations   = new DepartmentRelations;
			$departments = $relations->findByDepartmentId($user->getMyDepartmentId());
			
			if($user->haveNormalRole())
			{
				// 一般社員であれば自分しか出さない
				$deparment   = $relations->findOneByEmpNo($user->emp_no);
				$departments = array($deparment);
			}
		
			// 社員一覧情報をセット
			$_userList = array();
			
			$isFindFirstDisplayEmployee = $availableUserList[$userId]->attendance_flg === 0 ? true : false;

			foreach ($departments as $val)
			{
				if (isset($userList[$val->emp_no]))
				{
					//var_dump($userData[$val->emp_no]);
					if($availableUserList[$val->emp_no]->attendance_flg == 1)
					{
						$_userList[$val->emp_no] = $userList[$val->emp_no];
						
						if($isFindFirstDisplayEmployee === true)
						{
							$isFindFirstDisplayEmployee = false;
							$userData = $employees->findOneByPK($val->emp_no);
							$userId = $val->emp_no;
						}
					}
				}
			}
		
			$userList = $_userList;
			
		}else{
			//================================================================================
			//
			//	勤怠管理しない社員の削除
			//
			//================================================================================
			foreach($availableUserList as $emp)
			{
				if($emp->attendance_flg === 0)
				{
					unset($userList[$emp->emp_no]);
				}
			}
		}

		// 日付関係の変数
		$weeks = array('日', '月', '火', '水', '木', '金', '土');
		$date  = empty($date) ? date('Ym') : $date;
		$selectYear  = date('Y', strtotime($date . '01'));
		$selectMonth = date('m', strtotime($date . '01'));
		$selectStartTimestamp = mktime(0, 0, 0, $selectMonth, 1, $selectYear);
		$selectEndTimestamp   = mktime(0, 0, 0, $selectMonth + 1, 0, $selectYear);
		$selectStartDay  = date('j', $selectStartTimestamp);
		$selectEndDay    = date('j', $selectEndTimestamp);
		$yearMonth = $date;
		
		// 表示用リストデータを取得
		$listData = array();
		for($i = $selectStartDay; $i <= $selectEndDay; $i++)
		{
			$listData[$i]['day']        = sprintf('%02d', $i);
			$listData[$i]['date']	= date('Y/m/d', mktime(0, 0, 0, $selectMonth, $i, $selectYear));
			$listData[$i]['day_str']    = (int)$selectMonth . '月' . $i . '日';
			$listData[$i]['week']       = $weeks[date('w', mktime(0, 0, 0, $selectMonth, $i, $selectYear))];
			$listData[$i]['week_color'] = '';
			$listData[$i]['text_color'] = '#000000';
		
			// セルの色付けの(土・日・祝)
			if($listData[$i]['week'] == '日')
			{
				$listData[$i]['week_color'] = '#fff0f5';
				$listData[$i]['text_color'] = '#e60009';
			}elseif($listData[$i]['week'] == '土'){
				$listData[$i]['week_color'] = '#f0f8ff';
				$listData[$i]['text_color'] = '#1f497d';
			}else{
				$targetDate = $date . $listData[$i]['day'];
				if(\Holiday::isHoliday(strtotime($targetDate)))
				{
					$listData[$i]['week_color'] = '#fff0f5';
					$listData[$i]['text_color'] = '#e60009';
				}
			}
		}

		// ユーザーの日別勤務記録
		$attendances    = new Attendances;
		$attendanceList = $attendances->findDailyUserAttendance($selectStartTimestamp, $selectEndTimestamp, $userId);
		
		foreach ($attendanceList as $data)
		{
			//	$data->start_date だと 退勤のみの時に表示されない
			$key = date('j', strtotime($data->reg_date));
			$listData[$key]['detail'] = $data;
		}

		$approvalFooterStatus = false;
		
		$approvalNote = "";

		// 過去月以前かどうか
		//if(intval($date) <= intval(date("Ym", strtotime("-1 month"))))
		//{
			$approvalFooterStatus = 0;

			// その月に承認テーブルにデータが存在しているか
			if($approval = AttendanceApproval::findOneByPKs($userId, $date))
			{
				$approvalFooterStatus = $approval->status;
				$approvalNote = $approval->note;
			}
		//}
		// 現在のURLをセッションに保存
		\Session::set('attendance_list_url', $request->getRequestUri());
		
		$bladeName = \Input::get("print") == "1" ? 'admin.attendance.employees_print' : 'admin.attendance.employees';

		return view($bladeName, compact('userData', 'userList', 'selectYear', 'selectMonth', 'listData', 'approvalFooterStatus', 'approvalNote', 'yearMonth', 'paidHolidays', 'timeSheet'));
	}


	/**
	* 新規作成
	*/
	public function create($empNo, $targetDate)
	{
		$formType       = __FUNCTION__;
		$employees      = new Employees;
		$userList       = $employees->findListIdAndName();
		$hourList       = range(0, 23);
		$minuteList     = range(0, 59);
		$attendanceData = array();
		$listPageUrl    = \Session::get('attendance_list_url');
		//$paidHolidays	= PaidHolidays::all();
		$phd    = new PaidHolidays;
		$paidHolidays	= $phd->getAllSortCompensatory();
		$timeSheet	= TimeSheet::all();

		if (empty($listPageUrl)) {
			$listPageUrl = '/admin';
		}

		foreach ($hourList as &$val) {
			$val = sprintf('%02d', $val);
		}

		foreach ($minuteList as &$val) {
			$val = sprintf('%02d', $val);
		}

		$attendanceData['emp_no']    = $empNo;
		$userData		= $employees->findOneByPK($empNo);
		$attendanceData['default_division_id']    = $userData->default_division_id;
		$attendanceData['startDate'] = date('Y/m/d', strtotime($targetDate));
		$attendanceData['endDate']   = date('Y/m/d', strtotime($targetDate));

		return view('admin.attendance.form', compact('userList', 'hourList', 'minuteList', 'formType', 'attendanceData', 'listPageUrl', 'targetDate', 'paidHolidays', 'timeSheet'));
	}


	/**
	* 編集
	*/
	public function edit($id)
	{
		$formType       = __FUNCTION__;
		$employees      = new Employees;
		$attendances    = new Attendances;
		$userList       = $employees->findListIdAndName();
		$hourList       = range(0, 23);
		$minuteList     = range(0, 59);
		$attendanceData = array();
		$listPageUrl    = \Session::get('attendance_list_url');
		$targetDate     = null;
		//$paidHolidays	= PaidHolidays::all();
		$phd    = new PaidHolidays;
		$paidHolidays	= $phd->getAllSortCompensatory();
		
		$timeSheet	= TimeSheet::all();

		if (empty($listPageUrl)) {
			$listPageUrl = '/admin';
		}

		foreach ($hourList as &$val) {
			$val = sprintf('%02d', $val);
		}

		foreach ($minuteList as &$val) {
			$val = sprintf('%02d', $val);
		}

		$data = $attendances->find($id);
		$userData		= $employees->findOneByPK($data->emp_no);
		$attendanceData['emp_no']      = $data->emp_no;
		$attendanceData['username']    = $userList[$data->emp_no];
		$attendanceData['default_division_id']    = $userData->default_division_id;
		$attendanceData['division_id']	= $data->division_id;
		$attendanceData['paid_holiday']	= unserialize($data->paid_holiday);
		$attendanceData['note']        = $data->note;
		$attendanceData['startDate']   = is_null($data->start_date) ? null : date('Y/m/d', strtotime($data->start_date));
		$attendanceData['startHour']   = is_null($data->start_date) ? 0    : date('H', strtotime($data->start_date));
		$attendanceData['startMinute'] = is_null($data->start_date) ? 0    : date('i', strtotime($data->start_date));
		$attendanceData['endDate']     = is_null($data->end_date)   ? null : date('Y/m/d', strtotime($data->end_date));
		$attendanceData['endHour']     = is_null($data->end_date)   ? 0    : date('H', strtotime($data->end_date));
		$attendanceData['endMinute']   = is_null($data->end_date)   ? 0    : date('i', strtotime($data->end_date));
		$attendanceData['rawStartDate'] = is_null($data->start_date) ? null : $data->start_date;
		$attendanceData['rawEndDate']   = is_null($data->end_date)   ? null : $data->end_date;

		return view('admin.attendance.form', compact('userList', 'hourList', 'minuteList', 'formType', 'attendanceData', 'id', 'listPageUrl', 'targetDate', 'timeSheet', 'paidHolidays'));
	}
	//==================================================
	//	備考の更新
	//==================================================
	public function updateNote(Request $request)
	{
		$targetId = $request->input('targetId');

		$paidHoliday = $request->input('paidHoliday');

		if (empty($paidHoliday)  || count($paidHoliday) == 0)
		{
			$paidHoliday =  null;
		}else{
			$paidHoliday = serialize($paidHoliday);
		}

		$note = $request->input('note');

		if($note === "") $note = null;

		if(isset($targetId) && empty($targetId) === false)
		{
			$attendances = Attendances::find($targetId);

			if(isset($attendances))
			{
				
				$attendances->paid_holiday = $paidHoliday;
				$attendances->note = $note;

				if($attendances->save())
				{
					$success = true;
					$message = "有給休暇・備考を更新しました。";
				}else{
					$success = false;
					$message = "有給休暇・備考を保存できませんでした。";
				}
			}else{

				$success = false;
				$message = "指定された勤務記録が見つかりません。";
			}
		}else{
			
			$user = \Auth::user();
			
			$date = $request->input('date');
			
			$attendances = new Attendances();
			
			$attendanceInfo = $attendances->findTodayData($user->emp_no, $date);
			
			if(isset($attendanceInfo))
			{
				$attendanceInfo->note = $note;
				
				if($attendanceInfo->save())
				{
					$success = true;
					$message = "有給休暇・備考を更新しました。";
				}else{
					$success = false;
					$message = "有給休暇・備考を保存できませんでした。";
				}
				
			}else{
				
				return $this->saveNewNote($user->emp_no, $date, $paidHoliday, $note);
			}
		}

		return response()->json(["success"=>$success, "message"=>$message]);
	}

	//==================================================
	//	新規で備考を記録
	//==================================================
	private function saveNewNote($empNo, $targetDate, $paidHoliday, $note)
	{	
		if(empty($note))
		{
			$note = null;
		}
		
		$attendances = new Attendances;
		
		if(!$attendances->execSave(null, $empNo, $empNo, null, null, $targetDate, null, $paidHoliday, $note))
		{
			//return new JsonResponse('保存に失敗しました', 500);
			$success = false;
			$message = "備考を更新できませんでした。";
		}else{
			$success = true;
			$message =  "備考を更新しました。";
		}
		//return new JsonResponse('OK', 200);
		return response()->json(["success"=>$success, "message"=>$message]);
	}
	/**
	* データ削除
	*/
	public function delete(Request $request, Guard $auth)
	{
		if (!$targetId = $request->get('targetId'))
		{
			return new JsonResponse('パラメータが不正です', 400);
		}

		$attendances = new Attendances;

		if (!$attendances->execDelete($targetId, $auth->id()))
		{
			return new JsonResponse('保存に失敗しました', 500);
		}

		return new JsonResponse('OK', 200);
	}
	/**
	* 保存
	*/
	public function save(Request $request, Guard $auth)
	{
		$adminId    = $auth->id();
		$targetId   = $request->get('targetId');
		$empNo      = $request->get('empNo');
		$startDate  = $request->get('startDate');
		$endDate    = $request->get('endDate');
		$divisionId = $request->get('divisionId');
		$paidHoliday = $request->get('paidHoliday');
		$note       = $request->get('note');
		$targetDate = $request->get('targetDate');

		if (empty($startDate))
		{
			$startDate = null;
		}

		if (empty($endDate))
		{
			$endDate = null;
		}

		if (empty($divisionId))
		{
			$divisionId = 0;
		}

		if (empty($paidHoliday))
		{
			$paidHoliday =  null;
		}else{
			$paidHoliday = serialize($paidHoliday);
		}

		if (empty($note))
		{
			$note = null;
		}

		$attendances = new Attendances;

		if (!$attendances->execSave($targetId, $adminId, $empNo, $startDate, $endDate, $targetDate, $divisionId, $paidHoliday, $note)) {
			//return new JsonResponse('保存に失敗しました', 500);
			$success = false;
			$message = "勤怠記録を更新できませんでした。";
		}else{
			$success = true;
			$message =  "勤怠記録を更新しました。";
		}
		//return new JsonResponse('OK', 200);
		return response()->json(["success"=>$success, "message"=>$message]);
	}
}
