<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Departments;
use App\DepartmentRelations;

class DepartmentController extends Controller
{
    /**
     * リストページ
     */
    public function index()
    {
        $departmentList = Departments::all();

        foreach ($departmentList as &$val) {
            $val->attachMember = DepartmentRelations::where('department_id', $val->id)->count();
        }

        return view('admin.department.index', compact('departmentList'));
    }


    /**
     * 新規作成ページ
     */
    public function create()
    {
        $id = null;
        $department = null;

        return view('admin.department.form', compact('department', 'id'));
    }


    /**
     * 修正ページ
     */
    public function edit($id)
    {
        $department = Departments::find($id);

        return view('admin.department.form', compact('department', 'id'));
    }


    /**
     * 保存処理
     */
    public function save(Request $request)
    {
        $targetId  = $request->get('targetId');
        $name      = $request->get('name');

        if (!$department = Departments::find($targetId)) {
            $department = new Departments();
        }

        $department->name = $name;

        if (!$department->save()) {
            return new JsonResponse('SAVE_ERROR', 500);
        }

        return new JsonResponse('OK', 200);
    }
}