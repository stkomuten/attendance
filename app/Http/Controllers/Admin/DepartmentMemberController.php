<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Departments;
use App\DepartmentRelations;
use App\Employees;


/**
 * 部署メンバー情報コントローラ
 */
class DepartmentMemberController extends Controller
{
    /**
     * リストページ
     */
    public function index(Request $request, $departmentId)
    {
        if (!$department = Departments::find($departmentId)) {
            return redirect('/admin/department');
        }

        $relations = array();
        $dr = DepartmentRelations::where('department_id', $departmentId)->get();
        foreach ($dr as $val) {
            $relations[$val->emp_no] = $val;
        }

        $positionList    = array();
        $positionSetting = \Config::get('app.positionType');
        foreach ($positionSetting as $val) {
            $positionList[$val['id']] = $val['name'];
        }

        $userList = array();new \stdClass();
        $users = Employees::whereIn('emp_no', array_keys($relations))->whereIn('status', [1, 2])->get();
        foreach ($users as $key => $val) {
            $userList[$key] = new \stdClass();
            $userList[$key]->emp_no      = $val->emp_no;
            $userList[$key]->last_name   = $val->last_name;
            $userList[$key]->first_name  = $val->first_name;
            $userList[$key]->position    = $positionList[$relations[$val->emp_no]->position_id];
            $userList[$key]->assign_date = date('Y年m月d日 H時i分', strtotime($relations[$val->emp_no]->created_at));
        }

        // 現在のURLをセッションに保存
        \Session::set('department_member_list_page_url', $request->getRequestUri());

        return view('admin.department_member.index', compact('department', 'userList'));
    }


    /**
     * 修正ページ
     */
    public function edit($departmentId)
    {
        if (!$department = Departments::find($departmentId)) {
            return redirect('/admin/department');
        }

        $relations = array();
        $dr = DepartmentRelations::all();
        foreach ($dr as $val) {
            $relations[$val->emp_no] = $val;
        }

        $positionList    = array();
        $positionSetting = \Config::get('app.positionType');
        foreach ($positionSetting as $val) {
            $positionList[$val['id']] = $val['name'];
        }

        $registedUserList   = array();
        $unRegistedUserList = array();
        $users = Employees::whereIn('status', [1, 2])->where('del_flg', 0)->get();
        foreach ($users as $user) {
            if (isset($relations[$user->emp_no]))
            {
                if ($relations[$user->emp_no]->department_id == $departmentId) {
                    $user->position_id   = $relations[$user->emp_no]->position_id;
                    $user->position_name = $positionList[$user->position_id];
                    $registedUserList[]   = $user;
                }
            } else {
                $unRegistedUserList[] = $user;
            }
        }

        $listPageUrl = \Session::get('department_member_list_page_url');

        return view('admin.department_member.edit', compact('department', 'registedUserList', 'unRegistedUserList', 'positionList', 'listPageUrl'));
    }


    /**
     * 登録処理
     */
    public function save(Request $request)
    {
        $targetId     = $request->get('targetId');
        $valueList    = $request->get('valueList');
        $positionList = $request->get('positionList');

        if (!$department = Departments::find($targetId)) {
            return new JsonResponse('SAVE_ERROR', 500);
        }

        $saveData = array();
		
		if(isset($valueList))
		{
        	foreach ($valueList as $val)
			{
				$saveData[$val] = array();
        	}
		}
		
		if(isset($positionList))
		{
        	foreach ($positionList as $val)
			{
				$saveData[$val['emp_no']] = $val['position_id'];
			}
		}

        try {

            \DB::beginTransaction();

            // 最初は消す
            DepartmentRelations::where('department_id', $targetId)->delete();
			
			if(count($saveData) > 0) 
			{
				foreach ($saveData as $empNo => $positionId) {
					$relations = new DepartmentRelations();
					$relations->department_id = $targetId;
					$relations->emp_no        = $empNo;
					$relations->position_id   = $positionId;
					$relations->save();
				}
			}
			
			\DB::commit();
			
            $message = "OK";
            $status  = 200;

        } catch (\Exception $e) {
            \DB::rollback();
            $message = $e->getMessage();
            $status  = 500;
        }

        return new JsonResponse($message, $status);
    }
}
