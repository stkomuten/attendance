<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

use App\Attendances;
use App\Employees;
use App\PaidHolidays;
use App\PaidHolidayBalance;
use App\TimeSheet;
use App\DepartmentRelations;
use App\Departments;
use App\TeamRelations;
use App\Teams;

class DownloadController extends Controller
{
	public function getDownload($type)
	{
		$start = strtotime("2015/12/01");
		$end = strtotime("now - 1 month");
		//$end = strtotime("now");
		$data = array();
		
		$data[] = array("year" => date("Y", $start), "month" => date("m", $start));
		
		do
		{
			$start = strtotime("+1 month", $start);
			
			$data[] = array("year" => date("Y", $start), "month" => date("m", $start));
		}
		while($start < $end);
		
		return view('admin.download.' . $type)->with("data", $data);
	}
	//================================================================================
	//
	//	勤務記録シート用のCSV生成
	//
	//================================================================================	
	public function postEmployeesCSV(Request $request)
	{		
		//==========
		//	社員別
		//==========
		$date					= empty($request->input("ym")) ? date('Ym') : $request->input("ym");
		$selectYear				= date('Y', strtotime($date . '01'));
		$selectMonth			= date('m', strtotime($date . '01'));
		
		$selectWeekAgoYear		= date('Y', strtotime($date . "01" ." -7day"));
		$selectWeekAgoMonth		= date('n', strtotime($date . "01" ." -7day"));
		$selectWeekAgoDate		= date('j', strtotime($date . "01" ." -7day"));
		
		$selectLastMonth		= date('Ym', strtotime($date . "01" ." -7day"));
				
		//$selectStartTimestamp	= mktime(0, 0, 0, $selectMonth, 1, $selectYear);
		$selectStartTimestamp	= mktime(0, 0, 0, (int)$selectWeekAgoMonth, (int)$selectWeekAgoDate, $selectWeekAgoYear);
		$selectEndTimestamp		= mktime(0, 0, 0, $selectMonth + 1, 0, $selectYear);
		$holidayPath 			= "/var/www/ams/storage/files/holiday/";
		$csvPath 				= "/var/www/ams/storage/files/csv/";
		$publicCSVPath 			= "/var/www/ams/public/csv/";
		//$holidayPath 			= "/var/www/attendance/storage/files/holiday/";
		//$csvPath 				= "/var/www/attendance/storage/files/csv/";
		//$publicCSVPath 			= "/var/www/attendance/public/csv/";
		$csvDatePath			= $csvPath . $date . "/";
		$systemObservationUserList = \Config::get('app.systemObservationUser');
	
		$phBalance = new PaidHolidayBalance();
		//================================================================================
		//	ディレクトリがあるか判定
		//================================================================================		
		if(file_exists($csvDatePath) === false)
		{
			mkdir($csvDatePath, 0777, true);
			chmod($csvDatePath, 0777);
		}
		//================================================================================
		//	有休休暇CSVを読込
		//================================================================================
		setlocale(LC_ALL, 'ja_JP.UTF-8');
		/*
		$file = $holidayPath . $date . ".csv";
		$data = file_get_contents($file);
		$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
		
		$temp = tmpfile();
		$holidays  = array();
		
		fwrite($temp, $data);
		rewind($temp);
		
		while (($data = fgetcsv($temp, 1000, ",")) !== FALSE)
		{			
			$holidays[] = $data;
		}
		
		fclose($temp);
		*/
		//================================================================================
		//	先月分の有休休暇CSVを読込
		//================================================================================
		/*
		$file = $holidayPath . $selectLastMonth . ".csv";
		
		$data = file_get_contents($file);
		$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
		
		$temp = tmpfile();
		$lastMonthHolidays  = array();
		
		fwrite($temp, $data);
		rewind($temp);
		
		while (($data = fgetcsv($temp, 1000, ",")) !== FALSE)
		{			
			$lastMonthHolidays[] = $data;
		}
		
		fclose($temp);
		*/
		//================================================================================
		//	社員一覧を取得
		//================================================================================
		$employeeList = \DB::table("employees")
			->where('del_flg', '=', 0)
			->where('status', '=', 1)
			->orWhere('status', '=', 2)
			->get();

		//$holidayCount = count($holidays);
		//$lastMonthHolidayCount = count($lastMonthHolidays);
		
		foreach($employeeList as $employee)
		{
			if(($employee->status === 1 && $employee->attendance_flg === 0) && $employee->status === 3) continue;
			
			
			$isFind = false;
			
			foreach($systemObservationUserList as $empNo)
			{
				
				if((string)$employee->emp_no == (string)$empNo)
				{
					$isFind = true;
					continue;
				}
				
			}
			
			if($isFind) continue;
			
			
			
			$selectStartDay  = date('j', $selectStartTimestamp);
			$selectEndDay    = date('j', $selectEndTimestamp);
			
			$employeeID = $employee->emp_no;
			

			//if($employee->attendance_flg == 0) continue;
			// ユーザーの日別勤務記録
			$attendances    = new Attendances;
			$attendanceList = $attendances->findDailyUserAttendance($selectStartTimestamp, $selectEndTimestamp, $employeeID);
		
			$fileName = $employeeID . ".csv";
		
			//$fp = fopen('php://temp', 'r+b');
			$fp = fopen($csvDatePath . $fileName, "w");
			//fputcsv($fp, array("date", "start", "end", "holiday", "note"));
			
			
			//--------------------------------------------------------------------------------
			//	当月分の有給休暇
			//--------------------------------------------------------------------------------			
			
			/*
			$empHolidays = array();

			//	休暇データから社員番号を検索
			for($i=0;$i<$holidayCount;$i++)
			{
				$holidayInfo = $holidays[$i];
				
				if(strcmp($holidayInfo[1], $employeeID) === 0)
				{
					$empHolidays[] = $holidayInfo; 
				}
			}
			//--------------------------------------------------------------------------------
			//	先月分の有給休暇
			//--------------------------------------------------------------------------------
			$empLastMonthHolidays = array();
			//	休暇データから社員番号を検索
			for($i=0;$i<$lastMonthHolidayCount;$i++)
			{
				$holidayInfo = $lastMonthHolidays[$i];
				
				if(strcmp($holidayInfo[1], $employeeID) === 0)
				{
					$empLastMonthHolidays[] = $holidayInfo; 
				}
			}			
			*/
			
			for($i=-5;$i<=$selectEndDay;$i++)
			{
				
				if($i < 1)
				{
					$targetDate = date('Y/n/j',  strtotime($date . "01 " . ($i - 1) . "day"));
				}else{
					$targetDate = date('Y/n/j', strtotime($date . ($i < 10 ? "0". $i : $i)));					
				}
				
				$isFind = false;
				
				$holiday = "";
				
				foreach ($attendanceList as $data)
				{
					$reg_date = date('Y/n/j', strtotime($data->reg_date));
					
					//$empHolidayCount = count($empHolidays);
					//$empLastMonthHolidayCount = count($empLastMonthHolidays);
					
					/*if($i < 1)
					{
						//	有休の検索
						for($j=0;$j<$empLastMonthHolidayCount;$j++)
						{
							$holidayInfo = $empLastMonthHolidays[$j];
							
							if(strcmp($targetDate, $holidayInfo[0]) === 0)
							{
								$holiday = $holidayInfo[2];
							}
						}
					
					}else{
										
						//	有休の検索
						for($j=0;$j<$empHolidayCount;$j++)
						{
							$holidayInfo = $empHolidays[$j];
							
							if(strcmp($targetDate, $holidayInfo[0]) === 0)
							{
								$holiday = $holidayInfo[2];
							}
						}
					}
					*/
					
					if($targetDate === $reg_date)
					{
						$isFind = true;
						
						$start = is_null($data->start_date) === true ? "" : date('G:i', strtotime($data->start_date));
						
						//if($i > 0 ){$holiday = is_null($data->paid_holiday) === true ? "" : unserialize($data->paid_holiday)[0];}
						
						if(is_null($data->paid_holiday) === FALSE)
						{
							$holidays = unserialize($data->paid_holiday);
							$holiday1 = $holidays[0];
							$holiday2 = count($holidays) > 1 ? $holidays[1] : "";
						}else{
							$holiday1 = $holiday2 = "";
						}
						
						if(is_null($data->start_date) === true && is_null($data->end_date) === true)
						{
							$end = "";
						}else if(is_null($data->start_date) === true && is_null($data->end_date) === false){
							$end = date('G:i', strtotime($data->end_date));
						}else if(is_null($data->start_date) === false && is_null($data->end_date) === true){
							$end = "";
						}else{
							$end = $this->getExcelEndDate($data->start_date, $data->end_date);
						}
						
						$division = $data->division_id;
											
						fputcsv($fp, array($reg_date, $start, $end, $holiday1, $holiday2, $division, mb_convert_encoding(str_replace(chr(10),'\n',$data->note), 'SJIS', 'UTF-8')));
						
						break;
					}
					
				}
				
				if($isFind === false)
				{
					//if($i > 0 )$holiday = is_null($data->paid_holiday) === true ? "" : unserialize($data->paid_holiday)[0];
					fputcsv($fp, array($targetDate, "", "", "", "", "", ""));
				}

			}

			$phInfo = $phBalance->getEmployeeByDate($employeeID, $selectYear, $selectMonth);
			if(is_null($phInfo) === FALSE)
			{
				fputcsv($fp, array($phInfo->days, $phInfo->day_hours, $phInfo->hours));
			}else{
				fputcsv($fp, array("0", "0:00", "0:00"));
			}
			
			fclose($fp);
		}
				
		//var_dump($csvDatePath . $date . ".conf");
		$fp = @fopen($csvDatePath . $date . ".conf", "w");
		fputs($fp, $date);
		fclose($fp);
		
		$zipFileName = $date . ".zip";
		
		$message = "勤務記録シート用CSVを出力できませんでした。";
		$success = false;
		
		if($this->getZipCompression($csvDatePath, $publicCSVPath . $zipFileName) === true)
		{
			$message = "勤務記録シート用CSVを出力しました。";
			$success = true;
		}
		
		$html = "../../../csv/" . $zipFileName;
		
		return response()->json(["success"=>$success, "message"=>$message, "html"=>$html]);
	}
	//================================================================================
	//	社員一覧を取得
	//================================================================================	
	private function getZipCompression($target_path, $zip_name = '')
	{
		// 存在チェック
		if(!file_exists($target_path))
		{
			return FALSE;
		}
	
		// ZIPファイル名が未定義の場合は圧縮対象ディレクトリの同階層/日付.zip
		if(empty($zip_name))
		{
			$zip_name = $target_path . '/' . date('ymd') . '.zip';
		}
	
		$zip = new \ZipArchive();
		
		try
		{
			
			// アーカイブをオープン
			$zip->open($zip_name, \ZipArchive::CREATE);
	
			// 圧縮対象ディレクトリ
			$targetFiles = scandir($target_path);
			
			if (!empty($targetFiles))
			{
				foreach ($targetFiles as $targetFilesKey => $targetFilesVal)
				{
					// ファイルのみを抽出
					if(is_file($target_path . '/' . $targetFilesVal))
					{
						// アーカイブに追加
						$zip->addFile($target_path . '/' . $targetFilesVal, $targetFilesVal);
					}
				}
			}
			// アーカイブをクローズ
			$zip->close();
			
		}catch (Exception $e){
			
			return false;
		}
		return true;
		
	}
	
	//================================================================================
	//
	//	社員の入退館CSVのCSV生成
	//
	//================================================================================	
	public function postEntranceExitCSV(Request $request)
	{		
		//==========
		//	社員別
		//==========
		$date					= empty($request->input("ym")) ? date('Ym') : $request->input("ym");
		$selectYear				= date('Y', strtotime($date . '01'));
		$selectMonth			= date('m', strtotime($date . '01'));
		$selectStartTimestamp	= mktime(0, 0, 0, $selectMonth, 1, $selectYear);
		$selectEndTimestamp		= mktime(0, 0, 0, $selectMonth + 1, 0, $selectYear);
		$csvPath 				= "/var/www/ams/public/csv/";
		
		$selectStartDay 		= date('j', $selectStartTimestamp);
		$selectEndDay   		= date('j', $selectEndTimestamp);

		//================================================================================
		//	社員一覧を取得
		//================================================================================
		$employee = new Employees;
		$employeeList = $employee->getAllUserNameList(); 
	
		$fileName =  $date . "_EntranceExit.csv";
	
		$fp = fopen($csvPath . $fileName, "w");
	
		for($i=$selectStartDay;$i<=$selectEndDay;$i++)
		{
			
			$targetDate = date('Y-m-d', strtotime($date . ($i < 10 ? "0". $i : $i)));
			
			
				//->min('start_date');
			$first_enter_info = \DB::table("attendance")
				->select("emp_no", "start_date")
				->whereNotNull("start_date")
				->where("start_location", "=", 1)
				->where("del_flg", "=", 0)
				->whereBetween("reg_date", array($targetDate . " 00:00:00", $targetDate . " 23:59:59"))
				->orderBy("start_date", "asc")
				->first();
				
						
			$last_exit_info = \DB::table("attendance")
				->select("emp_no", "end_date")
				->where("end_location", "=", 1)
				->where("del_flg", "=", 0)
				->whereNotNull("end_date")
				->whereBetween("reg_date", array($targetDate . " 00:00:00", $targetDate . " 23:59:59"))
				->orderBy("end_date", "desc")
				->first();

				
			$first_enter_date = "";
			if(isset($first_enter_info->start_date)) $first_enter_date = $first_enter_info->start_date;
			
			$last_exit_date = "";
			if(isset($last_exit_info->end_date)) $last_exit_date = $last_exit_info->end_date;
			
			$first_enter_emp = "";
			
			if(isset($first_enter_info) && isset($first_enter_info->emp_no) && isset($employeeList[$first_enter_info->emp_no])) $first_enter_emp = mb_convert_encoding($employeeList[$first_enter_info->emp_no], 'SJIS', 'UTF-8');
			
			$last_exit_emp = "";
			if(isset($last_exit_info) && isset($last_exit_info->emp_no) && isset($employeeList[$last_exit_info->emp_no])) $last_exit_emp = mb_convert_encoding($employeeList[$last_exit_info->emp_no], 'SJIS', 'UTF-8');


			fputcsv($fp, array($targetDate, $first_enter_date, $first_enter_emp, $last_exit_date, $last_exit_emp));
		}
		
		fclose($fp);
				   
		$message = "CSV生成完了しました。";
		
		$html = "../../../csv/" . $fileName;
		
		return response()->json(["success"=>true, "message"=>$message, "html"=>$html]);
	}
	
	
	
	
	//================================================================================
	//
	//	勤怠分析用のCSV生成
	//
	//================================================================================	
	public function postAnalyzeAttendanceCSV(Request $request)
	{		
		//==========
		//	社員別
		//==========
		$date					= empty($request->input("ym")) ? date('Ym') : $request->input("ym");
		$selectYear				= date('Y', strtotime($date . '01'));
		$selectMonth			= date('m', strtotime($date . '01'));
		
		$selectWeekAgoYear		= date('Y', strtotime($date . "01" ." -7day"));
		$selectWeekAgoMonth		= date('n', strtotime($date . "01" ." -7day"));
		$selectWeekAgoDate		= date('j', strtotime($date . "01" ." -7day"));
		
		$selectLastMonth		= date('Ym', strtotime($date . "01" ." -7day"));
				
		//$selectStartTimestamp	= mktime(0, 0, 0, $selectMonth, 1, $selectYear);
		$selectStartTimestamp	= mktime(0, 0, 0, (int)$selectWeekAgoMonth, (int)$selectWeekAgoDate, $selectWeekAgoYear);
		$selectEndTimestamp		= mktime(0, 0, 0, $selectMonth + 1, 0, $selectYear);
	
		//$holidayPath 			= "/var/www/projects/www/laravel/storage/files/holiday/";
		//$csvPath 				= "/var/www/projects/www/laravel/storage/files/csv/";
		//$publicCSVPath 			= "/var/www/projects/www/laravel/public/csv/";
		$holidayPath 			= "/var/www/ams/storage/files/holiday/";
		$csvPath 				= "/var/www/ams/storage/files/csv/";
		$publicCSVPath 		= "/var/www/ams/public/csv/";
		$csvDatePath			= $csvPath . $date . "_analysis/";
		
		$jpDays = array(
								'日', //0
								'月', //1
								'火', //2
								'水', //3
								'木', //4
								'金', //5
								'土'  //6
								);

		
		$fileName =  $date . "_勤怠分析用.csv";
		$paidHolidays	= PaidHolidays::all();	
		$timeSheet	= TimeSheet::all();
		
		$systemObservationUserList = \Config::get('app.systemObservationUser');
		//================================================================================
		//	ディレクトリがあるか判定
		//================================================================================		
		//if(file_exists($csvDatePath) === false)
		//{
		//	mkdir($csvDatePath, 0777, true);
		//	chmod($csvDatePath, 0777);
		//}
		//================================================================================
		//	有休休暇CSVを読込
		//================================================================================
		setlocale(LC_ALL, 'ja_JP.UTF-8');
		/*
		$file = $holidayPath . $date . ".csv";
		$data = file_get_contents($file);
		$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
		
		$temp = tmpfile();
		$holidays  = array();
		
		fwrite($temp, $data);
		rewind($temp);
		
		while (($data = fgetcsv($temp, 1000, ",")) !== FALSE)
		{			
			$holidays[] = $data;
		}
		
		fclose($temp);
		*/
		//================================================================================
		//	先月分の有休休暇CSVを読込
		//================================================================================
		/*
		$file = $holidayPath . $selectLastMonth . ".csv";
		
		$data = file_get_contents($file);
		$data = mb_convert_encoding($data, 'UTF-8', 'sjis-win');
		
		$temp = tmpfile();
		$lastMonthHolidays  = array();
		
		fwrite($temp, $data);
		rewind($temp);
		
		while (($data = fgetcsv($temp, 1000, ",")) !== FALSE)
		{			
			$lastMonthHolidays[] = $data;
		}
		
		fclose($temp);
		*/
		//================================================================================
		//	社員一覧を取得
		//================================================================================
		$employeeList = \DB::table("employees")
			->where('del_flg', '=', 0)
			->where('status', '=', 1)
			->orWhere('status', '=', 2)
			->get();

		//$holidayCount = count($holidays);
		//$lastMonthHolidayCount = count($lastMonthHolidays);
		
		
		$fp = fopen($publicCSVPath . $fileName, "w");
		
		foreach($employeeList as $employee)
		{
			if(($employee->status === 1 && $employee->attendance_flg === 0) && $employee->status === 3) continue;
			
			if((string)$employee->emp_no === "010003") continue;
			
			$isFind = false;
			
			foreach($systemObservationUserList as $empNo)
			{
				
				if((string)$employee->emp_no == (string)$empNo)
				{
					$isFind = true;
					continue;
				}
				
			}
			
			if($isFind) continue;
			
			$selectStartDay  = date('j', $selectStartTimestamp);
			$selectEndDay    = date('j', $selectEndTimestamp);
			
			$employeeID = $employee->emp_no;
			$fullName = $employee->last_name . " " . $employee->first_name;
			$jpName = mb_convert_encoding($fullName, 'SJIS-win', 'UTF-8');
			
			
			
			//$relations = new DepartmentRelations;
			//$relateInfo = $relations->findOneByEmpNo($employeeID);
			
			$relations = new TeamRelations;
			$relateInfo = $relations->findOneByEmpNo($employeeID);
			
			if(is_null($relateInfo)) continue;
			
			//\Log::debug("department_id  = ". $relateInfo->department_id);
			$roleName = "";
			if($relateInfo->position_id === 110)
			{
				$roleName =  "上長";
			}else if($relateInfo->position_id === 120){
				$roleName =  "部長";
			}else{
				$roleName =  "一般";	
			}
			
			$roleName =  mb_convert_encoding($roleName, 'SJIS-win', 'UTF-8');
			$teamInfo = Teams::find((int)$relateInfo->team_id);
			$productName = mb_convert_encoding($teamInfo->project_name, 'SJIS-win', 'UTF-8');
			$deptName = mb_convert_encoding($teamInfo->team_name, 'SJIS-win', 'UTF-8');
			
			
			//if($employee->attendance_flg == 0) continue;
			// ユーザーの日別勤務記録
			$attendances    = new Attendances;
			$attendanceList = $attendances->findDailyUserAttendance($selectStartTimestamp, $selectEndTimestamp, $employeeID);
		
			//$fileName = $employeeID . ".csv";
			
			for($i=1;$i<=$selectEndDay;$i++)
			{
				$targetDate = date('Y/n/j', strtotime($date . ($i < 10 ? "0". $i : $i)));
				
				$isFind = false;
				
				$holiday = "";
				$jpDay = mb_convert_encoding($jpDays[date('w', strtotime($targetDate))], 'SJIS-win', 'UTF-8');
				foreach ($attendanceList as $data)
				{
					$reg_date = date('Y/n/j', strtotime($data->reg_date));
					
					//$jpDay = mb_convert_encoding($jpDays[date('w', strtotime($data->reg_date))], 'SJIS-win', 'UTF-8');
					$division_id =  $employee->default_division_id;
					
					if($targetDate === $reg_date)
					{
						$isFind = true;
						
						$start = is_null($data->start_date) === true ? "" : date('G:i', strtotime($data->start_date));
						
						//if($i > 0 ){$holiday = is_null($data->paid_holiday) === true ? "" : unserialize($data->paid_holiday)[0];}
						$holiday1Name = $holiday2Name = "";
						if(is_null($data->paid_holiday) === FALSE)
						{
							$holidays = unserialize($data->paid_holiday);
							$holiday1 = $holidays[0];
							$holiday2 = count($holidays) > 1 ? $holidays[1] : "";
							
							if(!is_null($holiday1) && (int)$holiday1 > 0 ) $holiday1Name = mb_convert_encoding($paidHolidays[(int)$holiday1-1]->name, 'SJIS-win', 'UTF-8');
							if(!is_null($holiday2) && (int)$holiday2 > 0 ) $holiday2Name = mb_convert_encoding($paidHolidays[(int)$holiday2-1]->name, 'SJIS-win', 'UTF-8');
							
						}else{
							$holiday1 = $holiday2 = "";
						}
						
						$originWorkDate = "";
						
						if(is_null($data->start_date) === true && is_null($data->end_date) === true)
						{
							$end = "";
						}else if(is_null($data->start_date) === true && is_null($data->end_date) === false){
							$end = date('G:i', strtotime($data->end_date));
						}else if(is_null($data->start_date) === false && is_null($data->end_date) === true){
							$end = "";
						}else{
							$end = $this->getExcelEndDate($data->start_date, $data->end_date);
							$division_id = is_null($data->division_id) ||  $data->division_id === 0 ? $employee->default_division_id : $data->division_id;
							
							$timeSheetAttr = $this->findDivisionAttrByID($division_id, $timeSheet);
							if(is_null($timeSheetAttr) === false) $originWorkDate = $this->getExcelOriginWorkDate($data->start_date, $data->end_date, $timeSheetAttr);
						}
						
						$divisionLabel = mb_convert_encoding(($division_id > 999 ? "時短" : ""), 'SJIS-win', 'UTF-8');
						
						//$division = $data->division_id;
											
						fputcsv($fp, array($productName, $deptName, $roleName, $employeeID, $jpName, $reg_date, $jpDay, $start, $end, $originWorkDate, $holiday1Name, $holiday2Name, $divisionLabel, mb_convert_encoding(str_replace(chr(10),'\n',$data->note), 'SJIS', 'UTF-8')));
						
						break;
					}
					
				}
				
				if($isFind === false)
				{
					//if($i > 0 )$holiday = is_null($data->paid_holiday) === true ? "" : unserialize($data->paid_holiday)[0];
					fputcsv($fp, array($productName, $deptName, $roleName, $employeeID, $jpName, $targetDate, $jpDay, "", "", "", "", "", "", ""));
				}	
			}	
		}
		
		fclose($fp);
		
		//$message = "勤怠分析用CSVを出力できませんでした。";
		//$success = false;
		
		$message = "勤怠分析用CSVを出力しました。";
		$success = true;
		
		
		$html = "../../../csv/" . $fileName;
		\Log::debug("html = ". $html);
		return response()->json(["success"=>$success, "message"=>$message, "html"=>$html]);
	}
	
	
	
	//================================================================================
	// 実働時間(Excel)
	//================================================================================
	function getExcelOriginWorkDate($excelStartDate, $excelEndDate, $timeSheetAttrs, $default = '--')
	{
		if (empty($excelStartDate) || empty($excelEndDate) || empty($timeSheetAttrs)) {
			return $default;
		}
		//
		microtime(true);
		//	
		$startDate = date('Y-m-d', strtotime($excelStartDate));
		
		$defaultTimeStamp =  strtotime("1970-01-01");
		
		//	開始日の9時を生成
		$startDateTime = strtotime($startDate . " " . $timeSheetAttrs['opening_time']);
	
		//	休憩時間算出始まり
		$breakTimeStartDate = strtotime("1970-01-01 " . $timeSheetAttrs['break_time_start']) - $defaultTimeStamp;
		
		$breakTimeSec = strtotime("1970-01-01 " . $timeSheetAttrs['break_time']) - $defaultTimeStamp;
		
		//	開始日時が9時以上か判定
		if(strtotime($excelStartDate) >= $startDateTime)
		{
			$startDateTime = strtotime($excelStartDate);
		}
		
		$origin = (strtotime($excelEndDate) - $startDateTime);
		
		if($origin < 0) return $default;	
		
		$calBreakTime =  $origin > $breakTimeStartDate ? $breakTimeSec : 0;
		
		$origin -= $calBreakTime;
		
		$hour   = intval($origin / 3600);
		$minute = intval(round($origin / 60, 2) % 60);
	
		return $hour . ":" . sprintf('%02d', $minute);
	}	
	
	//================================================================================
	//	IDで勤務区分を検索
	//================================================================================
	function findDivisionAttrByID($id, $timeSheet)
	{
		$timeSheetAttr;
		$count = count($timeSheet);
		
		for($i=0;$i<$count;$i++)
		{
			if($id == $timeSheet[$i]['id'])
			{
				$timeSheetAttr = $timeSheet[$i];
				break;
			}
		}
		
		return $timeSheetAttr;
	}
	
	
	
	
	//================================================================================
	//	エクセル上での退勤時間計算
	//================================================================================			
	private function getExcelEndDate($excelStartDate, $excelEndDate, $default = '')
	{
	
		if(isset($excelStartDate) && isset($excelEndDate))
		{
			
			$stratDate = date('Y-m-d', strtotime($excelStartDate));
			$endDate = date('Y-m-d', strtotime($excelEndDate));
			
			
			if($stratDate === $endDate)
			{
				//	同日ならそのまま時刻を返す
				$result = date('G:i', strtotime($excelEndDate));
			}else{
				
				$endHour = date("H", strtotime($excelEndDate));
				//	日付が異なるなら差分を出したす
				$hour = getIntegrateEndHour($endHour, $stratDate, $endDate) ;	
				//
				$minute = date("i", strtotime($excelEndDate));
				//	
				$result = $hour . ":" . $minute;
			}
		}else if(isset($excelEndDate)){
			$result = getTimeRecorderEndDate($excelEndDate, $default);
		}else{
			
			$result = $default;
		}
		
		return $result;
	}
	
	private function getTimeRecorderEndDate($timeRecorderEndDate, $default = '')
	{
		if (empty($timeRecorderEndDate)) {
			return $default;
		}
	
		$hour   = date("H", strtotime($timeRecorderEndDate));
		$minute = '00';
		if (intval($hour) < 48) {
			$minute = date("i", strtotime($timeRecorderEndDate));
		}
	
		return $hour . ":" . $minute;
	}
	//
	//
	//
	public function readHolidayCSV($path)
	{
	}
}