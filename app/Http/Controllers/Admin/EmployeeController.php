<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Employees;
use App\EmploymentTypes;
use App\EmploymentStatus;
use App\Departments;
use App\DepartmentRelations;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Input;
use Session;
use Cookie;
use App\TimeSheet;

class EmployeeController extends Controller
{
	public function __construct(Router $router)
	{
		$this->adminBeforeFilter();
	}

	public function index()
	{
		$timeSheet = TimeSheet::all();

		$paegCount = Cookie::get('page_count');

		if(is_null($paegCount) === true) $paegCount = 50;

		$data = [];

		$data["employees"] = $this->getEmployeeList($this->page_count, 1);

		$data["emp_status"] = $this->getEmploymentStatus();

		$data["status"] = 1;

		$data["timeSheet"] = $timeSheet;
		
		return view("admin.employee.index", $data);
	}
	//================================================================================
	//
	//	フィルタ付き
	//
	//================================================================================
	public function statusFilter($status)
	{
		$timeSheet = TimeSheet::all();

		$paegCount = Cookie::get('page_count');

		if(is_null($paegCount) === true) $paegCount = 50;

		$data = [];

		$data["employees"] = $this->getEmployeeList($this->page_count, $status);

		$data["emp_status"] = $this->getEmploymentStatus();

		$data["status"] = $status;

		$data["timeSheet"] = $timeSheet;

		return view("admin.employee.index", $data);
	}
	//================================================================================
	//
	//	編集
	//
	//================================================================================
	public function edit($emp_no)
	{
		if($this->isExistNumber($emp_no) === true)
		{
			//
			$employee = Employees::find($emp_no);
			//
			$data = [];
			//
			//$data["timeSheet"] = TimeSheet::geListOrderByWorkingHour();
			$data["timeSheet"] = TimeSheet::all();
			//
			$data["mode"] = "update";
			//
			$data["employee"] = $employee;
			//
			$data["emp_types"] = $this->getEmploymentTypes();
			//
			$data["emp_status"] = $this->getEmploymentStatus();
			//
			$data["departments"] = $this->getDepartments();
			//
			$data["department_relations"] = $this->getDepartmentRelation($emp_no);
			//
			$data["positions"] = $this->getPositions();
			//
			return view("admin.employee.form", $data);
			//
		}else{
			$success = false;

			$message = "社員番号が存在しません。";
		}
	}
	//================================================================================
	//
	//	社員更新
	//
	//================================================================================
	public function update(Request $request)
	{
		$emp_no = $request->input("emp_no");

		$employee = Employees::find($emp_no);

		if(is_null($employee) === true)
		{
			$success = false;
			$message = "社員が存在しません。";
		}else{
			//	社員証番号
			$employee->felica_id = $request->input("felica_id");
			//	姓
			$employee->last_name = $request->input("last_name");
			//	名
			$employee->first_name = $request->input("first_name");
			//	メールアドレス
			$employee->mail = $request->input("mail");
			//	雇用形態
			$employee->emp_type = $request->input("emp_type");
			//	状態
			$employee->status = $request->input("status");
			//	勤怠管理フラグ
			$employee->attendance_flg = $request->input("attendance_flg");
			//	メール送信フラグ
			$employee->send_mail_flg = $request->input("send_mail_flg");
			//	初期勤怠区分
			$employee->default_division_id = $request->input("default_division_id");
			//	登録日
			$employee->reg_date = date('Y-m-d', strtotime($request->input("reg_date")));
			//	退職日の存在確認
			if($request->input("resign_date") === "")
			{
				$employee->resign_date = NULL;
			}else{
				//	退職日
				$employee->resign_date = date('Y-m-d', strtotime($request->input("resign_date")));
			}
			
			//
			$dr = new DepartmentRelations;
			if (!$departmentRelations = $dr->findOneByEmpNo($emp_no)) {
				 $departmentRelations = new DepartmentRelations;
				 $departmentRelations->emp_no = $emp_no;
			}
			//
			$departmentRelations->department_id = $request->input("department_id");
			$departmentRelations->position_id = $request->input("position_id");
			//

			if($employee->save() === true && $departmentRelations->save() === true)
			{
				$success = true;
				$message = "社員情報を更新しました。";
			}else{
				$success = false;
				$message = "社員情報を更新できませんでした。";
			}
		}
		return response()->json(["success"=>$success, "message"=>$message]);
	}


	//================================================================================
	//
	//	社員削除
	//
	//================================================================================
	public function remove(Request $request)
	{
		$emp_no = $request->input("emp_no");

		if($this->isExistNumber($emp_no) === true)
		{
			//$employee = Employees::where("emp_no", "=", $emp_no)->first();

			$employee = Employees::find($emp_no);

			if(is_null($employee) === true)
			{
				$success = false;
				$message = "社員番号が存在しません。";
			}else{
				//
				$employee->del_flg = 1;
				//
				$employee->del_date = \DB::raw("CURRENT_TIMESTAMP");
				//
				if($employee->save() === true)
				{
					$success = true;
					$message = "社員を削除しました。";
				}else{
					$success = false;
					$message = "社員を削除できませんでした。";
				}

			}
		}else{

			$success = false;
			$message = "社員番号が存在しません。";
		}

		return response()->json(["success"=>$success, "message"=>$message]);
	}
	//================================================================================
	//
	//	社員番号が存在するか判定
	//
	//================================================================================
	private function isExistNumber($emp_no)
	{
		//$result = Employees::where('emp_no', '=', $emp_no)->first();

		$result = Employees::find($emp_no);

		return $result !== null;
	}
	/*
	//================================================================================
	//
	//	編集フォーム
	//
	//================================================================================
	public function form($emp_no)
	{
		$data = [];

		if(is_null($emp_no))
		{
			$data["type"] = "new";
		}else{
			$data["emp_no"] = $emp_no;
			$data["type"] = "update";
		}

		return view("admin.employee.form", $data);
	}*/
	//================================================================================
	//
	//	社員リスト取得
	//
	//================================================================================
	private function getEmployeeList($perPage, $status)
	{
		if($status === 0)
		{
			return \DB::table("employees")
			->where('del_flg', '=', 0)
			->leftJoin("employment_type", "emp_type", "=", "employment_type.id")
			->leftJoin("employment_status", "status", "=", "employment_status.id")
			->selectRaw('emp_no, last_name, first_name, felica_id, mail, emp_type, status, attendance_flg, send_mail_flg, default_division_id, reg_date, resign_date, upd_date, del_date, del_flg, employment_status.name as status_name, employment_type.name as type_name')
			->paginate($perPage);
		}else{
			return \DB::table("employees")
			->where('del_flg', '=', 0)
			->where('status', '=', $status)
			->leftJoin("employment_type", "emp_type", "=", "employment_type.id")
			->leftJoin("employment_status", "status", "=", "employment_status.id")
			->selectRaw('emp_no, last_name, first_name, felica_id, mail, emp_type, status, attendance_flg, send_mail_flg, default_division_id, reg_date, resign_date, upd_date, del_date, del_flg, employment_status.name as status_name, employment_type.name as type_name')
			->paginate($perPage);
		}
	}
	//================================================================================
	//
	//	雇用形態リスト取得
	//
	//================================================================================
	private function getEmploymentTypes()
	{
		return EmploymentTypes::all();
	}
	//================================================================================
	//
	//	雇用状態リスト取得
	//
	//================================================================================
	private function getEmploymentStatus()
	{
		return EmploymentStatus::all();
	}
	//================================================================================
	//
	//	部署リスト取得
	//
	//================================================================================
	private function getDepartments()
	{
		return Departments::all();
	}
	//================================================================================
	//
	//	役割リスト取得
	//
	//================================================================================
	private function getPositions()
	{
		return \Config::get('app.positionType');
	}
	//================================================================================
	//
	//	役割リスト取得
	//
	//================================================================================
	private function getDepartmentRelation($emp_no)
	{
		//return DepartmentRelations::where('emp_no', $emp_no)->get();

		return \DB::table("department_relations")->where('emp_no', '=', $emp_no)->first();
	}
}