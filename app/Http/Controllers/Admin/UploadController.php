<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Attendances;
use App\Departments;
use App\DepartmentRelations;
use App\Employees;
use App\TimeSheet;
use App\Lib\AMS2JIRAUtility;
use App\Lib\CurlUtility;
use App\Lib\DateUtility;
use App\Lib\ExcelFormula;
use App\Lib\TempoAPI;

class UploadController extends Controller
{
    public function getUpload()
    {
		$employees = new Employees;
		$userList  = $employees->findListIdAndNameWithoutObservationUser();		
		
        return view('admin.upload.index' , compact('userList'));
    }
	//================================================================================
	//
	//	JIRAに実働時間のアップロード
	//
	//================================================================================	
	public function uploadWorkingHours2JIRA(Request $request)
	{	
		$env = TempoAPI::PROD_ENV;
		
		$emp_no = $request->get('emp_no');
		//----------------------------------------------------------------------
		//	昨日を取得
		//----------------------------------------------------------------------		
		$yesterday = date('Y-m-d', strtotime('-1 day'));
		//----------------------------------------------------------------------
		//	社員一覧を取得
		//----------------------------------------------------------------------		
		/*$employeeList = \DB::table("employees")
			->where('del_flg', '=', 0)
			->where('attendance_flg', '=', 1)
			->where('status', '=', 1)
			->orWhere('status', '=', 2)
			->get();*/
		$empInfo = \DB::table("employees")
			->where('emp_no', '=', $emp_no)
			->first();
		
		if(isset($empInfo) === true)
		{
			$name = substr($empInfo->mail, 0, strpos($empInfo->mail, "@"));
		
			$timeSheet	= TimeSheet::all();

			TempoAPI::getJSESSIONID($env);
			
			$period = TempoAPI::getStart2End($yesterday);
			
			$ams2JIRA = new AMS2JIRAUtility();

			$curlsInfo = $ams2JIRA->getCURLsByCompareExistPlans($env, $emp_no, $name, $empInfo->default_division_id, $period["start"], $period["end"], $timeSheet, true);
		
			$message = "JIRAに実務時間をアップロードしました。";
			$success = true;
			
			if(count($curlsInfo["curls"]) > 0)
			{
				$result = CurlUtility::asyncExecCurls($curlsInfo["curls"]);
				
				
				$message = "勤怠時間の更新はありません。";
				
			}else{
				
				$message = "勤怠時間の更新はありません。";

			}
		}else{
			
			$message = "社員情報がありません";
			$success = true;
		}
		
		return response()->json(["success"=>$success, "message"=>$message, "html"=>implode("<br>", $curlsInfo["logs"])]);
	}
}