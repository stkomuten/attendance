<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\AuthenticatedMachines;
use App\Employees;
use App\Attendances;
use Monolog\Logger as MonologLogger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Formatter\LineFormatter;

class Api2Controller extends Controller
{

    const ACTION_TYPE_IN        = 'S';
    const ACTION_TYPE_OUT       = 'E';
    const ACTION_TYPE_BREAK_IN  = 'SB';
    const ACTION_TYPE_BREAK_OUT = 'EB';


    /**
     * サーバー時間を返す
     */
    public function servertime()
    {
        $body = array(
            'datetime' => date(DATE_ATOM, time())
        );

        return new JsonResponse($body, 200);
    }


    /**
     * 出退勤API(単発)
     */
    public function attendance(Request $request)
    {
        if (!$_data = $request->get('data')) {
            return new JsonResponse($this->getResponseBody("", 'DATA_PARAMETER_ERROR'), 400);
        }

        $requestData = null;
        $originContents = $this->decryptRequestContent($_data);
        parse_str($originContents, $requestData);

        $this->writeLog(__FUNCTION__, $requestData);

        $localId    = empty($requestData['ID']) ? "" : $requestData['ID'];
        $felicaId   = isset($requestData['felica_id']) ? $requestData['felica_id'] : null;
        $actionType = isset($requestData['type']) ? $requestData['type'] : null;
        $hash       = isset($requestData['hash']) ? $requestData['hash'] : null;

        if (empty($felicaId) || empty($actionType) || empty($hash)) {
            return new JsonResponse($this->getResponseBody($localId, 'PARAMETER_ERROR'), 400);
        }

        // 指定のタイプでなければエラー
        if (!in_array($actionType, array(self::ACTION_TYPE_IN, self::ACTION_TYPE_OUT, self::ACTION_TYPE_BREAK_IN, self::ACTION_TYPE_BREAK_OUT))) {
            return new JsonResponse($this->getResponseBody($localId, 'TYPE_ERROR'), 400);
        }

        // 登録済みの機体からのアクセスでなければエラー
        if (!(bool)AuthenticatedMachines::where('hash', $hash)->count()) {
            return new JsonResponse($this->getResponseBody($localId, 'MACHINE_ERROR'), 400);
        }

        // 登録済みのfelica_idでなければエラー
        if (!$employees = Employees::where('felica_id', $felicaId)->first()) {
            return new JsonResponse($this->getResponseBody($localId, 'USER_ERROR'), 400);
        }

        // 実行するメソッドを取得
        $methodName = $this->getExecuteMethodName($actionType);
        list($message, $status) = $this->$methodName($felicaId, $employees->emp_no);

        return new JsonResponse($this->getResponseBody($localId, $message), $status);
    }


    /**
     * 出退勤API(複数)
     */
    public function attendanceMulti(Request $request)
    {
        if (!$_data = $request->get('data')) {
            return new JsonResponse($this->getResponseBody("", 'DATA_PARAMETER_ERROR'), 400);
        }

        $originContents = $this->decryptRequestContent($_data);
        $requestData    = json_decode($originContents, true);
        $machineData    = array();
        $employeeData   = array();
        $responseData   = array();

        $hash        =  $request["hash"];
        $attendances =  $request["attendances"];

        $this->writeLog(__FUNCTION__, $requestData);

        //  機体データを取得
        if (isset($hash))
        {
            // 登録済みの機体からのアクセスでなければエラー
            if((bool)AuthenticatedMachines::where('hash', $hash)->count() === true)
            {
                foreach ($attendances as $data)
                {
                    $localId    = $data['id'];
                    $felicaId   = $data['felica_id'];
                    $actionType = $data['type'];
                    //$hash       = $data['hash'];
                    $createdAt  = $data['datetime'];

                    // 指定のタイプでなければエラー
                    if (!in_array($actionType, array(self::ACTION_TYPE_IN, self::ACTION_TYPE_OUT, self::ACTION_TYPE_BREAK_IN, self::ACTION_TYPE_BREAK_OUT)))
                    {
                        $responseData[] = $this->getResponseBody($localId, 'TYPE_ERROR');
                        continue;
                    }

                    // ユーザーデータを取得
                    if (!isset($employeeData[$felicaId]))
                    {
                        $employeeData[$felicaId] = Employees::where('felica_id', $felicaId)->first();
                    }

                    // 登録済みのfelica_idでなければエラー
                    if (!$employees = $employeeData[$felicaId])
                    {
                        $responseData[] = $this->getResponseBody($localId, 'USER_ERROR');
                        continue;
                    }

                    // 実行するメソッドを取得
                    $methodName = $this->getExecuteMethodName($actionType);

                    // レスポンスを取得
                    list($message, $status) = $this->$methodName($felicaId, $employees->emp_no, $createdAt);
                    $responseData[] = $this->getResponseBody($localId, $message);
                }
            }else{
                $responseData[] = $this->getResponseBody("", 'MACHINE_ERROR');
            }
        }

        return new JsonResponse($responseData);
    }



    /**
     * 出勤実行
     */
    private function attendanceIn($felicaId, $employeeId, $createdAt = null)
    {
        $action  = __FUNCTION__;

        return $this->_execute($felicaId, $employeeId, $createdAt, $action);
    }


    /**
     * 退勤実行
     */
    private function attendanceOut($felicaId, $employeeId, $createdAt = null)
    {
        $action  = __FUNCTION__;

        return $this->_execute($felicaId, $employeeId, $createdAt, $action);
    }


    /**
     * 休憩入実行
     */
    private function breakIn($felicaId, $employeeId, $createdAt = null)
    {
        $action  = __FUNCTION__;
        $status  = 200;

        return $this->_execute($felicaId, $employeeId, $createdAt, $action, $status);
    }


    /**
     * 休憩戻実行
     */
    private function breakOut($felicaId, $employeeId, $createdAt = null)
    {
        $action  = __FUNCTION__;
        $status  = 200;

        return $this->_execute($felicaId, $employeeId, $createdAt, $action, $status);
    }


    /**
     * 実行メソッド
     */
    private function _execute($felicaId, $employeeId, $createdAt, $action, $status = 201)
    {
        $attendances = new Attendances();
        $message = "OK";

        try {

            \DB::beginTransaction();

            // 重複登録チェック
            if ($attendances->isDuplicateData($employeeId, $createdAt)) {
                throw new \Exception("DUPLICATE_REGISTER_ERROR", 400);
            }

            // その日のうちの登録かどうかをチェック
            $methodName  = "isExistsTodayData" . ucfirst($action);
            if ($attendances->$methodName($employeeId, $createdAt)) {
                throw new \Exception("OK", 200);
            }

            // 保存実行
            $methodName  = "save" . ucfirst($action);
            if (!$attendances->$methodName($employeeId, $createdAt)) {
                throw new \Exception("SAVE_ERROR", 500);
            }

            \DB::commit();

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $status  = $e->getCode();
            \DB::rollback();
        } catch (\ErrorException $e) {
            $message = $e->getMessage();
            $status  = $e->getCode();
            \DB::rollback();
        }

        return array($message, $status);
    }


    /**
     * アクションタイプによって実行するメソッドを取得
     */
    private function getExecuteMethodName($actionType)
    {
        $types = array(
            self::ACTION_TYPE_IN          => 'attendanceIn'
            , self::ACTION_TYPE_OUT       => 'attendanceOut'
            , self::ACTION_TYPE_BREAK_IN  => 'breakIn'
            , self::ACTION_TYPE_BREAK_OUT => 'breakOut'
        );

        return $types[$actionType];
    }


    /**
     * レスポンス用のBodyを生成
     */
    private function getResponseBody($id, $message)
    {
        $body = array(
            "id"     => $id
            , 'type' => $message
        );

        return $body;
    }


    /**
     * パラメータの復号化
     */
    private function decryptRequestContent($base64Data)
    {
        //共通鍵
        $key = "H6UFfCLgfNGnKzRf";

        $size  = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $bytes = base64_decode($base64Data);
        $bytes_size = strlen($bytes);

        $iv  = substr($bytes, $bytes_size - $size);
        $src = substr($bytes, 0, $bytes_size - $size);
        $decryptText = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $src, MCRYPT_MODE_ECB, $iv);

        return $decryptText;
    }


    /**
     * ログを取る
     */
    private function writeLog($callMethod, $loggingData)
    {
        $log = new MonologLogger(MonologLogger::INFO);
        $log->pushHandler(
            $handler = new RotatingFileHandler(
                storage_path() . '/logs/api.log'
                , 365
                , MonologLogger::INFO
            )
        );

        $formatter = new LineFormatter("[%datetime%]\t%message%\t%context%\n", null, true, true);
        $handler->setFormatter($formatter);

        $log->addInfo($callMethod, $loggingData);
    }
}
