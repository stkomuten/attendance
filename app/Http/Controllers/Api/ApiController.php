<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\AuthenticatedMachines;
use App\Employees;
use App\Attendances;


class ApiController extends Controller
{
    const ACTION_TYPE_IN        = 'S';
    const ACTION_TYPE_OUT       = 'E';
    const ACTION_TYPE_BREAK_IN  = 'SB';
    const ACTION_TYPE_BREAK_OUT = 'EB';


    /**
     * サーバー時間を返す
     */
    public function servertime()
    {
        $body = array(
            'datetime' => date(DATE_ATOM, time())
        );

        return new JsonResponse($body, 200);
    }


    /**
     * 出退勤API(単発)
     */
    public function attendance(Request $request)
    {
        $requestData = null;
        // $originContents = STCrypt::Decrypt(Request::getContent());
        $originContents = $request->getContent();
        parse_str($originContents, $requestData);

        $localId    = empty($requestData['ID']) ? "" : $requestData['ID'];
        $felicaId   = isset($requestData['felica_id']) ? $requestData['felica_id'] : null;
        $actionType = isset($requestData['type']) ? $requestData['type'] : null;
        $hash       = isset($requestData['hash']) ? $requestData['hash'] : null;
		$location		= isset($requestData['location']) ? $requestData['location'] : null;

        if (empty($felicaId) || empty($actionType) || empty($hash)) {
            return new JsonResponse($this->getResponseBody($localId, 'PARAMETER_ERROR'), 400);
        }

        // 指定のタイプでなければエラー
        if (!in_array($actionType, array(self::ACTION_TYPE_IN, self::ACTION_TYPE_OUT, self::ACTION_TYPE_BREAK_IN, self::ACTION_TYPE_BREAK_OUT))) {
            return new JsonResponse($this->getResponseBody($localId, 'TYPE_ERROR'), 400);
        }

        // 登録済みの機体からのアクセスでなければエラー
        if (!(bool)AuthenticatedMachines::where('hash', $hash)->count()) {
            return new JsonResponse($this->getResponseBody($localId, 'MACHINE_ERROR'), 400);
        }

        // 登録済みのfelica_idでなければエラー
        if (!$employees = Employees::where('felica_id', $felicaId)->first()) {
            return new JsonResponse($this->getResponseBody($localId, 'USER_ERROR'), 400);
        }

        // 実行するメソッドを取得
        $methodName = $this->getExecuteMethodName($actionType);
        list($message, $status) = $this->$methodName($felicaId, $employees->emp_no, $location);

        return new JsonResponse($this->getResponseBody($localId, $message), $status);
    }


    /**
     * 出退勤API(複数)
     */
    public function attendanceMulti(Request $request)
    {
        $originContents = $request->getContent();
        $requestData    = json_decode($originContents, true);
        $machineData    = array();
        $employeeData   = array();
        $responseData   = array();

        $hash        =  $request["hash"];
        $attendances =  $request["attendances"];

        //  機体データを取得
        if (isset($hash))
        {
            // 登録済みの機体からのアクセスでなければエラー
            if((bool)AuthenticatedMachines::where('hash', $hash)->count() === true)
            {
                foreach ($attendances as $data)
                {
                    $localId    = $data['id'];
                    $felicaId   = $data['felica_id'];
                    $actionType = $data['type'];
                    //$hash       = $data['hash'];
                    $createdAt  = $data['datetime'];

                    // 指定のタイプでなければエラー
                    if (!in_array($actionType, array(self::ACTION_TYPE_IN, self::ACTION_TYPE_OUT, self::ACTION_TYPE_BREAK_IN, self::ACTION_TYPE_BREAK_OUT)))
                    {
                        $responseData[] = $this->getResponseBody($localId, 'TYPE_ERROR');
                        continue;
                    }

                    // ユーザーデータを取得
                    if (!isset($employeeData[$felicaId]))
                    {
                        $employeeData[$felicaId] = Employees::where('felica_id', $felicaId)->first();
                    }

                    // 登録済みのfelica_idでなければエラー
                    if (!$employees = $employeeData[$felicaId])
                    {
                        $responseData[] = $this->getResponseBody($localId, 'USER_ERROR');
                        continue;
                    }

                    // 実行するメソッドを取得
                    $methodName = $this->getExecuteMethodName($actionType);

                    // レスポンスを取得
                    list($message, $status) = $this->$methodName($felicaId, $employees->emp_no, $createdAt);
                    $responseData[] = $this->getResponseBody($localId, $message);
                }
            }else{
                $responseData[] = $this->getResponseBody($localId, 'MACHINE_ERROR');
            }
        }

        return new JsonResponse($responseData);
    }



    /**
     * 出勤実行
     */
    private function attendanceIn($felicaId, $employeeId, $location, $createdAt = null)
    {
        $action  = __FUNCTION__;

        return $this->_execute($felicaId, $employeeId, $location, $createdAt, $action);
    }


    /**
     * 退勤実行
     */
    private function attendanceOut($felicaId, $employeeId, $location, $createdAt = null)
    {
        $action  = __FUNCTION__;

        return $this->_execute($felicaId, $employeeId, $location, $createdAt, $action);
    }


    /**
     * 休憩入実行
     */
    private function breakIn($felicaId, $employeeId, $location, $createdAt = null)
    {
        $action  = __FUNCTION__;
        $status  = 200;

        return $this->_execute($felicaId, $employeeId, $location, $createdAt, $action, $status);
    }


    /**
     * 休憩戻実行
     */
    private function breakOut($felicaId, $employeeId, $location, $createdAt = null)
    {
        $action  = __FUNCTION__;
        $status  = 200;

        return $this->_execute($felicaId, $employeeId, $location, $createdAt, $action, $status);
    }


    /**
     * 実行メソッド
     */
    private function _execute($felicaId, $employeeId, $location, $createdAt, $action, $status = 201)
    {
        $attendances = new Attendances();
        $message = "OK";

        try {

           
			/*
			// データ更新チェック
            if ($attendances->isInUpdateInterval($employeeId)) {
                throw new \Exception("UPDATE_INTERVAL_ERROR", 400);
            }
			*/
            // 重複登録チェック
            if ($attendances->isDuplicateData($employeeId, $createdAt)) {
                throw new \Exception("DUPLICATE_REGISTER_ERROR", 400);
            }
			
			 \DB::beginTransaction();
			
            // その日のうちの登録かどうかをチェック
            $methodName  = "isExistsTodayData" . ucfirst($action);
            if ($attendances->$methodName($employeeId, $createdAt)) {
                throw new \Exception("OK", 200);
            }

            // 保存実行
            $methodName  = "save" . ucfirst($action);
            if (!$attendances->$methodName($employeeId, $createdAt, $location)) {
                throw new \Exception("SAVE_ERROR", 500);
            }

            \DB::commit();

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $status  = $e->getCode();
            \DB::rollback();
        } catch (\ErrorException $e) {
            $message = $e->getMessage();
            $status  = $e->getCode();
            \DB::rollback();
        }

        return array($message, $status);
    }


    /**
     * アクションタイプによって実行するメソッドを取得
     */
    private function getExecuteMethodName($actionType)
    {
        $types = array(
            self::ACTION_TYPE_IN          => 'attendanceIn'
            , self::ACTION_TYPE_OUT       => 'attendanceOut'
            , self::ACTION_TYPE_BREAK_IN  => 'breakIn'
            , self::ACTION_TYPE_BREAK_OUT => 'breakOut'
        );

        return $types[$actionType];
    }


    /**
     * レスポンス用のBodyを生成
     */
    private function getResponseBody($id, $message)
    {
        $body = array(
            "id"     => $id
            , 'type' => $message
        );

        return $body;
    }
}
