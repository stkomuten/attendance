<?php

namespace App\Http\Controllers\Auth;

use App\Employees;
use Validator;
use Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordController extends Controller
{
    /**
     * パスワードリセットのためのEmail入力indexページ
     */
    public function getEmail()
    {
        return view('auth.email');

    }


    /**
     * パスワードリセットのためのコミット & 送信ページ
     */
    public function postEmail(Request $request)
    {
        $mail = $request->get('email');
        $validator = $this->emailValidator($mail);
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $time  = hash('md5', time());
        $rand  = hash('md5', mt_rand());
        $token = hash('sha256', hash('sha256', $mail) . $time . $rand);
        Cache::add($token, $mail, 10);
        Cache::add('sendComplete', true, 1);


		//$url = empty($_SERVER["HTTPS"]) ? "http://" : "https://";
		$url = "https://";
		$url .= $_SERVER["HTTP_HOST"];

        //\Mail::plain('email.password_reset', ['token' => $token, 'url' => $request->getSchemeAndHttpHost()], function($message) use($mail) {
		\Mail::plain('email.password_reset', ['token' => $token, 'url' => $url ], function($message) use($mail) {
            $message->from('attendance@st-komuten.jp', 'STK ATTENDANCE SYSTEM');
            $message->subject('パスワードリセットのお願い');
            $message->to($mail);
        });

        return redirect('/password/email/complete');
    }


    /**
     * パスワードリセットのためのメール送信完了ページ
     */
    public function emailComplete()
    {
        if (!Cache::has('sendComplete')) {
            return redirect('/admin');
        }

        Cache::forget('sendComplete');

        return view('auth.email_send');
    }


    /**
     * パスワードリセットindexページ
     */
    public function getReset(Request $request)
    {
        $postURL = '/admin/user/password/reset';
        $mail    = null;

        // メールURLからの再設定
        if ($request->route()->getName() === 'auth.password.reset.mail.view') {
            if (!$token = $request->get('token')) {
                // トークンがなければエラー
                return view('auth.email_error');
            }

            if (!Cache::has($token)) {
                // トークンのキャッシュがなければエラー
                return view('auth.email_error');
            }

            $mail    = Cache::get($token);
            $postURL = '/password/reset';
        }

        return view('auth.reset', compact('postURL', 'mail'));
    }


    /**
     * パスワードリセットコミットアクション
     */
    public function postReset(Request $request)
    {
        $data = $request->all();

        $validator = $this->resetValidator($data);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($this->update($request->all())) {

            if (!isset($data['mail'])) {
                \Auth::logout();
            }

            return view('auth.reset_complete');
        }

        return redirect('/admin');
    }


    /**
     * パスワードリセットメール送信バリデータ
     */
    protected function emailValidator($email)
    {
        $data = [
            'email' => $email
        ];

        $rules = [
            'email' => 'required|email|exists:employees,mail'
        ];

        $messages = [
            'email.required' => 'メールアドレスが入力されていません。'
            , 'email.email'  => 'メールアドレスの形式が不正です。'
            , 'email.exists' => '登録されているメールアドレスではありません。'
        ];

        return Validator::make($data, $rules, $messages);
    }


    /**
     * リセットバリデータ
     */
    protected function resetValidator(array $data)
    {
        return Validator::make($data, [
            'password'   => 'required|confirmed|min:6'
        ]);
    }


    /**
     * パスワード更新
     */
    protected function update(array $data)
    {
        if (isset($data['mail'])) {
            $user = Employees::where('mail', $data['mail'])->first();
        } else {
            $user = \Auth::user();
        }

        $user->password = bcrypt($data['password']);

        return $user->save();
    }
}
