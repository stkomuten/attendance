<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Employees;
use App\EmploymentStatus;
use App\EmploymentTypes;
use App\Departments;
use App\DepartmentRelations;
use App\TimeSheet;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class RegisterController extends Controller
{
    private $redirectPath        = '/admin/employee';
    private $username            = 'mail';

    use AuthenticatesAndRegistersUsers;


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        // // なぜかconstruct()でリダイレクトしなかったから、ここでやる
        // if (\Auth::user()->isSystemAdministrator() === false) {
        //     return redirect($this->redirectPath());
        // }

        $statusList = array();
        foreach (EmploymentStatus::all() as $val) {
            $statusList[$val->id] = $val->name;
        }

        $typeList = array();
        foreach (EmploymentTypes::all() as $val) {
            $typeList[$val->id] = $val->name;
        }

        $departmentList = array();
        foreach (Departments::all() as $val) {
            $departmentList[$val->id] = $val->name;
        }

        $positionList   = \Config::get('app.positionType');
		
		$timeSheet	= TimeSheet::all();

        return view('auth.register', compact('statusList', 'typeList', 'positionList', 'departmentList', 'timeSheet'));
    }


    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        // // なぜかconstruct()でリダイレクトしなかったから、ここでやる
        // if (\Auth::user()->isSystemAdministrator() === false) {
        //     return redirect($this->redirectPath());
        // }

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->create($request->all());

        return redirect($this->redirectPath());
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'emp_no'       => 'required|max:255'
            , 'last_name'  => 'required|max:255'
            , 'first_name' => 'required|max:255'
            , 'felica_id'  => 'required|max:255'
            , 'mail'       => 'required|email|max:255|unique:employees'
            , 'password'   => 'required|confirmed|min:6'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        try {
            \DB::beginTransaction();

            $result = Employees::create([
                'emp_no'       => $data['emp_no']
                , 'last_name'  => $data['last_name']
                , 'first_name' => $data['first_name']
                , 'felica_id'  => $data['felica_id']
                , 'mail'       => $data['mail']
                , 'password'   => bcrypt($data['password'])
                , 'emp_type'   => $data['emp_type']
                , 'status'     => $data['status']
				, 'attendance_flg'     => $data['attendance_flg']
				, 'send_mail_flg'     => $data['send_mail_flg']
				, 'default_division_id'	=> $data['default_division_id']
            ]);

            $relations = new DepartmentRelations();
            $relations->department_id = $data['department_id'];
            $relations->emp_no        = $data['emp_no'];
            $relations->position_id   = $data['position_id'];
            $relations->save();

            \DB::commit();

        } catch (\Exception $e) {
            $result = false;
            \DB::rollback();
        }

        return $result;
    }
}
