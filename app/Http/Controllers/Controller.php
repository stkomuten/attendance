<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $page_count = 30;

    protected function adminBeforeFilter()
    {
    	$this->page_count = empty($_COOKIE['page_count']) ? $this->page_count : $_COOKIE['page_count'];
    }
}
