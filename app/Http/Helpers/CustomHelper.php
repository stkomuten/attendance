<?php


/**
 * 出勤時間(タイムカード)
 */
function getTimeRecorderStartDate($timeRecorderStartDate, $default = '--')
{
	if (empty($timeRecorderStartDate)) {
		return $default;
	}

	$timeRecorderStartDate = date("G:i", strtotime($timeRecorderStartDate));

	return $timeRecorderStartDate;
}


/**
 * 退勤時間(タイムカード)
 */
function getTimeRecorderEndDate($timeRecorderEndDate, $default = '--')
{
	if (empty($timeRecorderEndDate)) {
		return $default;
	}

	$hour   = date("H", strtotime($timeRecorderEndDate));
	$minute = '00';
	
	if (intval($hour) < 48) {
		$minute = date("i", strtotime($timeRecorderEndDate));
	}

	return $hour . ":" . $minute;
}


/**
 * 出勤時間(Excel)
 */
function getExcelStartDate($excelStartDate, $default = '--')
{
	return getTimeRecorderStartDate($excelStartDate, $default);
}


/**
 * 退勤時間(Excel)
 */
/*
function getExcelEndDate($excelEndDate, $default = '--')
{
	return getTimeRecorderEndDate($excelEndDate, $default);
}
*/


/**
 *	Time型表記を変換
 */
function convertWorkingHourDisplayTime($time)
{
	$hour = substr($time,0,2);
	$minitus = substr($time,3,2);
	$minitus = $minitus === "30" ? ".5" : "";
	
	return  (int)$hour . $minitus . "時間";
}


/**
 *	Time型表記を変換
 */
function convertDisplayTime($time)
{
	$hour = substr($time,0,2);
	$minutes = substr($time,3,2);

	return ((int)$hour < 10 ? "0" : "") . (int)$hour . ":" . $minutes;
}

/**
 *	休憩表示
 */
function convertBreakDisplayTime($breakTime)
{
	$result = "休憩なし";
	
	if($breakTime === "01:00:00")
	{
		$result = "休憩1H";
	}
	
	return $result;
}




function getExcelEndDate($excelStartDate, $excelEndDate, $default = '--')
{
	
	if(isset($excelStartDate) && isset($excelEndDate))
	{
		
		$stratDate = date('Y-m-d', strtotime($excelStartDate));
		$endDate = date('Y-m-d', strtotime($excelEndDate));
		
		
		if($stratDate === $endDate)
		{
			//	同日ならそのまま時刻を返す
			$result = date('G:i', strtotime($excelEndDate));
		}else{
					
			$endHour = date("H", strtotime($excelEndDate));
			
			//	日付が異なるなら差分を出したす
			$hour = getIntegrateEndHour($endHour, $stratDate, $endDate) ;			
			
			$minute = date("i", strtotime($excelEndDate));
				
			$result = $hour . ":" . $minute;
		}
	}else if(isset($excelEndDate)){
		$result = getTimeRecorderEndDate($excelEndDate, $default);
	}else{
		
		$result = $default;
	}
	
	return $result;
}

/**
 * 24時間積算表示(Excel)
 */
function getIntegrateEndHour($endHour, $startDate, $endDate)
{
	$hour = (strtotime($endDate) - strtotime($startDate)) / (3600 * 24);
	return 24 * $hour + $endHour;
}

/**
 * 実働時間(Excel)
 */
function getExcelOriginWorkDate($excelStartDate, $excelEndDate, $timeSheetAttrs, $default = '--')
{
	if (empty($excelStartDate) || empty($excelEndDate) || empty($timeSheetAttrs)) {
		return $default;
	}
	//	出勤時間の秒を切り捨て
	$excelStartDate = date('Y-m-d H:i', strtotime($excelStartDate));
	//	退勤時間の秒を切り捨て
	$excelEndDate = date('Y-m-d H:i', strtotime($excelEndDate));
	//
	microtime(true);
	//	
	$startDate = date('Y-m-d', strtotime($excelStartDate));
	
	$defaultTimeStamp =  strtotime("1970-01-01");
	
	//	開始日の9時を生成
	$startDateTime = strtotime($startDate . " " . $timeSheetAttrs['opening_time']);

	//	休憩時間算出始まり
	$breakTimeStartDate = strtotime("1970-01-01 " . $timeSheetAttrs['break_time_start']) - $defaultTimeStamp;
	
	$breakTimeSec = strtotime("1970-01-01 " . $timeSheetAttrs['break_time']) - $defaultTimeStamp;
	
	//	開始日時が9時以上か判定
	if(strtotime($excelStartDate) > $startDateTime)
	{
		//$startDateTime = strtotime($excelStartDate);
		
		$min = (int)date("i", strtotime($excelStartDate));
		
		if($min === 0) $min = 60;
		
		$minutesLeft = 60 - $min;
		
		$startDateTime = strtotime($excelStartDate . "+" . $minutesLeft ." minute");
	}
	
	$origin = (strtotime($excelEndDate) - $startDateTime);
	
	if($origin < 0) return $default;	
	
	$calBreakTime =  $origin > $breakTimeStartDate ? $breakTimeSec : 0;
	
	$origin -= $calBreakTime;
	
	$hour   = intval($origin / 3600);
	$minute = intval(round($origin / 60, 2) % 60);

	return $hour . ":" . sprintf('%02d', $minute);
}
/**
 * 実働時間の合計
 */
function getExcelOriginWorkDateTotal($OriginWorkDates)
{
	$total = 0;
	
	foreach ($OriginWorkDates as $workDate)
	{
		$total += strtotime("1970-01-01 " . $workDate . ":00") - strtotime("1970-01-01");
	}
	
	$hour   = intval($total / 3600);
	$minute = intval(round($total / 60, 2) % 60);

	return $hour . ":" . sprintf('%02d', $minute);
}

//================================================================================
//	残業時間計算 8時間越え
//================================================================================
function getOver8HourWork($originWork, $paidHolidayID, $paidHolidays, $excelStartDate, $excelEndDate, $timeSheetAttrs, $default = '--')
{
	if (empty($originWork) || empty($excelStartDate) || empty($excelEndDate) || empty($timeSheetAttrs))
	{
		return $default;
	}
	//	出勤日付
	$startDate = date('Y-m-d', strtotime($excelStartDate));
	//	有休考慮算出
	$paidholidyHourDate = isset($paidHolidayID) ? strtotime("1970-01-01 " . $paidHolidays[(int)$paidHolidayID[0] - 1]['hour']) : 0;
	//	実働時間 + 有休考慮算出 計算が怪しい…
	$totalWorkHourDate = strtotime($originWork . ":00") + $paidholidyHourDate;
	//	残業時間の始まり
	$overtimeStart = $timeSheetAttrs['overtime_start'];
	
	$overtimeStartDiv = explode(":", $overtimeStart);
	
	$overtimeStartHour = (int)$overtimeStartDiv[0];
	
	$overtimeStartDate = $startDate;
	
	if($overtimeStartHour >= 24)
	{
		$dateCount = floor($overtimeStartHour / 24);

		$overtimeStartDate = date('Y-m-d', strtotime($overtimeStartDate . " +" . $dateCount ." day"));

		$overtimeStartHour = $overtimeStartHour - 24 * $dateCount;
		
		$overtimeStart = sprintf('%02d', $overtimeStartHour) . ":00:00";
	}

	$overtimeStartDate = strtotime($overtimeStartDate . " " . $overtimeStart);
	//	所定労働時間 
	$officialWorkingHourDate = strtotime("1970-01-01 " . $timeSheetAttrs->official_working_hour);
	//	退勤時間
	$endDate = strtotime($excelEndDate);
	
	$result = "0:00";
	
	if($totalWorkHourDate > $officialWorkingHourDate)
	{
		if($endDate > $overtimeStartDate)
		{
			$total = $endDate - $overtimeStartDate;
			
			$hour   = intval($total / 3600);
			$minute = intval(round($total / 60, 2) % 60);
			$result = $hour . ":" . sprintf('%02d', $minute);
		}
	}
	
	return $result;
}

//================================================================================
//	実働時間の過不足バリデーション
//================================================================================
function validateOverOfficialWorkingHour($originWorkingHour, $officialWorkingHour, $paidHoliday1Hour, $paidHoliday2Hour)
{
	//		実働時間
	$originWorkingHourDate = strtotime("1970-01-01 " .  $originWorkingHour . "UTC");
	//		所定労働時間 
	$officialWorkingHourDate = strtotime("1970-01-01 " . $officialWorkingHour . "UTC");
	//		
	$paidHoliday1HourDate = isset($paidHoliday1Hour) ? strtotime("1970-01-01 " . $paidHoliday1Hour . "UTC") : 0;
	//
	$paidHoliday2HourDate = isset($paidHoliday2Hour) ? strtotime("1970-01-01 " . $paidHoliday2Hour . "UTC") : 0;
	//
	$result = true;
	//
	if($originWorkingHourDate < $officialWorkingHourDate)
	{
		$result = ($originWorkingHourDate + $paidHoliday1HourDate + $paidHoliday2HourDate) >= $officialWorkingHourDate;
	}
	
	return $result;
}
//================================================================================
//		勤務区分のカラー取得
//================================================================================
function getWorkingHourColor($officialWorkingHour, $breakTime)
{	
	$result = "#FF0000";
	
	if($officialWorkingHour === "08:00:00")
	{
		$result = "#448aca";
	
	}else if($officialWorkingHour === "07:00:00" || $officialWorkingHour === "07:30:00"){
	
		$result = "#47c9c4";
	
	}else if($officialWorkingHour === "06:00:00" || $officialWorkingHour === "06:30:00"){
		
		if($breakTime === "01:00:00")
		{
			$result = "#5edf7a";
		}else{
			$result = "#a7d100";			
		}
	}else if($officialWorkingHour === "05:00:00" || $officialWorkingHour === "05:30:00"){
	
		$result = "#f3c200";
	}else if($officialWorkingHour === "04:00:00"){
	
		$result = "#f19149";
	}else if($officialWorkingHour === "03:00:00"){
	
		$result = "#e5004f";
	}

	return $result;
}



//================================================================================
//	IDで勤務区分を検索
 //================================================================================
function findDivisionAttrByID($id, $timeSheet)
{
	$timeSheetAttr;
	$count = count($timeSheet);
	
	for($i=0;$i<$count;$i++)
	{
		if($id == $timeSheet[$i]['id'])
		{
			$timeSheetAttr = $timeSheet[$i];
			break;
		}
	}
	
	return $timeSheetAttr;
}
//================================================================================
//	有給休暇のカラー取得
//================================================================================
function getPaidHolidayColor($id)
{	
	$result = "#FF0000";
	
	if($id === 2 || $id === 3)
	{
		$result = "#f19149";
	
	}else if($id === 4 || $id === 5 || $id === 6 || $id === 7 || $id === 8 || $id === 9){
	
		$result = "#f3c200";
	
	}else if($id === 10 || $id === 11 || $id === 12 || $id === 13){
		
		$result = "#5edf7a";
		
	}else if($id === 14 || $id === 15 || $id === 16 || $id === 17 || $id === 18 || $id === 19){
	
		$result = "#448aca";
	}else if($id === 20 || $id === 21 || $id === 22){
	
		$result = "#b36bca";
	}


	return $result;
}

//================================================================================
//	曜日インデックス取得
//================================================================================
function getDayIndex($year, $month, $day)
{
	return date('w', mktime(0, 0, 0, $month, $day, $year));
}
//================================================================================
//	日本語表記 曜日取得
//================================================================================
function getJpDay($N)
{
	if($N === "1")
	{
		$result = "月";
	
	}else if($N === "2"){
	
		$result = "火";
	
	}else if($N === "3"){
		
		$result = "水";
		
	}else if($N === "4"){
	
		$result = "木";
	}else if($N === "5"){
	
		$result = "金";
	}else if($N === "6"){
	
		$result = "土";
	}else if($N === "7"){
	
		$result = "日";
	}
	
	return $result;
}

//================================================================================
//	有給申請 状態取得
//================================================================================
function getJpPaidHolidayStatus($id)
{
	if($id === 0)
	{
		$result = "申請";
	
	}else if($id === 1){
	
		$result = "承認";
	
	}else if($id === 2){
		
		$result = "承認取消";
		
	}else if($id === 3){
	
		$result = "却下";
		
	}else if($id === 4){
		
		$result = "申請取下";
		
	}else if($id === 5){
	
		$result = "削除";
	}
	
	return $result;
}
//================================================================================
//	有給申請 状態ボタンクラス取得
//================================================================================
function getPaidHolidayStatusColor($id)
{
	if($id === 0)
	{
		$result = "#2271BD";
	
	}else if($id === 1){
	
		$result = "#79D804";
	
	}else if($id === 2){
		
		$result = "#F41C54";
		
	}else if($id === 3){
	
		$result = "#FF9F00";
		
	}else if($id === 4){
	
		$result = "#FBD506";
	
	}else if($id === 5){
	
		$result = "#FF9F00";
	}
	
	return $result;
}
//================================================================================
//		役職名取得
//================================================================================
function getPositionName($id)
{
	if($id === App\DepartmentRelations::ID_POSITION_NORMAL)
	{
		$result = "一般";
	
	}else if($id === App\DepartmentRelations::ID_POSITION_MANAGER){
	
		$result = "上長";
	
	}else if($id === App\DepartmentRelations::ID_POSITION_GENERAL_MANAGER){
		
		$result = "部長";
		
	}
	
	return $result;
}

//================================================================================
//		有給日数残カラー取得
//================================================================================
function getPaidHolidayBalanceColor($dayBalance)
{
	$result = "#00FF00";
	
	if((float)$dayBalance > 5 && (float)$dayBalance <= 10)
	{ 
		$result = "#FFCC00";
	}else if((float)$dayBalance <= 5){
	
		$result = "#FF0000";
	}
	
	return $result;
}
//================================================================================
//		時間有給残カラー取得
//================================================================================
function getPaidHourBalanceColor($hour)
{
	$result = "#00FF00";
	
	if($hour > 5 && $hour <= 10)
	{ 
		$result = "#FFCC00";
	}else if($hour <= 5){	
		$result = "#FF0000";
	}	
	
	return $result;
}