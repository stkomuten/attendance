<?php

namespace App\Http\Middleware;

use Closure;
use App\DepartmentRelations;
use Illuminate\Routing\Router;

class AdminRoleRouteManagement
{
    private $router;
    private $guard;

    public function __construct(Router $r)
    {
        $this->router = $r;
    }


    /**
     * 個人個人が持つ権限によって、ページ遷移を制御する
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if (!$user) {
            return $next($request);
        }

        // ログインや、パスワード変更の場合はチェックしない
        if (!preg_match('/^admin.*$/', $this->router->currentRouteName())) {
            return $next($request);
        }

        // システム管理者または管理部の場合はチェックしない
        if ($user->haveSystemAdministratorRole() || $user->haveManagimentTeamRole()) {
            return $next($request);
        }

        // 一般社員かどうか
        if ($user->haveNormalRole()) {
            // 一般社員の場合は、日別勤怠の表示しかさせない
            if (!preg_match('/^admin\.attendance\.(employees|index|updateNote)$/', $this->router->currentRouteName())) {
                return redirect('/admin');
            }

        // 上長以上の場合
        } else {
            // 上長以上の場合は、勤怠の編集追加などが行える
            if (!preg_match('/^admin\.attendance\.\w+/', $this->router->currentRouteName())) {
                return redirect('/admin');
            }
        }

        return $next($request);
    }
}
