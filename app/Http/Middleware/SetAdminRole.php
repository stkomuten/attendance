<?php

namespace App\Http\Middleware;

use Closure;
use App\DepartmentRelations;
use App\Departments;
use App\AdminRoleMatrices;

class SetAdminRole
{
    /**
     * 管理画面の権限をセットする
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        if ($user) {

            $systemAdministratorUserList = \Config::get('app.systemAdministratorUser');
            if (in_array($user->emp_no, $systemAdministratorUserList)) {
                // システム管理者の場合
                $user->addAdminRole(AdminRoleMatrices::ID_ROLE_SYSTEM_ADMINISTRATOR);
                return $next($request);
            }


            $departmentRelations = new DepartmentRelations;
            if ($departmentRelations->isManagementTeam($user->emp_no)) {
                // 管理部の場合
                $user->addAdminRole(Departments::ID_DEPARTMENT_MANAGEMENT);
                return $next($request);
            }

            // それ以外
            $departmentRelations = new DepartmentRelations;
            if ($relation = $departmentRelations->findOneByEmpNo($user->emp_no)) {
                $user->setMyDepartmentInfo($relation->department_id, $relation->position_id);
                $user->addAdminRole($relation->position_id);
            }
        }

        return $next($request);
    }
}
