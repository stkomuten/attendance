<?php

/**
 * IPフィルタ
 */
Route::filter('ip', function($request) {
    $clientIp = Request::getClientIp();
	/*
    if ($clientIp !== Config::get('app.client_id')) {
    	return Response::json("ACCESS_FORBIDDEN", 403);
    }*/
});

/**
 * APIテスト用ルーティング(削除予定)
 */
Route::group(['namespace' => 'Api', 'prefix' => 'api2', 'before' => 'ip'], function() {

	// 1件ごとの出退勤・休憩
	Route::post('attendance', array(
		'uses' => 'Api2Controller@attendance'
	));

	// 複数の出退勤・休憩
	Route::post('attendance/multi', array(
		'uses' => 'Api2Controller@attendanceMulti'
	));
});



/**
 * API通信用のルーティング
 */
Route::group(['namespace' => 'Api', 'prefix' => 'api', 'before' => 'ip'], function() {

	// 1件ごとの出退勤・休憩
	Route::post('attendance', array(
		'uses' => 'ApiController@attendance'
	));

	// 複数の出退勤・休憩
	Route::post('attendance/multi', array(
		'uses' => 'ApiController@attendanceMulti'
	));

	// サーバータイム取得
	Route::get('servertime', array(
		'uses' => 'ApiController@servertime'
	));
});



/**
 * 管理画面側ルーティング
 */
Route::group(['middleware' => ['auth', 'routeManagement']], function() {

	// 承認・非承認リスト
	Route::get('/admin/attendance/approval/{selectDate}/{status}', array(
		'uses' => 'Admin\ApprovalController@index'
		, 'as' => 'admin.attendance.approval.index'
	));
	Route::get('/admin/attendance/approval/{selectDate}', array(
		'uses' => 'Admin\ApprovalController@index'
		, 'as' => 'admin.attendance.approval.index'
	));
	Route::get('/admin/attendance/approval', array(
		'uses' => 'Admin\ApprovalController@index'
		, 'as' => 'admin.attendance.approval.index'
	));

	// 承認ステータス変更
	Route::post('/admin/attendance/approval', array(
		'uses' => 'Admin\ApprovalController@postUpdate'
		, 'as' => 'admin.attendance.approval.save'
	));


	// 社員別 勤怠記録(ユーザーID, 選択日付)
	Route::get('/admin/attendance/employees/{userId}/{selectDate}', array(
		'uses' => 'Admin\AttendanceController@employees'
		, 'as' => 'admin.attendance.employees'
	));

	// 社員別 勤怠記録(ユーザーID)
	Route::get('/admin/attendance/employees/{userId}', array(
		'uses' => 'Admin\AttendanceController@employees'
		, 'as' => 'admin.attendance.employees'
	));

	// 社員別 勤怠記録
	Route::get('/admin/attendance/employees', array(
		'uses' => 'Admin\AttendanceController@employees'
		, 'as' => 'admin.attendance.employees'
	));

	// 日付別 勤怠記録(選択日付, フィルタ)
	Route::match(['get', 'post'], '/admin/attendance/daily/{selectDate}/{filter}', array(
		'uses' => 'Admin\AttendanceController@daily'
		, 'as' => 'admin.attendance.daily'
	));

	// 日付別 勤怠記録(選択日付)
	Route::match(['get', 'post'], '/admin/attendance/daily/{selectDate}', array(
		'uses' => 'Admin\AttendanceController@daily'
		, 'as' => 'admin.attendance.daily'
	));

	// 日付別 勤怠記録
	Route::match(['get', 'post'], '/admin/attendance/daily', array(
		'uses' => 'Admin\AttendanceController@daily'
		, 'as' => 'admin.attendance.daily'
	));

	//  勤怠記録新規作成
	Route::get('/admin/attendance/create/{empNo}/{targetDate}', array(
		'uses' => 'Admin\AttendanceController@create'
		, 'as' => 'admin.attendance.create'
	));

	//  勤怠記録編集
	Route::get('/admin/attendance/edit/{id}', array(
		'uses' => 'Admin\AttendanceController@edit'
		, 'as' => 'admin.attendance.edit'
	));

	//  勤怠記録削除
	Route::delete('/admin/attendance/delete', array(
		'uses' => 'Admin\AttendanceController@delete'
		, 'as' => 'admin.attendance.delete'
	));

	//  勤怠記録保存
	Route::post('/admin/attendance/save', array(
		'uses' => 'Admin\AttendanceController@save'
		, 'as' => 'admin.attendance.save'
	));

	//  勤怠備考更新
	Route::post('/admin/attendance/updateNote', array(
		'uses' => 'Admin\AttendanceController@updateNote'
		, 'as' => 'admin.attendance.updateNote'
	));


	//  日付別 勤怠記録
	Route::get('/admin/attendance', array(
		'uses' => 'Admin\AttendanceController@index'
		, 'as' => 'admin.attendance.index'
	));

	// 社員情報編集
	Route::get('/admin/employee/edit/{emp_no}', array(
		'uses' => 'Admin\EmployeeController@edit'
		, 'as' => 'admin.employee.edit'
	));

	// 社員情報更新
	Route::post('/admin/employee/update', array(
		'uses' => 'Admin\EmployeeController@update'
		, 'as' => 'admin.employee.update'
	));

	// 社員情報削除
	Route::post('/admin/employee/remove', array(
		'uses' => 'Admin\EmployeeController@remove'
		, 'as' => 'admin.employee.remove'
	));

	// 社員情報(ステータスのフィルタ)
	Route::get('/admin/employee/{status}', array(
		'uses' => 'Admin\EmployeeController@statusFilter'
		, 'as' => 'admin.employee.index'
	));

	// 社員情報
	Route::get('/admin/employee', array(
		'uses' => 'Admin\EmployeeController@index'
		, 'as' => 'admin.employee.index'
	));

	// 部署情報新規作成
	Route::get('/admin/department/create', array(
		'uses' => 'Admin\DepartmentController@create'
		, 'as' => 'admin.department.create'
	));

	// 部署情報修正
	Route::get('/admin/department/edit/{id}', array(
		'uses' => 'Admin\DepartmentController@edit'
		, 'as' => 'admin.department.edit'
	));

	// 部署情報保存
	Route::post('/admin/department/save', array(
		'uses' => 'Admin\DepartmentController@save'
		, 'as' => 'admin.department.save'
	));

	// 部署情報一覧
	Route::get('/admin/department', array(
		'uses' => 'Admin\DepartmentController@index'
		, 'as' => 'admin.department.index'
	));

	// 部署メンバー情報修正
	Route::get('/admin/department/member/edit/{departmentId}', array(
		'uses' => 'Admin\DepartmentMemberController@edit'
		, 'as' => 'admin.department.member.edit'
	));

	// 部署メンバー情報保存
	Route::post('/admin/department/member/save', array(
		'uses' => 'Admin\DepartmentMemberController@save'
		, 'as' => 'admin.department.member.save'
	));

	// 部署メンバー情報一覧情報
	Route::get('/admin/department/member/{departmentId}', array(
		'uses' => 'Admin\DepartmentMemberController@index'
		, 'as' => 'admin.department.member.index'
	));

	// アップロード(view)
	Route::get('/admin/upload', array(
		'uses' => 'Admin\UploadController@getUpload'
		, 'as' => 'admin.upload.index'
	));

	// JIRAへ実働時間をアップロード(ajax)
	Route::post('/admin/upload/uploadWorkingHours2JIRA', array(
		'uses' => 'Admin\UploadController@uploadWorkingHours2JIRA'
		, 'as' => 'admin.upload.uploadWorkingHours2JIRA'
	));
	
	
	
	// ダウンロード(view)
	Route::get('/admin/download/{type}', array(
		'uses' => 'Admin\DownloadController@getDownload'
		, 'as' => 'admin.download.index'
	));
	// 社員別CSVダウンロード (ajax)
	Route::post('/admin/download/employees', array(
		'uses' => 'Admin\DownloadController@postEmployeesCSV'
		, 'as' => 'admin.download.postEmployeesCSV'
	));
	// 入退館CSVダウンロード (ajax)
	Route::post('/admin/download/entranceExit', array(
		'uses' => 'Admin\DownloadController@postEntranceExitCSV'
		, 'as' => 'admin.download.postEntranceExitCSV'
	));
	// 勤怠分析用CSVダウンロード (ajax)
	Route::post('/admin/download/analyzeAttendance', array(
		'uses' => 'Admin\DownloadController@postAnalyzeAttendanceCSV'
		, 'as' => 'admin.download.postAnalyzeAttendanceCSV'
	));

	// ユーザー作成(view)
	Route::get('/admin/user/register', array(
		'uses' => 'Auth\RegisterController@getRegister'
		, 'as' => 'admin.user.register.view'
	));

	// ユーザー作成(保存)
	Route::post('/admin/user/register', array(
		'uses' => 'Auth\RegisterController@postRegister'
		, 'as' => 'admin.user.register.create'
	));

	// パスワードリセット(view)
	Route::get('/admin/user/password/reset', array(
		'uses' => 'Auth\PasswordController@getReset'
		, 'as' => 'auth.password.reset.view'
	));

	// パスワードリセット(保存)
	Route::post('/admin/user/password/reset', array(
		'uses' => 'Auth\PasswordController@postReset'
		, 'as' => 'auth.password.reset.create'
	));

	// 管理画面トップ
	Route::get('/admin', array(
		'uses' => 'Admin\AttendanceController@index'
		, 'as' => 'admin.attendance.employees'
	));
});



/**
 * ユーザー登録、パスワードリセットなど認証系のルーティング
 */
Route::group(['namespace' => 'Auth'], function() {

	// Authentication routes...
	Route::get('auth/login', 'AuthController@getLogin');
	Route::post('auth/login', 'AuthController@postLogin');
	Route::get('auth/logout', 'AuthController@getLogout');

	// パスワードリセット
	Route::get('password/email', 'PasswordController@getEmail');
	Route::post('password/email', 'PasswordController@postEmail');
	Route::get('password/email/complete', 'PasswordController@emailComplete');

	Route::get('password/reset', [
		'uses' => 'PasswordController@getReset'
		, 'as' => 'auth.password.reset.mail.view'
	]);
	Route::post('password/reset', [
		'uses' => 'PasswordController@postReset'
		, 'as' => 'auth.password.reset.mail.create'
	]);
});

