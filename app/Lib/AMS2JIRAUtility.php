<?php

namespace App\Lib;

use App\Attendances;
use App\TimeSheet;
use App\Employees;

class AMS2JIRAUtility
{
	public function __construct(){}
	//================================================================================
	//
	//	期間中の実働時間と計画時間を比較しCurlを生成
	//
	//================================================================================	
	public function getCURLsByCompareExistPlans($env, $empId, $user, $defaultDivisionId, $start, $end, $timesheet, $isLog = false)
	{	
		$existingPlanList = TempoAPI::getTempoPlanningList($env, $user, $start, $end);
		
		$attendanceList = $this->getActualWorkingHoursListWithInPeriod($empId, $defaultDivisionId, $start, $end, $timesheet);	
		//\Log::debug("planList = ". json_encode($existingPlanList));
		//\Log::debug("attendanceList = ". json_encode($attendanceList));
		$startDate = date($start);
		$endDate = date($end);
		//
		$diff = (strtotime($endDate) - strtotime($startDate)) / ( 60 * 60 * 24);
		//
		$curls = [];
		//
		$logs = [];
		//
		for($i=0;$i<=$diff;$i++)
		{
			$date = date('Y-m-d', strtotime($startDate . '+' . $i . 'days'));
			
			$plane = $this->findPlanByDate($existingPlanList, $date);
			
			$attendance = isset($attendanceList[$date]) === true ? $attendanceList[$date] : null;
			
			if(isset($attendance) === true &&  isset($plane) === false)
			{
				//	勤怠に記録あり		計画時間はなし → 作成
				$curls[] = TempoAPI::getCURL4CreateTempoPlanItem($env, $user, $attendance["commitment"], $date, $date);
				if($isLog === true) $logs[] = $date . "	処理:作成	実働時間:" . $attendance["commitment"];
			}else if(isset($attendance) === false &&  isset($plane) === true){
				//	勤怠の記録なし		計画時間はある → 削除
				$curls[] = TempoAPI::getCURL4DeleteTempoPlanItem($env, $plane["id"]);
				if($isLog === true) $logs[] = $date . "	処理:削除	計画ID:" . $plane["id"];
			}else if(isset($attendance) === true &&  isset($plane) === true){
				//	勤怠に記録あり		計画時間はあり → 時間が違う→ 更新
				//									  → 時間が同じ→ なし
				if((string)$attendance["commitment"] !== (string)$plane["commitment"])
				{
					//	勤怠の記録なし		計画時間はある → 削除
					$curls[] = TempoAPI::getCURL4UpdateTempoPlanItem($env, $plane["id"], $user, $attendance["commitment"], $date, $date);
					if($isLog === true) $logs[] = $date . "	処理:更新	実働時間:" . $attendance["commitment"];
				}else{
					if($isLog === true) $logs[] = $date . "	処理:なし ※同一データ	実働時間:" . $attendance["commitment"] ;
				}
			}else{
				if($isLog === true) $logs[] = $date. "	処理:なし ※データなし";
			}
			//	両方データがない → なし
		}
		
		return array("curls"=>$curls, "logs"=>$logs);
	}
	//================================================================================
	//
	//	期間中の実働時間一覧を取得
	//
	//================================================================================	
	private function getActualWorkingHoursListWithInPeriod($empId, $defaultDivisionId, $start, $end, $timeSheet)
	{				
		$selectStartTimestamp	= strtotime($start);
		$selectEndTimestamp		= strtotime($end);
		
		$attendances    = new Attendances();
		$attendanceList = $attendances->findDailyUserAttendance($selectStartTimestamp, $selectEndTimestamp, $empId);
	
		$employee = Employees::find($empId);
		
		$actualWorkingHoursList = [];
		
		foreach ($attendanceList as $data)
		{	
			if(is_null($data->start_date) === false && is_null($data->end_date) === false)
			{
				$end = ExcelFormula::getEndDate($data->start_date, $data->end_date);
				$division_id = is_null($data->division_id) ||  $data->division_id === 0 ? $employee->default_division_id : $data->division_id;

				$timeSheetAttr = TimeSheet::findDivisionAttrByID($division_id, $timeSheet);
				if(is_null($timeSheetAttr) === false)
				{
					$actualWorkingHours = ExcelFormula::getOriginWorkDate($data->start_date, $data->end_date, $timeSheetAttr);
					
					if($actualWorkingHours === "--") continue;
					
					$date = date('Y-m-d', strtotime($data->reg_date));
	
					$actualWorkingHoursList[$date] = array("date"=>$date, "commitment"=>DateUtility::convertActualWorkingHourByBaseHour($actualWorkingHours));
				}
			}
		}
		
		return $actualWorkingHoursList;
	}
	//================================================================================
	//	
	//	日付で計画時間を検索
	//
	//================================================================================
	private function findPlanByDate($existingPlanList, $date)
	{
		$count = count($existingPlanList);
		
		for($i=0;$i<$count;$i++)
		{
			$plan = $existingPlanList[$i];
			
			if($plan["start"] === $date && $plan["end"] === $date)	
			{
				return $plan;
			}
		}
		
		return;
	}
}