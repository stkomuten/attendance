<?php

namespace App\Lib;

class CurlUtility
{
	public function __construct(){}
	//================================================================================
	//
	//	JSONデータからCurlの配列生成
	//
	//================================================================================
	function getJSON2CURLs(array $jsons, $cookieFilePath)
	{
		$curls = [];
		$count = count($jsons);

		for ($i=0;$i<$count;$i++)
		{
			$item = $jsons[$i];

			$ch = curl_init();
			//
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFilePath);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFilePath);
			//
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			//
			curl_setopt($ch, CURLOPT_URL, $item["url"]);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($item["data"]));
			//
			$curls[] = $ch;
		}

		return $curls;
	}
	//================================================================================
	//
	//	同時実行制限つきCurlマルチ
	//
	//================================================================================
	public static function asyncExecCurls(array $curls, $limit = 10)
	{
		$mh = curl_multi_init();
		$queue = []; // キュー
		$errors = array_fill_keys(array_keys($curls), null); // 順番が揃うように最初にキーを埋めておく
		$count = 0; // 現在稼働中のリクエスト数

		// 出来る限りリクエストをプールに追加し，オーバーしたぶんはキューに入れる
		foreach ($curls as $i => $ch)
		{
			if ($count < $limit)
			{
				if (CURLM_OK === curl_multi_add_handle($mh, $ch))
				{
					++$count;
				}
			}else{
				$queue[] = $ch;
			}
		}

		// リクエスト実行開始
		curl_multi_exec($mh, $active);

		do{

			// 最大0.5秒間の間監視し，結果をプールに反映する
			curl_multi_select($mh, 0.5);
			curl_multi_exec($mh, $active);

			// 一度すべての結果を取り出す
			// このときにエラーコードの配列を埋めておく
			// (ここでプールから除去もしてしまうとエラーになるので注意！一旦すべて取り出す必要がある)
			$entries = [];

			do if ($entry = curl_multi_info_read($mh, $remains))
			{
				$errors[array_search($entry['handle'], $curls)] = $entry['result'];
				$entries[] = $entry;
			} while ($remains);

			// 取り出された数だけリクエストをプールから除去し，キューからまだのぶんを追加する
			foreach ($entries as $entry)
			{
				curl_multi_remove_handle($mh, $entry['handle']);
				--$count;
				if ($ch = array_shift($queue) and CURLM_OK === curl_multi_add_handle($mh, $ch))
				{
					++$count;
				}
			}

		} while ($count > 0 || $queue); // 稼働中のリクエストとキューが無くなるまでループする

		assert($active === 0); // これは必ず成立する

		return $errors;
	}
}