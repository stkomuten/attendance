<?php

namespace App\Lib;

class ExcelFormula
{
	public function __construct(){}
	//================================================================================
	//
	//	実働時間(Excel)
	//
	//================================================================================
	public static function getOriginWorkDate($excelStartDate, $excelEndDate, $timeSheetAttrs, $default = '--')
	{
		if (empty($excelStartDate) || empty($excelEndDate) || empty($timeSheetAttrs)) {
			return $default;
		}
		//
		microtime(true);
		//	
		$startDate = date('Y-m-d', strtotime($excelStartDate));
		
		$defaultTimeStamp =  strtotime("1970-01-01");
		
		//	開始日の9時を生成
		$startDateTime = strtotime($startDate . " " . $timeSheetAttrs['opening_time']);
	
		//	休憩時間算出始まり
		$breakTimeStartDate = strtotime("1970-01-01 " . $timeSheetAttrs['break_time_start']) - $defaultTimeStamp;
		
		$breakTimeSec = strtotime("1970-01-01 " . $timeSheetAttrs['break_time']) - $defaultTimeStamp;
		
		//	開始日時が9時以上か判定
		if(strtotime($excelStartDate) >= $startDateTime)
		{
			$startDateTime = strtotime($excelStartDate);
		}
		
		$origin = (strtotime($excelEndDate) - $startDateTime);
		
		if($origin < 0) return $default;	
		
		$calBreakTime =  $origin > $breakTimeStartDate ? $breakTimeSec : 0;
		
		$origin -= $calBreakTime;
		
		$hour   = intval($origin / 3600);
		$minute = intval(round($origin / 60, 2) % 60);
	
		return $hour . ":" . sprintf('%02d', $minute);
	}	
	//================================================================================
	//
	//	エクセル上での退勤時間計算
	//
	//================================================================================
	public static function getEndDate($excelStartDate, $excelEndDate, $default = '')
	{
		if(isset($excelStartDate) && isset($excelEndDate))
		{
			
			$stratDate = date('Y-m-d', strtotime($excelStartDate));
			$endDate = date('Y-m-d', strtotime($excelEndDate));
			
			
			if($stratDate === $endDate)
			{
				//	同日ならそのまま時刻を返す
				$result = date('G:i', strtotime($excelEndDate));
			}else{
				
				$endHour = date("H", strtotime($excelEndDate));
				//	日付が異なるなら差分を出したす
				$hour = self::getIntegrateEndHour($endHour, $stratDate, $endDate) ;	
				//
				$minute = date("i", strtotime($excelEndDate));
				//	
				$result = $hour . ":" . $minute;
			}
		}else if(isset($excelEndDate)){
			$result = self::getTimeRecorderEndDate($excelEndDate, $default);
		}else{
			
			$result = $default;
		}
		
		return $result;
	}
	//================================================================================
	//
	//	退勤時間
	//
	//================================================================================	
	private static function getTimeRecorderEndDate($timeRecorderEndDate, $default = '')
	{
		if (empty($timeRecorderEndDate)) {
			return $default;
		}
	
		$hour   = date("H", strtotime($timeRecorderEndDate));
		$minute = '00';
		if (intval($hour) < 48) {
			$minute = date("i", strtotime($timeRecorderEndDate));
		}
	
		return $hour . ":" . $minute;
	}
	//================================================================================
	//
	//	24時間積算表示(Excel)
	//
	//================================================================================
	private static function getIntegrateEndHour($endHour, $startDate, $endDate)
	{
		$hour = (strtotime($endDate) - strtotime($startDate)) / (3600 * 24);
		return 24 * $hour + $endHour;
	}
}