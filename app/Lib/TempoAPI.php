<?php

namespace App\Lib;

class TempoAPI
{
	//	本番環境
	const PROD_ENV = "production";
	//	開発環境
	const DEV_ENV = "development";
	
	//	本番ドメイン
	//const PROD_DOMAIN = "https://komuten.rickcloud.jp/";
	const PROD_DOMAIN = "http://52.198.46.197/";
	//	開発ドメイン
	const DEV_DOMAIN = "http://10.203.17.156/";
	
	//	本番 Tempo API トークン
	const PROD_TEMPO_API_TOKEN = "0f5ae9df-2c98-4c5e-bb32-d831e71f9f1e";
	//	開発 Tempo API トークン
	const DEV_TEMPO_API_TOKEN = "dbdeaad9-410f-44b5-96c7-2ebc501e966b";
	
	//	JIRA ログイン API 
	const JIRA_LOGIN_API = 1;
	//	Tempo プラン一覧取得 API
	const TEMPO_PLANNING_GET_ITEMS_API = 2;
	//	Tempo プラン作成 API
	const TEMPO_PLANNING_CREATE_PLAN_API = 3;
	//	Tempo プラン更新 API
	const TEMPO_PLANNING_UPDATE_PLAN_API = 4;
	//	Tempo プラン削除 API
	const TEMPO_PLANNING_DELETE_PLAN_API = 5;
	
	
	//	本番勤怠プロジェクトID
	const PROD_JIRA_PROJECT_ID = "11400";
	//	開発勤怠プロジェクトID
	const DEV_JIRA_PROJECT_ID = "11400";
	
	
	//	本番ユーザー ID
	const PROD_JIRA_USER_ID = "kintai";
	//	本番ユーザー パスワード
	const PROD_JIRA_USER_PASSWORD = "aoyama2016";
	//	開発ユーザーID
	const DEV_JIRA_USER_ID = "yuji.okita";
	//	開発ユーザー パスワード
	const DEV_JIRA_USER_PASSWORD = "aoyamao";
	
	//	クッキーファイルパス
	const  COOKIE_FILE_PATH = "/var/www/ams/storage/files/";
	//	クッキーファイル名
	const COOKIE_FILE_NAME = "cookie.txt";
		
	public function __construct(){}
	//================================================================================
	//
	//	URLの取得
	//
	//================================================================================
	public static function getURL($env, $type, $allocationId = "")
	{
		$domain = $env === self::PROD_ENV ? self::PROD_DOMAIN : self::DEV_DOMAIN;

		switch($type)
		{
			//	ログイン
			case self::JIRA_LOGIN_API:	
				$url = $domain . "jira/rest/auth/1/session";
			break;
			
			//	プラン一覧取得
			case self::TEMPO_PLANNING_GET_ITEMS_API:
				$url = $domain . "jira/rest/tempo-planning/1/allocation";
			break;
			
			//	プラン作成
			case self::TEMPO_PLANNING_CREATE_PLAN_API:
				$url = $domain . "jira/rest/tempo-planning/1/allocation";
			break;
			
			//	プラン更新
			case self::TEMPO_PLANNING_UPDATE_PLAN_API:
			//	プラン削除
			case self::TEMPO_PLANNING_DELETE_PLAN_API:
				$url = $domain . "jira/rest/tempo-planning/1/allocation/" . $allocationId . "/";
			break;
		}
		
		return $type !== self::JIRA_LOGIN_API ? $url . "?tempoApiToken=" . ($env === self::PROD_ENV ? self::PROD_TEMPO_API_TOKEN : self::DEV_TEMPO_API_TOKEN) : $url;
	}
	
	//================================================================================
	//
	//	データ生成
	//
	//================================================================================
	public static function generateData($env, $type, $param = null)
	{
		$projectId = $env === self::PROD_ENV ? self::PROD_JIRA_PROJECT_ID : self::DEV_JIRA_PROJECT_ID;
		
		switch($type)
		{
			//	ログイン
			case self::JIRA_LOGIN_API:
				
				$id = $env === self::PROD_ENV ? self::PROD_JIRA_USER_ID : self::DEV_JIRA_USER_ID;
				$pass = $env === self::PROD_ENV ? self::PROD_JIRA_USER_PASSWORD : self::DEV_JIRA_USER_PASSWORD;
				
				$data = array("username" => $id, "password" => $pass);
			
			break;
			
			//	プラン一覧取得
			case self::TEMPO_PLANNING_GET_ITEMS_API:
	
				$data =	"&assigneeKeys=" . $param["user"] . "&assigneeKeys=admin&assigneeType=user&startDate=" . $param["start"] . "&endDate=" . $param["end"];
			
			break;
			
			//	プラン作成
			case self::TEMPO_PLANNING_CREATE_PLAN_API:
			//	プラン更新
			case self::TEMPO_PLANNING_UPDATE_PLAN_API:
				//	ステイタス取得
				$data = array(
							"planItem"=> array("id"=>$projectId, "type"=>"PROJECT"), 
							"scope"=> array("id"=>$projectId, "type"=>"project"),
							"assignee"=> array("key"=>$param["user"], "type"=>"user"), 
							"commitment"=>$param["commitment"],
							"start"=>$param["start"], 
							"end"=>$param["end"], 
							"recurrence"=>array("rule"=>"NEVER")
				);	
				
			break;
		}
		
		return $type ===  self::TEMPO_PLANNING_GET_ITEMS_API ? $data : json_encode($data);
	}
	
	//================================================================================
	//
	//	プラン一覧を取得
	//
	//================================================================================	
	public static function getCURLHandle($type, $url, $data = null, $isUserPwd = false, $env = self::PROD_ENV)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_COOKIEJAR, self::COOKIE_FILE_PATH . self::COOKIE_FILE_NAME);
		curl_setopt($ch, CURLOPT_COOKIEFILE, self::COOKIE_FILE_PATH . self::COOKIE_FILE_NAME);
		
		if($isUserPwd === true)
		{
			$id = $env === self::PROD_ENV ? self::PROD_JIRA_USER_ID : self::DEV_JIRA_USER_ID;
			$pass = $env === self::PROD_ENV ? self::PROD_JIRA_USER_PASSWORD : self::DEV_JIRA_USER_PASSWORD;
			curl_setopt($ch, CURLOPT_USERPWD, $id . ":" . $pass);
		}
		
		
		switch($type)
		{
			//	ログイン
			case self::JIRA_LOGIN_API:
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				
			break;
			
			//	プラン一覧取得
			case self::TEMPO_PLANNING_GET_ITEMS_API:
				
				
			break;
			
			//	プラン登録
			case self::TEMPO_PLANNING_CREATE_PLAN_API:
				
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			break;
			
			//	プラン更新
			case self::TEMPO_PLANNING_UPDATE_PLAN_API:
				
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data)));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		
			break;
				
			//	プラン削除
			case self::TEMPO_PLANNING_DELETE_PLAN_API:
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
			break;
		}
		
		return $ch;
	}
	//================================================================================
	//
	//	セッションIDを取得
	//
	//================================================================================	
	public static function getJSESSIONID($env)
	{
		$url = self::getURL($env, self::JIRA_LOGIN_API);
		
		$data = self::generateData($env, self::JIRA_LOGIN_API);
		
		$ch = self::getCURLHandle(self::JIRA_LOGIN_API, $url, $data, true);
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		
		return $result;
	}
	//================================================================================
	//
	//	プラン一覧を取得
	//
	//================================================================================	
	public static function getTempoPlanningList($env, $user, $start, $end)
	{
		$url = self::getURL($env, self::TEMPO_PLANNING_GET_ITEMS_API);
		
		$param = array("user"=>$user, "start"=>$start, "end"=>$end);
		
		$data = self::generateData($env, self::TEMPO_PLANNING_GET_ITEMS_API, $param);

		$ch = self::getCURLHandle(self::TEMPO_PLANNING_GET_ITEMS_API, $url . $data, null, true);
	
		$result = curl_exec($ch);
		
		curl_close($ch);
		
		return json_decode($result, true);
	}
	//================================================================================
	//
	//	プランを作成
	//
	//================================================================================	
	public static function getCURL4CreateTempoPlanItem($env, $user, $commitment, $start, $end)
	{
		$url = self::getURL($env, self::TEMPO_PLANNING_CREATE_PLAN_API);
		
		$param = array("user"=>$user, "commitment"=>$commitment, "start"=>$start, "end"=>$end);
		
		$data = self::generateData($env, self::TEMPO_PLANNING_CREATE_PLAN_API, $param);
		
		return self::getCURLHandle(self::TEMPO_PLANNING_CREATE_PLAN_API, $url, $data, true);
	}
	//================================================================================
	//
	//	プランを更新
	//
	//================================================================================	
	public static function getCURL4UpdateTempoPlanItem($env, $allocationId, $user, $commitment, $start, $end)
	{
		//		
		$url = self::getURL($env, self::TEMPO_PLANNING_UPDATE_PLAN_API, $allocationId);
		//
		$param = array("user"=>$user, "commitment"=>$commitment, "start"=>$start, "end"=>$end);
		//
		$data = self::generateData($env, self::TEMPO_PLANNING_UPDATE_PLAN_API, $param);
		//
		return self::getCURLHandle(self::TEMPO_PLANNING_UPDATE_PLAN_API, $url, $data, true);
	}
	
	//================================================================================
	//
	//	プランを削除
	//
	//================================================================================	
	public static function getCURL4DeleteTempoPlanItem($env, $allocationId)
	{
		//		
		$url = self::getURL($env, self::TEMPO_PLANNING_DELETE_PLAN_API, $allocationId);
		//
		return self::getCURLHandle(self::TEMPO_PLANNING_DELETE_PLAN_API, $url, null, true);
	}
	//================================================================================
	//
	//	営業日による開始日・終了日の取得
	//
	//================================================================================
	public static function getStart2End($date, $businessDayCount = 3)
	{
		if(!strptime($date, '%Y-%m-%d')) return false;
		//
		$start = date("Y", strtotime($date)) . "-" . date("m", strtotime($date)) . "-01";
		//
		$dateUtility = new \App\Lib\DateUtility();
		//	昨日が第2営業日か判定
		$isWithin2BusinessDay = $dateUtility->isWithinBusinessDay($date, $businessDayCount);	
		//	第2営業日以内の場合は先月の1日、それ以外あは当月
		$start = $isWithin2BusinessDay ? date('Y-m-01', strtotime($start . " -1 month")) : $start;	
		//
		return array("start"=>$start, "end"=>$date);
	}
}