<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaidHolidayBalance extends Model
{
	//
	protected $table = "paid_holiday_balance";
	//
	protected $fillable = ["id", "emp_no", "date", "days"];
	//
	public static function getListByDate($year, $month)
	{
		$date = date("Y-m-d", strtotime($year . "-" . $month ."-01"));
		
		$result = self::where("date", '=', $date);

		return $result;
	}
	//
	public static function getEmployeeByDate($empNo, $year, $month)
	{
		$date = date("Y-m-d", strtotime($year . "-" . $month ."-01"));
		//
		$result = self::where('emp_no', $empNo)
					->where("date", '=', $date)
					->first();

		return $result;
	}
}
