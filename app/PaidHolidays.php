<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaidHolidays extends Model
{
	const PAID_HOLIDAY_WHOLE				= 1;
	const PAID_HOLIDAY_MORNING			= 2;
	const PAID_HOLIDAY_AFTERNOON		= 3;
	const PAID_HOLIDAY_1_HOUR				= 4;
	const PAID_HOLIDAY_2_HOUR				= 5;
	const PAID_HOLIDAY_3_HOUR				= 6;
	const PAID_HOLIDAY_5_HOUR				= 7;
	const PAID_HOLIDAY_6_HOUR				= 8;
	const PAID_HOLIDAY_7_HOUR				= 9;
	const PAID_HOLIDAY_SUMMER		= 10;
	const PAID_HOLIDAY_WINTER				= 11;
	const PAID_HOLIDAY_CELEBRATION_OR_CONDOLENCE	= 12;
	const PAID_HOLIDAY_ROKUJIRO				= 13;
	const PAID_HOLIDAY_COMPENSATORY	= 14;
	const PAID_HOLIDAY_SUBSTITUTE			= 15;
	const PAID_HOLIDAY_ABSENCE				= 16;
	const PAID_HOLIDAY_NURSING				= 17;
	const PAID_HOLIDAY_CARE						= 18;
	const PAID_HOLIDAY_PHYSIOLOGY			= 19;
	//
	protected $table = "paid_holidays";
	//
	protected $fillable = ["id", "name", "hour"];
	//
	public function getAllSortCompensatory()
	{
		$result = self::where('id', '!=', PaidHolidays::PAID_HOLIDAY_COMPENSATORY)->where('id', '<', 20)->get();
		$result[] = self::where('id', PaidHolidays::PAID_HOLIDAY_COMPENSATORY)->first();
		$result[] = self::where('id', '=', 20)->first();
		$result[] = self::where('id', '=', 21)->first();
		$result[] = self::where('id', '=', 22)->first();
		return $result;
	}
}
