<?php

namespace App\Providers;

use Illuminate\View\ViewServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\FileViewFinder;

class CustomViewServiceProvider extends ViewServiceProvider
{
    /**
     * Register the Blade engine implementation.
     *
     * @param  \Illuminate\View\Engines\EngineResolver  $resolver
     * @return void
     */
    public function registerBladeEngine($resolver)
    {
        $app = $this->app;

        // The Compiler engine requires an instance of the CompilerInterface, which in
        // this case will be the Blade compiler, so we'll first create the compiler
        // instance to pass into the engine so it can compile the views properly.
        $app->singleton('blade.compiler', function ($app) {
            $mode = $this->getCookieViewMode();
            $cache = $app['config']['view.compiled'];
            if ($mode === "mock") {
              $cache = realpath(storage_path('framework/mock_views'));
            }

            return new BladeCompiler($app['files'], $cache);
        });

        $resolver->register('blade', function () use ($app) {
            return new CompilerEngine($app['blade.compiler']);
        });
    }

    /**
     * Register the view finder implementation.
     *
     * @return void
     */
    public function registerViewFinder()
    {
        $this->app->bind('view.finder', function ($app) {
            $mode = $this->getCookieViewMode();
            $paths = $app['config']['view.paths'];

            if ($mode === "mock") {
              $paths = [
                realpath(base_path('resources/mock_views'))
              ];
            }

            return new FileViewFinder($app['files'], $paths);
        });
    }


    private function getCookieViewMode()
    {
      $mode = isset($_COOKIE['change_mode']) ? $_COOKIE['change_mode'] : null;
      return $mode;
    }
}
