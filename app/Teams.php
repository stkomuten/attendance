<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
	const CREATED_AT = "created_at";
    const UPDATED_AT = null;

    protected $table = "teams";
	protected $fillable = ["id", "project_name", "team_name", "created_at"];
	
	/**
	 * プロジェクトIDを指定して複数取得
	 */
	public static function allOrderByName()
	{
		return Departments::orderBy("team_name","desc")->get();
	}	
}


