<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeSheet extends Model
{	
	protected $table       = "time_sheet";
	protected $fillable    = ["id", "opening_time", "closing_time", "overtime_start", "overtime_end", "late_night_start", "late_night_end", "break_time", "break_time_start", "official_working_hour","morning_off_time","afternoon_off_time"];
	//================================================================================
	//
	//		選択リスト取得
	//
	//================================================================================
	public static function geListOrderByWorkingHour()
	{
			return \DB::table("time_sheet")
			->select("*")
			->orderBy("official_working_hour", "DESC")
			//->orderBy("opening_time", "ASC")
			->get();
	}
	//================================================================================
	//	IDで勤務区分を検索
	//================================================================================
	public static function findDivisionAttrByID($id, $timeSheet)
	{
		$timeSheetAttr;
		$count = count($timeSheet);
		
		for($i=0;$i<$count;$i++)
		{
			if($id == $timeSheet[$i]['id'])
			{
				$timeSheetAttr = $timeSheet[$i];
				break;
			}
		}
		
		return $timeSheetAttr;
	}
}