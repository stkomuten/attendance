#!/bin/sh

PROJECT_PATH='/var/www/projects/www/laravel'
DUMPFILE_PATH='storage/dump'
FILENAME=`date +%Y%m%d`
DB_PASSWORD='yappa8811'

mysqldump -u root -p$DB_PASSWORD attendance > $FILENAME.sql

zip $FILENAME.zip $FILENAME.sql

mv --force $FILENAME.zip $PROJECT_PATH/$DUMPFILE_PATH

rm $FILENAME.sql

