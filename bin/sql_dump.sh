#!/bin/sh

if [ $# -ne 1 ]; then
  echo "引数がありません" 1>&2
  exit 1
fi

# ルートのパスワード
ROOT_PASSWORD='bouZuJu29odNwjMn'

# プロジェクトルート
PROJECT_PATH='/var/www/projects/www/laravel'

# dumpファイル格納先
DUMPFILE_PATH='storage/dump'

# fileのprefix
FILE_PREFIX=$1

# SQLファイル名
FILENAME="today_sql_${FILE_PREFIX}.sql"

# 時間の幅(1h)
HOUR_RANGE=3

# 指定開始時間
START_DATE=`date -d "${HOUR_RANGE} hours ago" "+%Y-%m-%d %H:00:00"`

# 指定終了時間
END_DATE=`date "+%Y-%m-%d %H:00:00"`

# MYSQLの指定時間
MYSQL_DIR=/var/lib/mysql

# BINLOGファイル
BINLOG_FILE=`echo ${ROOT_PASSWORD} | sudo -S less ${MYSQL_DIR}/mysql-bin.index`

echo ${ROOT_PASSWORD} | sudo -S mysqlbinlog $MYSQL_DIR/$BINLOG_FILE \
    --start-datetime "${START_DATE}" \
    --stop-datetime "${END_DATE}" | \
    perl -e 'while(<>){ chomp; next if m!^#!; if ( m{/*!*/;$} ) { $p .= $_; print "$p\n"; $p="" } else { $p .= $_." "} }' | \
    perl -nle 'm!^(DELETE FROM|REPLACE INTO|INSERT INTO|UPDATE)\s+([^ ]+)!i && print' > $PROJECT_PATH/$DUMPFILE_PATH/$FILENAME 2>/dev/null
