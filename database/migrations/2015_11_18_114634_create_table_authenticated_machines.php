<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAuthenticatedMachines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authenticated_machines', function (Blueprint $table) {
            $table->string('name')->comment = "機体名";
            $table->string('mac_address', 128)->comment = "機体のMACアドレス";
            $table->string('hash', 255)->comment = "ハッシュ値";
            $table->dateTime('created_at')->comment = "登録日";

            $table->primary('mac_address');
            $table->index('hash');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authenticated_machines');
    }
}
