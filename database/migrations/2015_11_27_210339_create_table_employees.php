<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->string('emp_no', 255)->comment = "社員番号";
            $table->string('last_name', 255)->comment = "姓";
            $table->string('first_name', 255)->comment = "名";
            $table->string('felica_id', 255)->comment = "社員証番号";
            $table->string('mail', 255)->comment = "メールアドレス";
            $table->string('password', 60)->comment = "ログインパスワード";
            $table->string('remember_token', 100)->comment = "ログイントークン";
            $table->unsignedInteger('emp_type')->comment = "雇用形態";
            $table->unsignedInteger('status')->comment = "雇用状態";
            $table->boolean('attendance_flg')->default(true)->comment = "勤怠記録をつける華道家のフラグ";
            $table->dateTime('reg_date')->comment = "登録日";
            $table->dateTime('upd_date')->comment = "更新日";
            $table->dateTime('del_date')->nullable()->comment = "削除日";
            $table->boolean('del_flg')->default(false)->comment = "削除フラグ";

            $table->primary('emp_no');
            $table->index('felica_id');

            $table->foreign('emp_type')->references('id')->on('employment_type');
            $table->foreign('status')->references('id')->on('employment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function(Blueprint $table) {
            $table->dropForeign('employees_emp_type_foreign');
            $table->dropForeign('employees_status_foreign');

            $table->dropIfExists('employees');
        });
    }
}
