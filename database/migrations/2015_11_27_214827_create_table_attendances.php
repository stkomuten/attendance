<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('emp_no', 255)->comment = "社員番号";
            $table->dateTime('start_date')->nullable()->comment = "出勤時間";
            $table->dateTime('init_start_date')->nullable()->comment = "初期出勤時間";
            $table->dateTime('end_date')->nullable()->comment = "退勤時間";
            $table->dateTime('init_end_date')->nullable()->comment = "初期退勤時間";
            $table->dateTime('start_break_date')->nullable()->comment = "休憩入り時間";
            $table->dateTime('end_break_date')->nullable()->comment = "休憩戻り時間";
            $table->text('note')->nullable()->comment = "備考";
            $table->string('reg_emp_no')->comment = "登録時に使用された社員番号";
            $table->dateTime('reg_date')->comment = "登録日";
            $table->string('upd_emp_no')->nullable()->comment = "更新時に使用された社員番号";
            $table->dateTime('upd_date')->comment = "更新日";
            $table->string('del_emp_no')->nullable()->comment = "削除時に使用された社員番号";
            $table->dateTime('del_date')->nullable()->comment = "削除日";
            $table->boolean('del_flg')->default(false)->comment = "削除フラグ";

            $table->index('emp_no');
            $table->foreign('emp_no')->references('emp_no')->on('employees')->onDelete('cascade');
            $table->foreign('reg_emp_no')->references('emp_no')->on('employees');
            $table->foreign('upd_emp_no')->references('emp_no')->on('employees');
            $table->foreign('del_emp_no')->references('emp_no')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance', function(Blueprint $table) {
            $table->dropForeign('attendance_emp_no_foreign');
            $table->dropForeign('attendance_reg_emp_no_foreign');
            $table->dropForeign('attendance_upd_emp_no_foreign');
            $table->dropForeign('attendance_del_emp_no_foreign');

            $table->dropIfExists('attendance');
        });
    }
}
