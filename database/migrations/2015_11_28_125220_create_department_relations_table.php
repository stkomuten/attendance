<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_relations', function (Blueprint $table) {
            $table->unsignedInteger('department_id')->comment = "部署ID";
            $table->string('emp_no', 255)->comment = "社員ID";
            $table->unsignedInteger('position_id')->comment = "部署役職ID";
            $table->dateTime('created_at')->nullable()->comment = "登録日";

            $table->primary(['department_id', 'emp_no']);
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
            $table->foreign('emp_no')->references('emp_no')->on('employees')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('department_relations', function(Blueprint $table) {
            $table->dropForeign('department_relations_department_id_foreign');
            $table->dropForeign('department_relations_emp_no_foreign');

            $table->dropIfExists('department_relations');
        });
    }
}
