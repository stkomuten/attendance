<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminRoleMatrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_role_matrices', function (Blueprint $table) {
            $table->string('target_id', 255)->comment = "部署IDまたはポジションID";
            $table->string('name', 255)->comment = "権限名";
            $table->dateTime('created_at')->comment = "登録日";
            $table->primary('target_id');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_role_matrices');
    }
}
