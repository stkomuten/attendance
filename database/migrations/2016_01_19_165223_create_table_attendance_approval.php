<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttendanceApproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_approval', function (Blueprint $table) {
            $table->string('emp_no', 255)->comment = "社員ID";
            $table->string('approval_month', 8)->comment = "認証月(YYYYMM)";
            $table->unsignedSmallInteger('status')->comment = "状態(未承認=0、承認=1、承認取消=2)";
            $table->text('note')->nullable()->comment = "備考";
            $table->string('reg_emp_no')->comment = "登録時に使用された社員番号";
            $table->dateTime('reg_date')->comment = "登録日";
            $table->string('upd_emp_no')->nullable()->comment = "更新時に使用された社員番号";
            $table->dateTime('upd_date')->comment = "更新日";
            $table->string('del_emp_no')->nullable()->comment = "削除時に使用された社員番号";
            $table->dateTime('del_date')->nullable()->comment = "削除日";
            $table->boolean('del_flg')->default(false)->comment = "削除フラグ";

            $table->primary(['emp_no', 'approval_month']);

            $table->index('status');

            $table->foreign('emp_no')->references('emp_no')->on('employees')->onDelete('cascade');
            $table->foreign('reg_emp_no')->references('emp_no')->on('employees');
            $table->foreign('upd_emp_no')->references('emp_no')->on('employees');
            $table->foreign('del_emp_no')->references('emp_no')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_approval');
    }
}
