<?php

use Illuminate\Database\Seeder;

class AdminRoleMatricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");

        DB::table('admin_role_matrices')->insert([
            'target_id'    => '3'
            , 'name'       => '管理部'
            , 'created_at' => $now
        ]);

        DB::table('admin_role_matrices')->insert([
            'target_id'    => '101'
            , 'name'       => '一般'
            , 'created_at' => $now
        ]);

        DB::table('admin_role_matrices')->insert([
            'target_id'    => '110'
            , 'name'       => '上長'
            , 'created_at' => $now
        ]);

        DB::table('admin_role_matrices')->insert([
            'target_id'    => '120'
            , 'name'       => '本部長'
            , 'created_at' => $now
        ]);

        DB::table('admin_role_matrices')->insert([
            'target_id'    => '1000'
            , 'name'       => 'システム管理者'
            , 'created_at' => $now
        ]);
    }
}
