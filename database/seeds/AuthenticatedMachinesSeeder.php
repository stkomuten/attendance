<?php

use Illuminate\Database\Seeder;

class AuthenticatedMachinesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now = date("Y-m-d H:i:s");

        DB::table('authenticated_machines')->insert([
            'name'          => "VAIO-S"
            , 'mac_address' => "88-9F-FA-8E-67-86"
            , 'hash'        => "15602d224f8efea960af416f42993e6d20dcfd9ce62433c442f4eb7073a17891"
            , 'created_at'  => $now
        ]);
    }
}
