<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('AuthenticatedMachinesSeeder');
        $this->call('EmploymentTypesSeeder');
        $this->call('EmploymentStatusSeeder');
        $this->call('DepartmentsSeeder');

        $this->call('EmployeesSeeder');

        $this->call('AdminRoleMatricesSeeder');

        Model::reguard();
    }
}
