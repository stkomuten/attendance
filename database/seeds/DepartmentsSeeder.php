<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");

        DB::table('departments')->insert([
            'name'         => "アプリ部"
            , 'created_at' => $now
        ]);

        DB::table('departments')->insert([
            'name'         => "プロジェクト管理部"
            , 'created_at' => $now
        ]);

        DB::table('departments')->insert([
            'name'         => "経営管理部"
            , 'created_at' => $now
        ]);

        DB::table('departments')->insert([
            'name'         => "品質管理部"
            , 'created_at' => $now
        ]);
    }
}
