<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now = date("Y-m-d H:i:s");

        DB::table('employees')->insert([
            'emp_no'          => '010094'
            , 'last_name'     => '大北'
            , 'first_name'    => '雄志'
            , 'felica_id'     => '011204128415c502'
            , 'mail'          => 'yuji_okita@yappa.co.jp'
            , 'password'      => password_hash("yappa8811", PASSWORD_BCRYPT)
            , 'emp_type'      => 1
            , 'status'        => 1
            , 'reg_date'      => $now
            , 'upd_date'      => $now
        ]);

        // DB::table('employees')->insert([
        // 	'emp_no'       => '010113'
        // 	, 'last_name'  => '椿谷'
        // 	, 'first_name' => '勇次'
        // 	, 'felica_id'  => '01120412a5147c02'
        // 	, 'mail'       => 'Yuji_Tsubakiya@yappa.co.jp'
        // 	, 'emp_type'   => 1
        // 	, 'status'     => 1
        // 	, 'reg_date'   => $now
        // 	, 'upd_date'   => $now
        // 	, 'del_flg'    => 0
        // ]);

        // DB::table('employees')->insert([
        //     'emp_no'  => '010142'
        //     , 'last_name'  => '鈴木'
        //     , 'first_name' => '由実'
        //     , 'felica_id'  => '01120112d310630f'
        //     , 'mail'       => 'yumi_suzuki@yappa.co.jp'
        //     , 'emp_type'   => 1
        //     , 'status'     => 1
        //     , 'reg_date'   => $now
        //     , 'upd_date'   => $now
        //     , 'del_flg'    => 0
        // ]);

        // DB::table('employees')->insert([
        //     'emp_no'  => '010186'
        //     , 'last_name'  => '山本'
        //     , 'first_name' => '恵美'
        //     , 'felica_id'  => '011201121712761d'
        //     , 'mail'       => 'emi_yamamoto@yappa.co.jp'
        //     , 'emp_type'   => 1
        //     , 'status'     => 1
        //     , 'reg_date'   => $now
        //     , 'upd_date'   => $now
        //     , 'del_flg'    => 0
        // ]);

        DB::table('employees')->insert([
            'emp_no'          => '010192'
            , 'last_name'     => '藤田'
            , 'first_name'    => '雄大'
            , 'felica_id'     => '010103100c12f813'
            , 'mail'          => 'yudai_fujita@yappa.co.jp'
            , 'password'      => password_hash("yappa8811", PASSWORD_BCRYPT)
            , 'emp_type'      => 1
            , 'status'        => 1
            , 'reg_date'      => $now
            , 'upd_date'      => $now
        ]);

    }
}
