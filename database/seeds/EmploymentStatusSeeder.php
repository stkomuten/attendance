<?php

use Illuminate\Database\Seeder;

class EmploymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employment_status')->insert([
            'name' => "在籍"
        ]);

        DB::table('employment_status')->insert([
            'name' => "休職"
        ]);

        DB::table('employment_status')->insert([
            'name' => "退職"
        ]);
    }
}
