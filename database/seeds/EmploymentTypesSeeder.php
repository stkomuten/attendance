<?php

use Illuminate\Database\Seeder;

class EmploymentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employment_type')->insert([
            'name' => "正社員"
        ]);

        DB::table('employment_type')->insert([
            'name' => "契約社員"
        ]);

        DB::table('employment_type')->insert([
            'name' => "フリーランス"
        ]);

        DB::table('employment_type')->insert([
            'name' => "アルバイト"
        ]);

        DB::table('employment_type')->insert([
            'name' => "インターン"
        ]);
    }
}
