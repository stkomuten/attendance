# 環境構築

## はじめに

* ネットワーク設定などが終了したcentos6のMinimal環境をイメージしております。

## 1. 事前準備

    yum update
    yum install gcc kernel-devel perl git wget curl-devel vim tree man unzip

## 2. LAMP環境作成

    # 以下のコマンドはすべてroot作業


    # 日付をJSTへ変更
    cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

    # SELinuxを無効
    setenforce 0

    # EPELレポジトリの追加
    yum install epel-release

    # Remiレポジトリの追加
    wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
    rpm -Uvh remi-release-6.rpm

    # Apacheインストール
    yum install httpd-devel

    # Apache設定
    sed -i -e "s/#ServerName www.example.com:80/ServerName *:80/g" /etc/httpd/conf/httpd.conf
    /etc/init.d/httpd start

    # MySQLインストール
    yum install http://dev.mysql.com/get/mysql-community-release-el6-5.noarch.rpm
    yum install --enablerepo=mysql56-community mysql mysql-server mysql-devel

    # MySQLログフォルダ作成
    mkdir /var/log/mysql
    chown mysql.mysql /var/log/mysql

    # MySQL起動
    /etc/init.d/mysqld start

    # MySQLの初回設定
    mysql_secure_installation
        Enter current password for root (enter for none):(今のrootのパスワード入力して) -> 最初はないのでそのままエンター
        Set root password? [Y/n]:(rootのパスワード設定する？) -> したいからY
        New password:(新しいパスワード入力して) -> yappa8811
        Re-enter new password:(もっかい入力して) -> yappa8811
        Remove anonymous users? [Y/n]:(最初からいるanonymousユーザー削除する?) -> 使わないから削除していいのでY
        Disallow root login remotely? [Y/n]:(リモートからのrootのログイン禁止する？) -> 禁止したいからY
        Remove test database and access to it? [Y/n]:(testデータベース削除する？) -> 削除したいからY
        Reload privilege tables now? [Y/n]:(権限テーブルを再読込する？) -> したいからY

    # MySQLの再起動
    /etc/init.d/mysqld restart

    # PHPとそのたツールインストール
    yum install --enablerepo=remi-php56 php php-mysql php-redis php-mbstring php-devel php-opcache php-soap php-pdo php-mcrypt php-pecl-xdebug php-xml gd-last php-gd phpMyAdmin

    # PHP設定
    sed -i -e "s/;date.timezone =/date.timezone = Asia\/Tokyo/g" /etc/php.ini

    # composerのインストール
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin
    mv /usr/local/bin/composer.phar /usr/local/bin/composer
    chmod 755 /usr/local/bin/composer

    # phpMyAdmin設定
    sed -i -e "s/Order Deny,Allow/#Order Deny,Allow/g" /etc/httpd/conf.d/phpMyAdmin.conf
    sed -i -e "s/Deny from All/#Deny from All/g" /etc/httpd/conf.d/phpMyAdmin.conf
    sed -i -e "s/Allow from 127.0.0.1/#Allow from 127.0.0.1/g" /etc/httpd/conf.d/phpMyAdmin.conf
    sed -i -e "s/Allow from ::1/#Allow from ::1/g" /etc/httpd/conf.d/phpMyAdmin.conf

    # Redisインストール
    yum install --enablerepo=remi redis
    /etc/init.d/redis restart

    #自動起動
    chkconfig httpd on
    chkconfig mysqld on
    chkconfig redis on

## 2. 初期設定

1. bitbucketからソースを落とす(ドキュメントルートはpublic/になるように設定)
2. 環境ファイルを作成
3. composer updateでコアファイル群を落とす
4. dev環境からdumpSQLを取得してDBに反映

** 以下はサンプルです！ **

    cd /var/www/attendance
    git clone git@bitbucket.org:stk_attendance/attendance.git ./
    cp .env.prod .env
    composer install

## 3. ローカル設定

** 以下はローカル設定です！本番ではやめてください ##

    # iptables無効
    /etc/init.d/iptables stop

    # 自動起動off
    chkconfig iptables off

    # 簡単なvirtualHostの設定
    vim /etc/httpd/conf.d/attendance.conf
        NameVirtualHost *:80

        <VirtualHost *:80>
          DirectoryIndex index.php
          DocumentRoot   "/var/www/attendance/public"
          ServerName     attendance.stk.local
          ErrorLog       logs/attendance.error_log
          CustomLog      logs/attendance.access_log combined

          EnableSendfile off

          <Directory "/var/www/attendance/public">
            Options FollowSymLinks
            AllowOverride All
            Order allow,deny
            Allow from All
          </Directory>
        </VirtualHost>

    # apache再起動
    /etc/init.d/httpd restart

    # ホストマシンのhostsファイルで以下を書き込み
    # mac: /etc/hosts
    # windows: Windwos\System32\drivers\etc\hosts
        vmのIPアドレス    attendance.stk.local

