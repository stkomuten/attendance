# cron系の説明

## 各cronの詳細

### 1. 勤怠通し忘れリマインドメール

#### 実行タイミング

平日 AM9時

#### cron記述

    0 9 * * 1-5 /usr/bin/php /home/yappa/laravel/artisan remind

### 2. DBのオールダンプ

#### 実行タイミング

毎日AM 6時

#### cron記述

    0 6 * * * /bin/sh /var/www/projects/www/laravel/bin/all_db_dump.sh

### 3. 指定期間でのSQL発行のダンプ

#### 実行タイミング

毎日　AM11時, PM20時

#### cron記述

    0 11 * * * /bin/sh /var/www/projects/www/laravel/bin/sql_dump.sh am
    0 20 * * * /bin/sh /var/www/projects/www/laravel/bin/sql_dump.sh pm
