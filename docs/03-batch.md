# バッチ系の説明

## Laravelのバッチ参考URL

[http://readouble.com/laravel/5/1/ja/artisan.html](http://readouble.com/laravel/5/1/ja/artisan.html)

## 実行場所

**artisanがあるディレクトリ**

svr009の場合は以下

    /var/www/projects/www/laravel

## 各バッチの詳細

### 勤怠通し忘れリマインドバッチ

#### [仕様]

社員が勤怠を通し忘れた際、次の日に、前日の通し忘れをしている社員に対して、リマインドメールを送信する。

対象の社員の他に、その社員の上長をCCにいれてメールは送信される。

金曜に通し忘れたときは、次の月曜に送信する。

#### [実行方法]

##### 1. 実際に送信する

    php artisan remind

##### 2. 実際の送信はせず、送信対象だけ表示する

    php artisan remind --dry-run

##### 3. 日時を偽装して送信する

    php artisan remind --simulateDate="YYYY-MM-DD HH:ii:ss"

##### 4. 偽装した日時で、送信対象だけ表示する

    php artisan remind --simulateDate="YYYY-MM-DD HH:ii:ss" --dry-run

