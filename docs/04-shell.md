# シェル系の説明

## シェルファイル格納場所

**binディレクトリ**

svr009の場合は以下

    /var/www/projects/www/laravel/bin

## 各シェルの詳細

### 1. DBのオールダンプ

#### ファイル名

    all_db_dump.sh

#### 仕様

    mysqldumpを使って、全テーブルのdumpを行う

#### dumpファイル格納場所

**storage/dumpディレクトリ**

svr009の場合は以下

    /var/www/projects/www/laravel/storage/dump

### 2. 指定期間でのSQL発行のダンプ

#### ファイル名

    sql_dump.sh

#### 仕様

    binlogから指定期間に発行されたselect以外のSQL文を抜き取る

#### dumpファイル格納場所

**storage/dumpディレクトリ**

svr009の場合は以下

    /var/www/projects/www/laravel/storage/dump
