(function(window, $){


// datePicker開始
$("#daily-datepicker").datepicker({
    showOn: "both",
    buttonImage: "/images/calendar_icon.svg",
    buttonImageOnly: true
});


/**
 * カレンダー設定イベント
 */
$('#daily-datepicker').change(function(e){
	var date   = e.target.value.replace('/', '').replace('/', '')
	var filter = $('#select_type')[0].value
	jumpToDailyPage(date + '/' + filter)
})


/**
 * フィルター変更イベント
 */
$('#select_type').change(function(e){
	var date = $('#daily-datepicker')[0].value.replace('/', '').replace('/', '')
	jumpToDailyPage(date + '/' + e.target.value)
})


/**
 * 削除ボタンのイベント
 */
$('.deleteButton').click(function(e) {
	var targetId  = $(e.target).data('number')
	var csrfToken = $('meta[name="csrf-token"]').attr('content')

	var okCallback = function() {
		alert('勤怠記録を削除しました。')
		window.location.reload()
	}

	var ngCallback = function() {
		alert('勤怠記録を削除できませんでした。')
	}

	execDelete(targetId, csrfToken, okCallback, ngCallback)
})


})(window, jQuery);