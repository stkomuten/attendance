var paidHolidaySelectCount = 1; 

(function(window, $){


$.ajaxSetup({
        headers: { "X-CSRF-TOKEN": $.cookie("csrf-token") }
});
/**
 * 年リスト変更イベント
 */
$('#selectYear').change(function(e) {
	var date = e.target.value + $('#selectMonth')[0].value
	var userId = $('#select_user')[0].value
	jumpToEmployeesPage(userId + "/" + date)
});


/**
 * 月リスト変更イベント
 */
$('#selectMonth').change(function(e) {
	var date = $('#selectYear')[0].value + e.target.value
	var userId = $('#select_user')[0].value
	jumpToEmployeesPage(userId + "/" + date)
});


/**
 * ユーザーリスト変更イベント
 */
$('#select_user').change(function(e) {
   var date = $('#selectYear')[0].value + $('#selectMonth')[0].value
   var userId = e.target.value
   jumpToEmployeesPage(userId + "/" + date)
})


/**
 * 削除ボタンのイベント
 */
$('.deleteButton').click(function(e) {
	var targetId  = $(e.target).data('number')
	var csrfToken = $('meta[name="csrf-token"]').attr('content')

	var okCallback = function() {
		alert("勤怠記録を削除しました。")
		window.location.reload()
	}

	var ngCallback = function() {
		alert("勤怠記録を削除できませんでした。")
	}

	execDelete(targetId, csrfToken, okCallback, ngCallback)
})



/**
 * 承認取消ボタンのイベント
 */
$('.approval-cancel-button').click(function(e)
{
	
	$(e.target).popover({
		trigger : 'manual',
		placement:"top",
		html : true,
	    title: function()
		{
			return "承認取消";
		},
		content: function()
		{
			var content = '';
			content = $('#approval-cancel-markup').html();		
			return content;
		}
	}).popover('show');
	
})

/**
 * 承認ボタンのイベント
 */
$('.approval-save-button').click(function(e) {
	
	if(unRecordedCount > 0)
	{
		alert("未記録または未設定の日付が" + unRecordedCount + "日分あります。\n記録を入力してから承認してください。");
	
	}else{
		var date   = $('#selectYear')[0].value + $('#selectMonth')[0].value
		var emp_no = $('#select_user')[0].value
		var status = 1
	
		new $.ajax({
				url: "/admin/attendance/approval",
				headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				data: {
					"approval_month": date
					, "emp_no":       emp_no
					, "status":       status
				},
				cache:false,
				type:"POST",
				dataType: "json",
				success: function() {
					window.location.href = "/admin/attendance/approval/" + date + "/-1"
				},
				error:errorAjax
		});
	}
})

var popover_tabIndex;
/**
 * 備考編集ボタンのイベント
 */
$('[data-toggle="popover"]').click(function(e)
{
	
	paidHolidayButtonWidth = $("#addSelectGroupBtn").width();
	
	if($("div[id*='popover']")) hidePopover();
	
	
	if(popover_tabIndex == e.target.tabIndex)
	{
		popover_tabIndex = -1;
		 return;
	}
	
	popover_tabIndex = e.target.tabIndex;
	var targetId = $(e.target).data('id');
	if(targetId == undefined) targetId = "";
	var date = $(e.target).data('date');
	var dateLabel = $(e.target).data('date-label');
	var paidHoliday = $(e.target).data('paid-holiday');
	var day = $(e.target).data('day');
	var noteText = $(e.target).data('text') == undefined ? "" : $(e.target).data('text');

	$(e.target).popover({
		trigger : 'manual',
		placement:"left",
		html : true,
	    title: function()
		{
			return "休暇または休業 ・ 備考";
		},
		content: function()
		{
			var content = '';
			content = $('#popover-markup').html();
			var re = new RegExp("#####DATE#####");
			content = content.replace(re, date);
			re = new RegExp("#####DATE_DISPLAY#####");
			content = content.replace(re, dateLabel + "(" + day + ")");
			re = new RegExp("#####NOTE#####");
			content = content.replace(re, noteText);
			re = new RegExp("#####ID#####");
			content = content.replace(re, targetId);
			
			
			
			return content;
		}
	}).popover('show');
	
	var timer = setInterval(function()
	{
		$("#paidHoliday").val(paidHoliday);
		initSelectGroup(paidHoliday);
		switchPaidHolidayButtonVisible();
		clearInterval(timer); 
	},100);

	//============================================================
	//
	//		追加ボタン Clickハンドラ
	//
	//============================================================
	$("#addSelectGroupBtn").click(function()
	{
		if(paidHolidaySelectCount < 2)
		{
			//　カウントアップ
			paidHolidaySelectCount++;
			//
			addSelectGroup();
			//
			switchPaidHolidayButtonVisible();
		}
	});
	//============================================================
	//
	//		削除ボタン Clickハンドラ
	//
	//============================================================		
	$("#removeSelectGroupBtn").click(function()
	{
		$('#paidHoliday' + paidHolidaySelectCount).remove();
		
		paidHolidaySelectCount--;
		 
		switchPaidHolidayButtonVisible();
	});

})


$('[data-toggle="popover-paidholiday-help"]').click(function(e)
{	
	if($("div[id*='popover']")) hidePopover();
	
	
	if(popover_tabIndex == e.target.tabIndex)
	{
		popover_tabIndex = -1;
		 return;
	}
	
	popover_tabIndex = e.target.tabIndex;
	
	$(e.target).popover({
	trigger : 'auto',
	placement:"left",
	html : true,
	content:$('#popover-paidholiday-help').html()

	}).popover('show');
	
	$(".popover").css("color", "#333");
	$(".popover-content").css("padding", "2");
});






$('body').on('click', function(e)
{
		if (e.target.name != 'popover_anchor' && e.target.name != 'note' && e.target.name != 'paidHoliday' && e.target.parentNode.className != 'popover-content' && e.target.name != 'updateNoteBtn' &&  $('.popover').has(e.target).length === 0 && e.target.parentNode.className != 'popover-content' && e.target.name != 'approval-cancel-button' && e.target.name != 'paidholiday-help-button')
		{
            hidePopover();
        }
});


})(window, jQuery);

//---------------------------------------------------------------------------------------------------------------
//	備考編集ボタンクリックハンドラ
//---------------------------------------------------------------------------------------------------------------
function hidePopover()
{
	$('.popover').each(function() {
		$('[data-toggle="popover"]').popover('destroy');
		$('[data-toggle="approval-cancel-popover"]').popover('destroy');
		$('[data-toggle="popover-paidholiday-help"]').popover('destroy');

		if($(this).is(':visible') )
		{
			$('.popover').remove();
		}
	});
	popover_tabIndex = -1;
	
	paidHolidaySelectCount = 1;
}

var  paidHolidayButtonWidth = 0;
//---------------------------------------------------------------------------------------------------------------
//	有休追加・削除ボタン切り替え
//---------------------------------------------------------------------------------------------------------------
function switchPaidHolidayButtonVisible()
{
	if(paidHolidaySelectCount < 2)
	{
		$("#removeSelectGroupBtn").toggle(false);
		$("#removeSelectGroupBtn").width(0);
		$("#addSelectGroupBtn").width(paidHolidayButtonWidth)
		$("#addSelectGroupBtn").toggle(true);		
	}else{
		$("#removeSelectGroupBtn").width(paidHolidayButtonWidth)
		$("#removeSelectGroupBtn").toggle(true);
		$("#addSelectGroupBtn").toggle(false);
		$("#addSelectGroupBtn").width(0)
	}
}


//---------------------------------------------------------------------------------------------------------------
//	備考更新ボタンクリックハンドラ
//---------------------------------------------------------------------------------------------------------------
function clickUpdateNote(id)
{
	if(confirm("有給休暇 ・ 備考を更新します。\nよろしいでしょうか？"))
	{
		if(id == undefined) id = "";
		var values = [];
		$("select[name='paidHoliday']").each(function() {
			if($(this).val() != 0)
			{
				values.push($(this).val());
			}
		});
		
		//if(values.length == 0) values = "";
		
		updateAttendanceNote(id, $("input[name='date']").val(), values, $("textarea[name='note']").val())
	}
}



//================================================================================
//
//		承認取り消しボタンクリックハンドラ
//
//================================================================================
function clickApprovalCancel()
{
	var date		= $('#selectYear')[0].value + $('#selectMonth')[0].value;
	var emp_no	= $('#select_user')[0].value;
	var note		= $('#approval-cancel-note').val();
	var status	= 2;

	new $.ajax({
			url: "/admin/attendance/approval",
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data: {
				"approval_month": date
				, "emp_no":       emp_no
				, "status":       status
				, "note":       note
			},
			cache:false,
			type:"POST",
			dataType: "json",
			success: function() {
				window.location.href = "/admin/attendance/approval/" + date + "/-1"
			},
			error:errorAjax
	});
}



//================================================================================
//	Ajaxで社員情報削除
//================================================================================
function updateAttendanceNote(id, date, paidHoliday, note)
{
	var url = "/admin/attendance/updateNote";
	var data = "targetId=" + id + "&date=" + date  + "&note=" + note;
	for(var i=0;i<paidHoliday.length;i++)
	{
		 data +=  "&paidHoliday[]=" + paidHoliday[i];
	}
	
	
	/*var data = 
	{
			"targetId" : id,
			"date" : date,
			"paidHoliday" : paidHoliday,
			"note" :  note
	};*/

	new $.ajax({
			url:url,
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data: data,
			cache:false,
			type:"POST",
			dataType: "json",
			traditional: true,
			processData:false,
			async:true,
			beforeSend:beforeSend,
			success:successUpdateAttendanceNote,
			error:errorAjax
	});
}
//================================================================================
// Ajax通信開始前処理
//================================================================================
function beforeSend(xhr)
{
	var token = $('meta[name="csrf-token"]').attr('content');

	if(token)
	{
		return xhr.setRequestHeader('X-XSRF-TOKEN', token);
	}
}
//================================================================================
//	更新成功
//================================================================================
function successUpdateAttendanceNote(data, dataType)
{
	alert(data.message);
	//
	if(data.success)
	{
		location.reload();
	}
	//
}
//================================================================================
//	Ajax通信エラー
//================================================================================
function errorAjax(XMLHttpRequest, textStatus, errorThrown)
{
	alert(XMLHttpRequest.statusText + " " + XMLHttpRequest.status);
}

//============================================================
//
//		有給休暇セレクターの初期化
//
//============================================================		
function initSelectGroup(data)
{
	if(data != null)
	{
		var phSelect = $("#paidHoliday1");
		
		for(var i=0;i<data.length;i++)
		{
			if(i > 0)
			{
				paidHolidaySelectCount++;
				//
				phSelect = addSelectGroup();
			}
			phSelect.val(data[i])
		}
	}
}
//============================================================
//
//		有給休暇セレクター追加
//
//============================================================
function addSelectGroup()
{
	//	クローン
	var phSelect = $("#paidHoliday1").clone(true).insertBefore("#note");
	//	id変更
	phSelect.attr('id', 'paidHoliday' + paidHolidaySelectCount);
	//	追加ボタン削除
	phSelect.children("button[id='addSelectBtn']").remove();
	//	削除ボタン表示
	phSelect.children("button[id='removeSelectBtn']").show();
	//
	return  phSelect;
}
//============================================================
//
//		印刷画面の表示
//
//============================================================
function clickPrintLink()
{
	window.open(window.location.href + "?print=1", "employee.print", "resizable=yes,scrollbars=yes");
}
//============================================================
//
//		次の社員表示
//
//============================================================
function clickNextEmployee()
{
	var empNo = searchEmployee(1);
	if(empNo != null)
	{
		selectEmployee(empNo, getSelectedDate());
	}
}
//============================================================
//
//		前の社員表示
//
//============================================================
function clickPreviousEmployee()
{
	var empNo = searchEmployee(-1);
	if(empNo != null)
	{
		selectEmployee(empNo, getSelectedDate());
	}
}
//============================================================
//
//		年月取得
//
//============================================================
function getSelectedDate()
{
	var year = $("#selectYear").val();	
	var month = $("#selectMonth").val();
	return  year + month;
}
//============================================================
//
//		前後の社員番号の取得
//
//============================================================
function searchEmployee(step)
{
	//var count = $("#select_user").children('option').length;
	var index = $("#select_user").prop("selectedIndex");
	return $($("#select_user option")[index + step]).val();
}
//============================================================
//
//		任意の社員別勤怠表示
//
//============================================================
function selectEmployee(empNo, date)
{
		window.location.href = "/admin/attendance/employees/" + empNo + "/" + date; 
}