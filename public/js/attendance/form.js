var paidHolidaySelectCount = 1;

(function(window, $)
{
	/**
	 * windowロード時
	 */
	window.onload = function()
	{
		setDisplayEmpNo($('#emp_no')[0].value)
		
	
		changeDateFrom('start', false)
		changeDateFrom('end', false)

		
		setDivisionDisplay($('#divisionSelect').val());
		setPreviewDate($('#divisionSelect').val());
		
		setDivisionColor();
		
		setPaidHolidayColor();
	}

	$('[data-toggle=datepicker]').each(function () {
		var target = $(this).data('target') || '';
		if (target)
		{
			$(target).datepicker();
			$(this).bind("click", function () {
			$(target).datepicker("show");
			});
		}
	});
/*
	// datePicker開始
	$("#startDate").datepicker();
	$("[data-toggle=#startDate]").bind("click", function ()
	{
		$("#startDate").datepicker("show");
	});
	// datePicker開始
	$("#endDate").datepicker();
	$("[data-toggle=#endDate]").bind("click", function ()
	{
		$("#endDate").datepicker("show");
	});
*/
	// datePicker開始
	/*$("#startDate").datepicker({
		showOn: "both",
		buttonImage: "/images/calendar_icon.svg",
		buttonImageOnly: true
	});
	
	
	$("#endDate").datepicker({
		showOn: "both",
		buttonImage: "/images/calendar_icon.svg",
		buttonImageOnly: true
	});*/


	/**
	 * ユーザーリスト変更イベント
	 */
	$('#emp_no').change(function(e) {
		setDisplayEmpNo(e.target.value);
	})


	/**
	 * 時間フォーム変更イベント
	 */
	$('.attendance-date-form').change(function(e)
	{
		setPreviewDate($('#divisionSelect').val());
	})

	
	/**
	 * 勤務区分変更イベント
	 */
	$('#divisionSelect').change(function(e) {
		setDivisionDisplay(e.target.value)
		setDivisionColor();
	})
	
	/**
	 * 有給変更イベント
	 */
	$('#paidHoliday').change(function(e) {
		setPaidHolidayColor();
	})


	/**
	 * キャンセルボタンのイベント
	 */
	$('.resetButton').click(function(e) {
		window.location.href = CANCEL_URL
	})


	/**
	 * 追加ボタンのイベント
	 */
	$('.submitButton').click(function(e) {
	
		if ($(e.target).attr('disabled') == 'disabled') {
			return ;
		}
	
		var csrfToken = $('meta[name="csrf-token"]').attr('content')
		
		
		var values = [];
		$("select[name='paidHoliday']").each(function() {
			if($(this).val() != 0)
			{
				values.push($(this).val());
			}
		});
		
		var saveData  = {
			'targetId':     $(e.target).data('number')
			, 'empNo':      $("#display_emp_no")[0].value
			, 'startDate':  getFormDate('start')
			, 'endDate':    getFormDate('end')
			, 'divisionId': $("#divisionSelect").val() 
			, 'paidHoliday': values
			, 'note':       $("#note")[0].value
			, 'targetDate': TARGET_DATE
		}
	
		if ($("#start-date-form-active-checkbox")[0].checked == false) {
			delete saveData.startDate
		}
	
		if ($("#end-date-form-active-checkbox")[0].checked == false) {
			delete saveData.endDate
		}
	
		var okCallback = function(data, dataType)
		{
			//alert('更新しました')
			alert(data.message);
			window.location.href = CANCEL_URL
		}
	
		var ngCallback = function(XMLHttpRequest, textStatus, errorThrown) {
			//alert('更新できませんでした')
			alert(XMLHttpRequest.statusText + " " + XMLHttpRequest.status);
		}
	
		execSave(saveData, csrfToken, okCallback, ngCallback)
	})


	$("#start-date-form-active-checkbox").change(function(e) {
		changeDateFrom('start', e.target.checked);
		setPreviewDate($('#divisionSelect').val());
	})
	
	
	$("#end-date-form-active-checkbox").change(function(e) {
		changeDateFrom('end', e.target.checked);
		setPreviewDate($('#divisionSelect').val());
	})
	
	//$("#addSelectGroupBtn").hide()
	$("#removeSelectGroupBtn").hide()
	//============================================================
	//
	//		追加ボタン Clickハンドラ
	//
	//============================================================
	$("#addSelectGroupBtn").click(function()
	{
		if(paidHolidaySelectCount < 2)
		{
			//　カウントアップ
			paidHolidaySelectCount++;
			//
			addSelectGroup();
		}
	});
	//============================================================
	//
	//		削除ボタン Clickハンドラ
	//
	//============================================================		
	$("#removeSelectGroupBtn").click(function()
	{
		paidHolidaySelectCount--;
		
		$(this).parent("div").remove();
		
		 renumberSelectGroup();
	});
	
	initSelectGroup();

})(window, jQuery);


//============================================================
//
//		勤務区分のテキストカラー設定
//
//============================================================
function setDivisionColor()
{
	var color = $("#divisionSelect option:selected").css("color");
	var hex = RGB2HEX(color);
	if(color != null && hex != "#ffffff") $('#divisionSelect').css("color", hex);
}
//============================================================
//
//		勤務区分のテキストカラー設定
//
//============================================================
function setPaidHolidayColor()
{
	var color = $("#paidHoliday option:selected").css("color");
	var hex = RGB2HEX(color);
	if(color != null && hex != "#ffffff") $('#paidHoliday').css("color", hex);
}
//============================================================
//
//		有給休暇セレクター追加
//
//============================================================
function addSelectGroup()
{
	$("#addSelectGroupBtn").hide();
	//	クローン
	var selectG = $("#paidHolidaySelectGroup1").clone(true).insertBefore("#noteGroup");
	//	id変更
	selectG.attr('id', 'paidHolidaySelectGroup' + paidHolidaySelectCount);
	//	
	selectG.find("label[id='title']").text("休暇・休業" + paidHolidaySelectCount + ":");
	//	追加ボタン削除
	selectG.children("button[id='addSelectGroupBtn']").remove();
	//	削除ボタン表示
	selectG.children("button[id='removeSelectGroupBtn']").show();
	//
	return  selectG;
}
//============================================================
//
//		有給休暇セレクター再採番
//
//============================================================		
function renumberSelectGroup()
{
	$("#addSelectGroupBtn").show();
	
	var selectG = $("#noteGroup");
	
	for(var i=paidHolidaySelectCount;i>1;i--)
	{
		selectG = selectG.prev();
		//	id変更
		selectG.attr('id', 'paidHolidaySelectGroup' + i);
		
		selectG.find("label[id='title']").text("&#009;&#009;有休 " + i + ": ");
	}
}
//============================================================
//
//		有給休暇セレクターの初期化
//
//============================================================		
function initSelectGroup()
{
	if($(":hidden[name='paidHolidayData']").val() != null)
	{
		var paidHolidayData = JSON.parse( $(":hidden[name='paidHolidayData']").val());
		var selectG = $("#paidHolidaySelectGroup1");
		
		for(var i=0;i<paidHolidayData.length;i++)
		{
			if(i > 0)
			{
				paidHolidaySelectCount++;
				//
				selectG = addSelectGroup();
			}
			selectG.find("select[id='paidHoliday']").val(paidHolidayData[i])
			//alert(paidHolidayData[i]);				
		}
	}
}


function validateStartEndDatetime()
{
	var isExistStartDate;
	var isExistEndDate;
	
	var startDatetime;
	var endDatetime;

	var result = true;
	
	
	if($("#start-date-form-active-checkbox")[0].checked == true || $("#end-date-form-active-checkbox")[0].checked == true)
	{ 
	
		if($("#start-date-form-active-checkbox")[0].checked == true)
		{
			isExistStartDate = true;
			
			var startDate	= $('#startDate')[0].value;
			var startHour	= $('#startHour')[0].value;
			var startMinute	= $('#startMinute')[0].value;
			//
			startDatetime	= new Date(startDate + " " + startHour + ":" + startMinute);
			//
		}else{
			
			isExistStartDate = $("#isExistStartDate").val();
			
			if(isExistStartDate == true) startDatetime	= new Date($("#rawStartDate").val().replace(/-/g,'/'));
		}
		
		if($("#end-date-form-active-checkbox")[0].checked == true)
		{
			isExistEndDate = true;
			
			var endDate		= $('#endDate')[0].value;
			var endHour		= $('#endHour')[0].value;
			var endMinute	= $('#endMinute')[0].value;
			
			endDatetime		= new Date(endDate + " " + endHour + ":" + endMinute);
		}else{
			isExistEndDate = $("#isExistEndDate").val();
			
			if(isExistEndDate == true) endDatetime	= new Date($("#rawEndDate").val().replace(/-/g,'/'));
		}
		
		if(isExistStartDate == true && isExistEndDate == true)
		{
			//出勤・退勤両方存在するなら
			result = !(startDatetime.getTime() > endDatetime.getTime());
		
		}
	}
	
	return result;
}


