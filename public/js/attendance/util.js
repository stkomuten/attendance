(function(window, $){


// 時間の上限
DAYS_LIMIT = 48


/**
 * 表示用IDのvalueを更新
 */
setDisplayEmpNo = function(empNo) {
	$('#display_emp_no')[0].value = empNo
}



/**
 * 勤務区分表示を更新
 */
setDivisionDisplay = function(divisionID)
{
	var startDate = "--:--";
	var endDate = "--:--";
	
	if(divisionID != "")
	{	
		//var timeSheetAttr = timeSheet[parseInt(divisionID) - 1];
		var timeSheetAttr = findDivisionAttrByID(divisionID);
		
		startDate = convertDisplayTime(timeSheetAttr.opening_time);
		endDate = convertDisplayTime(timeSheetAttr.closing_time);
	}
	
	$('#preview-division-start-date').html(startDate); 
	$('#preview-division-end-date').html(endDate);
}

/**
 *	IDで勤務区分を検索
 */
findDivisionAttrByID = function(divisionID)
{
	var timeSheetAttr;
	var count = timeSheet.length;
	
	for(var i=0;i<count;i++)
	{
		if(divisionID == timeSheet[i].id)
		{
			timeSheetAttr = timeSheet[i];
			break;
		}
	}
	
	return timeSheetAttr;
}

/**
 *	Time型表記を変換
 */
convertDisplayTime = function(time)
{
	var hour = time.substr(0,2);
	var minutes = time.substr(3,2);

	return parseInt(hour) + ":" + minutes;
}




/**
 * 日付のリクエスト用文字列を取得
 */
getFormDate = function(type) {

	if (type != 'start' && type != 'end') {
		return null
	}

	var dateEM   = '#' + type + 'Date'
	var hourEM   = '#' + type + 'Hour'
	var minuteEM = '#' + type + 'Minute'

	if ($(dateEM)[0].value.length <= 0) {
		return '';
	}

	return $(dateEM)[0].value.replace('/', '').replace('/', '') + $(hourEM)[0].value + $(minuteEM)[0].value + "00"
}


/**
 * 時間の差分を取得
 */
getDiffHour = function(endHour, startDate, endDate) {

	// getTimeメソッドで経過ミリ秒を取得し、２つの日付の差を求める
	//var msDiff = date2.getTime() - date1.getTime()

	// 求めた差分（ミリ秒）を日付へ変換します（経過ミリ秒÷(1000ミリ秒×60秒×60分)
	//var diffhour = Math.floor(msDiff / (1000 * 60 * 60))
		
	var dateDiff = Math.floor((endDate.getTime() - startDate.getTime())/ (1000 * 60 * 60 * 24));
	
	return dateDiff * 24 + endHour;
}


/**
 * 計算された退勤時間を返す
 */
getCalculatedEndDate = function(startDate, startHour, startMinute, endDate, endHour, endMinute) {

	var date1 = new Date(startDate + " " + startHour + ":" + startMinute)
	var date2 = new Date(endDate + " " + endHour + ":" + endMinute)
	
	var startStandardDate = new Date(startDate + " 00:00" )
	var endStandardDate = new Date(endDate + " 00:00" )

	if(date1.getTime()  == date2.getTime() || date1.getTime() > date2.getTime())
	{
		return "--:--";	
	}
	
	if(startDate === endDate)
	{
		result = startHour;
	}else{
		// 差分の時間を取得
		var result = getDiffHour(Number(endHour), startStandardDate, endStandardDate);
		/*
		// 最大48時間で丸める
		if (result >= DAYS_LIMIT) {
			result  = DAYS_LIMIT
			endMinute = "00*"
		}*/
		//	開始時間も足す
		result = parseInt(result);
	}

	result += ":" + endMinute;

	return result;
}







/**
 * 実働時間の計算
 */
getCalculatedOriginDate = function(startDate, startHour, startMinute, endDate, endHour, endMinute, openingTime, closingTime, breakTimeStart, breakTime) {

	var date1 = new Date(startDate + " " + startHour + ":" + startMinute)
	var date2 = new Date(endDate + " " + endHour + ":" + endMinute)
	
	var startStandardDate = new Date(startDate + " 00:00" )
	var endStandardDate = new Date(endDate + " 00:00" )
	
	if(date1.getTime()  == date2.getTime() || date1.getTime() > date2.getTime())
	{
		return "--:--";	
	}

	// 差分の時間を取得
	var result = getDiffHour(Number(endHour), startStandardDate, endStandardDate);

	//	始業時間
	var startDateTime = new Date(startDate + " " + openingTime);


	//	休憩時間算出始まり
	var breqakTimeStartDate = (new Date("1970/01/01 " + breakTimeStart).getTime() - new Date("1970/01/01").getTime()) / 1000;
	
	var breakTimeSec =  (new Date("1970/01/01 " + breakTime).getTime() -  new Date("1970/01/01").getTime()) / 1000;


	//	開始日時が9時以上か判定
	if(date1 > startDateTime)
	{
		var min = 60 - Number(startMinute);
		
		if(min === 60) min = 0;
		
		var beLate = new Date(startDate + " " + startHour + ":" + startMinute)
		
		beLate.setMinutes(beLate.getMinutes()+min);
		
		startDateTime = beLate;
	}

	
	// 最大48時間で丸める
	/*if (result >= DAYS_LIMIT) {
		hour  = DAYS_LIMIT
		minute = "00"
	} else {*/
		//var datet = parseInt((date2.getTime() - date1.getTime()) / 1000);
		var datet = (date2.getTime() - startDateTime.getTime()) / 1000;
				
		if(datet < 0) return "--:--";	
		
		var calBreakTime =  datet > breqakTimeStartDate ? breakTimeSec : 0;
		
		datet -= calBreakTime;
		
		hour      = parseInt(datet / 3600);
		minute    = parseInt((datet / 60) % 60);

		if(minute < 10) minute = "0" + minute;

	//}

	return hour + ":" + minute;
}




/**
 * 実働時間の計算
 */
/*
getCalculatedOriginDate = function(startDate, startHour, startMinute, endDate, endHour, endMinute) {

	var date1 = new Date(startDate + " " + startHour + ":" + startMinute)
	var date2 = new Date(endDate + " " + endHour + ":" + endMinute)

	if(date1.getTime()  == date2.getTime() || date1.getTime() > date2.getTime())
	{
		return "--:--";	
	}

	// 差分の時間を取得
	var result = getDiffHour(date1, date2);

	//	始業時間
	var startDateTime = new Date(startDate + " 09:00:00");

	//	開始日時が9時以上か判定
	if(date1 >= startDateTime)
	{
		startDateTime = date1;
	}



	// 最大48時間で丸める
	/*if (result >= DAYS_LIMIT) {
		hour  = DAYS_LIMIT
		minute = "00"
	} else {*/
	/*
		//var datet = parseInt((date2.getTime() - date1.getTime()) / 1000);
		var datet = parseInt((date2.getTime() - startDateTime) / 1000);
		hour      = parseInt(datet / 3600) - 1;
		minute    = parseInt((datet / 60) % 60);

		if(minute < 10) minute = "0" + minute;

	//}

	return hour + ":" + minute;
}
*/

/**
 * プレビューを表示
 */
setPreviewDate = function(divisionID)
{
	var isExistStartDate = true;
	var isExistEndDate = true;
	
	var startDatetime;
	var endDatetime;
	
	var startDate;
	var endDate;
	
	// 日付
	var startDateAry;
	var date_ja_JP;

	// 出勤時間
	var startHour;
	var startMinute;

	// 退勤時間
	var endHour;
	var endMinute;
	
	
	// 退勤時間
	var endHour;
	var endMinute;
	
	if(divisionID != "")
	{
	
		//var timeSheetAttr = timeSheet[parseInt(divisionID) - 1];
		var timeSheetAttr = findDivisionAttrByID(divisionID);
		//	始業時間
		var openingTime = timeSheetAttr.opening_time;
		//	就業時間
		var closingTime = timeSheetAttr.closing_time;
		
		//	休憩時間算出始まり
		var breakTimeStart = timeSheetAttr.break_time_start;
		//	休憩時間
		var breakTime = timeSheetAttr.break_time;
	}

	if($("#start-date-form-active-checkbox")[0].checked == true)
	{
		startDate = $('#startDate')[0].value;
		
		if(!startDate) isExistStartDate = false;
		
		if(isExistStartDate == true)
		{
			startHour   = $('#startHour')[0].value;
			startMinute = $('#startMinute')[0].value;
		}
		
	}else{
		
		isExistStartDate = $("#isExistStartDate").val();
			
		if(isExistStartDate == true)
		{
			startDatetime	= $("#rawStartDate").val().replace(/-/g,'/');
			
			startDate = startDatetime.substr(0, 10);
			startHour = startDatetime.substr(11, 2);
			startMinute = startDatetime.substr(14, 2);
		}
	}
	
	if($("#end-date-form-active-checkbox")[0].checked == true)
	{
		endDate = $('#endDate')[0].value;
		
		if(!endDate) isExistEndDate = false;
		
		if(isExistEndDate == true)
		{
			endHour   = $('#endHour')[0].value;
			endMinute = $('#endMinute')[0].value;
		}
	}else{
		isExistEndDate = $("#isExistEndDate").val();
			
		if(isExistEndDate == true)
		{
			endDatetime	= $("#rawEndDate").val().replace(/-/g,'/');
			endDate = endDatetime.substr(0, 10);
			endHour = endDatetime.substr(11, 2);
			endMinute = endDatetime.substr(14, 2);
		}
	}
	
	if(isExistStartDate == true)
	{
		startDateAry = startDate.split('/')
		$("#preview-day").html(Number(startDateAry[1]) + "月" + Number(startDateAry[2]) + "日");
		$("#preview-start-date").html(Number(startHour) + ":" + startMinute);
	}else{
		
		$("#preview-day").html('--月--日');
		$("#preview-start-date").html('--:--');
	}
	
	if(isExistStartDate == true && isExistEndDate == true)
	{
		if (startDate == endDate)
		{
			$("#preview-end-date").html(Number(endHour) + ":" + endMinute);
		}else{
			var calcEndDate = getCalculatedEndDate(startDate, startHour, startMinute, endDate, endHour, endMinute)
			$("#preview-end-date").html(calcEndDate);
		}
		if(breakTimeStart  == null || breakTime == null)
		{
			$("#preview-origin-date").html('--:--');
		}else{
			// 実働時間
			var calcOriginDate = getCalculatedOriginDate(startDate, startHour, startMinute, endDate, endHour, endMinute, openingTime, closingTime, breakTimeStart, breakTime)
			$("#preview-origin-date").html(calcOriginDate);
		}
	}else{
		
		$("#preview-end-date").html('--:--');
		$("#preview-origin-date").html('--:--');

	}
}


/**
 * 削除実行
 */
execDelete = function(targetId, csrfToken, okCallback, ngCallback) {
	if(window.confirm('勤怠記録を削除します。\nよろしいでしょうか？')) {
		$.ajax({
			"url":        attendanceURL("delete")
			, "type":     "DELETE"
			, "cache":    false
			, "dataType": "json"
			, "error":    ngCallback
			, "success":  okCallback
			, "headers":  { 'X-CSRF-TOKEN': csrfToken }
			, "data":     { 'targetId': targetId }
		});
	}
}


/**
 * 保存実行
 */
execSave = function(saveData, csrfToken, okCallback, ngCallback)
{
	if(validateStartEndDatetime() == true)
	{
		//if(window.confirm('勤怠記録を更新します。\nよろしいでしょうか？')) {
			$.ajax({
				"url":        attendanceURL("save")
				, "type":     "POST"
				, "cache":    false
				, "dataType": "json"
				, "headers":  { 'X-CSRF-TOKEN': csrfToken }
				, "data":     saveData
				, "error":    ngCallback
				, "success":  okCallback
			});
		//}
	}else{
		alert('退勤日時が出勤日時より過去に設定されています。');	
	}
}


/**
 * dailyページへ移動
 */
jumpToDailyPage = function(path) {
	if (!path) {
		path = ""
	}

	var baseURL = attendanceURL('daily')
	jumpURL = baseURL + "/" + path

	window.location.href = jumpURL
}


/**
 * employeesページヘ移動
 */
jumpToEmployeesPage = function(path) {
	if (!path) {
		path = ""
	}

	var baseURL = attendanceURL('employees')
	jumpURL = baseURL + "/" + path

	window.location.href = jumpURL
}


/**
 * attendance内のリンクURLを取得
 */
attendanceURL = function(path) {
	if (!path) {
		path = ""
	}

	return "/admin/attendance/" + path
}


/**
 * 時間のフォームの活性比活性
 */
changeDateFrom = function(formType, checkboxState) {
	var dateId   = $('#' + formType + 'Date')
	var hourId   = $('#' + formType + 'Hour')
	var minuteId = $('#' + formType + 'Minute')

	if (checkboxState == true) {
		dateId.prop("disabled", false)
		hourId.prop("disabled", false)
		minuteId.prop("disabled", false)
		dateId.css("background-color", 'white')
		hourId.css("background-color", 'white')
		minuteId.css("background-color", 'white')
		dateId.datepicker("option", "disabled", false);
	} else {
		dateId.prop("disabled", true)
		hourId.prop("disabled", true)
		minuteId.prop("disabled", true)
		dateId.css("background-color", 'lightgray')
		hourId.css("background-color", 'lightgray')
		minuteId.css("background-color", 'lightgray')
		dateId.datepicker("option", "disabled", true);
	}
}

RGB2HEX = function(col)
{
	return "#" + col.match(/\d+/g).map(function(a){return ("0" + parseInt(a).toString(16)).slice(-2)}).join("");
}


})(window, jQuery);