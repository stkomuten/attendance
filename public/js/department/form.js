(function(window, $){


/**
 * 名前変更フォームイベント
 */
$('#department_name').change(function(e){

    $('.submitButton').attr('disabled', false);
    if (!e.target.value) {
        $('.submitButton').attr('disabled', true)
    }
})


/**
 * 追加ボタンのイベント
 */
$('.submitButton').click(function(e) {

    var csrfToken = $('meta[name="csrf-token"]').attr('content')
    var saveData  = {
        'targetId': $(e.target).data('number')
        , 'name':   $('#department_name')[0].value
    }

    var okCallback = function() {
        alert('保存に成功しました')
        window.location.href = "/admin/department"
    }

    var ngCallback = function() {
        alert('保存に失敗しました')
    }

    if(window.confirm('保存しますか？')) {
        $.ajax({
            "url":        '/admin/department/save'
            , "type":     "POST"
            , "cache":    false
            , "dataType": "json"
            , "headers":  { 'X-CSRF-TOKEN': csrfToken }
            , "data":     saveData
            , "error":    ngCallback
            , "success":  okCallback
        });
    }
})

/**
 * キャンセルボタンのイベント
 */
$('.resetButton').click(function(e) {
	window.location.href = "/admin/department"
})


})(window, jQuery);
