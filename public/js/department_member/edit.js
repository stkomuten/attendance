(function(window, $){



/**
 * ポジションリストを取得
 */
function getPositionList() {
    var positionList = []

    $('.department-position-list').each(function(key, value) {
        var emp_no      = $(value).data('empno')
        var position_id = $(value).find('select option:selected')[0].value

        var data = {}
        data.emp_no      = emp_no
        data.position_id = position_id
        positionList[key] = data
    })

    return positionList
}


/**
 * 所属・未所属の要素の入れ替え
 */
function changeOption(selectedList, unSelectedList) {
    if (selectedList.length == 0) {
        return ;
    }

    for (var i = 0; i < selectedList.length; i++) {
        var registValue = selectedList[i].value
        var registText  = selectedList[i].text
        $option = $("<option>", { value: registValue, text: selectedList[i].text });

        unSelectedList.append($option)
    }

    selectedList.remove();

    setPositionType()
}


/**
 * ポジションHTMLの挿入
 */
function setPositionType() {
    var registUserList  = []
    var _registUesrList = $('#registUserList option')
    for (var i = 0; i < _registUesrList.length; i++) {
        var emp_no = _registUesrList[i].value
        var text   = _registUesrList[i].text
        registUserList[emp_no] = text
    }

    var unRegistUserList  = []
    var _unRegistUserList = $('#unRegistUserList option')
    for (var i = 0; i < _unRegistUserList.length; i++) {
        var emp_no = _unRegistUserList[i].value
        var text   = _unRegistUserList[i].text
        unRegistUserList[emp_no] = text
    }

    var positionList  = []
    var _positionList = getPositionList()
    for (var i = 0; i < _positionList.length; i++) {
        var emp_no = _positionList[i].emp_no
        positionList[emp_no] = emp_no
    }

    for (var emp_no in unRegistUserList) {
        if (positionList[emp_no]) {
            $('*[data-empno="' + emp_no + '"]').remove()
        }
    }

    for (var emp_no in registUserList) {
        if (!positionList[emp_no]) {
            $("#department_position_type").append('<tr class="department-position-list" data-empno="' + emp_no + '"> \
                <td>' + registUserList[emp_no] + '</td> \
                <td>' + $POSITION_TYPE_SELECT[0].outerHTML + '</td> \
            </tr>')
        }
    }
}


/**
 * 追加ボタン
 */
$('#registButton').click(function() {
    var selectedList   = $('#unRegistUserList option:selected')
    var unSelectedList = $('#registUserList')

    changeOption(selectedList, unSelectedList)
})


/**
 * 削除ボタン
 */
$('#deleteButton').click(function() {
    var selectedList   = $('#registUserList option:selected')
    var unSelectedList = $('#unRegistUserList')

    changeOption(selectedList, unSelectedList)
})


/**
 * 保存ボタン
 */
$('.submitButton').click(function(e) {
    var valueList  = []
    var optionList = $('#registUserList option')
    for (var i = 0; i < optionList.length; i++) {
        valueList[i] = optionList[i].value
    }

    var positionList = getPositionList()
    var csrfToken = $('meta[name="csrf-token"]').attr('content')
    var saveData  = {
        'targetId':       $(e.target).data('number')
        , 'valueList':    valueList
        , 'positionList': positionList
    }

    var okCallback = function() {
        alert('保存に成功しました')
        window.location.reload()
    }

    var ngCallback = function() {
        alert('保存に失敗しました')
    }

    if(window.confirm('保存しますか？')) {
        $.ajax({
            "url":        '/admin/department/member/save'
            , "type":     "POST"
            , "cache":    false
            , "dataType": "json"
            , "headers":  { 'X-CSRF-TOKEN': csrfToken }
            , "data":     saveData
            , "error":    ngCallback
            , "success":  okCallback
        });
    }
})

/**
 * キャンセルボタンのイベント
 */
$('.resetButton').click(function(e) {
	window.location.href = CANCEL_URL
})


})(window, jQuery);
