(function(window, $)
{
	
	//==================================================
	//	勤怠記録CSVダウンロードボタンをデリゲート
	//==================================================
	$('#download1-btn').click(function()
	{
		downloadAttendanceCSV();
	})

	//==================================================
	//	入退館CSVダウンロードボタンをデリゲート
	//==================================================
	$('#download2-btn').click(function()
	{
		downloadEntranceExitCSV();
	})
	
	//==================================================
	//	勤怠分析用CSVダウンロードボタンをデリゲート
	//==================================================
	$('#download3-btn').click(function()
	{
		analyzeAttendanceCSV();
	})
	
	
	
	//==================================================
	//	勤怠記録CSVダウンロード
	//==================================================
	downloadAttendanceCSV = function()
	{
		
		if(window.confirm("勤怠記録CSVをダウンロードします。よろしいですか?") == false)
		{
			return ;
		}

		// FormDataオブジェクトを用意
		//var fd = $form.serialize();
		var data = "ym=" + $("#yearMonth1").val();
		// csrftoken取得
		var token = $('meta[name="csrf-token"]').attr('content')

		$.ajax({
				url:'/admin/download/employees',
				type:'POST',
				Accept:"text/csv; charset=utf-8","Content-Type": "text/csv; charset=utf-8",
				data: data,
				cache:false,
				headers:  { 'X-CSRF-TOKEN': token },
				processData: false,
				async:true,
				beforeSend:beforeSend,
				success: successDownloadCSV,
				error: function(response)
				{
					alert("ダウンロードできませんでした。")
				}
				})
	}
	//==================================================
	//	入退館CSVダウンロード
	//==================================================
	downloadEntranceExitCSV = function()
	{
		
		if(window.confirm("入退館CSVをダウンロードします。よろしいですか?") == false)
		{
			return ;
		}

		// FormDataオブジェクトを用意
		//var fd = $form.serialize();
		var data = "ym=" + $("#yearMonth2").val();
		// csrftoken取得
		var token = $('meta[name="csrf-token"]').attr('content')

		$.ajax({
				url:'/admin/download/entranceExit',
				type:'POST',
				Accept:"application/json; charset=utf-8","Content-Type": "application/json; charset=utf-8",
				data: data,
				cache:false,
				headers:  { 'X-CSRF-TOKEN': token },
				processData: false,
				async:true,
				beforeSend:beforeSend,
				success: successDownloadEntranceExitCSV,
				error: function(response)
				{
					alert("ダウンロードできませんでした。")
				}
				})
	}
	//==================================================
	//	勤怠分析用CSVダウンロード
	//==================================================
	analyzeAttendanceCSV = function()
	{
		
		if(window.confirm("勤怠分析用CSVをダウンロードします。よろしいですか?") == false)
		{
			return ;
		}

		// FormDataオブジェクトを用意
		//var fd = $form.serialize();
		var data = "ym=" + $("#yearMonth3").val();
		// csrftoken取得
		var token = $('meta[name="csrf-token"]').attr('content')

		$.ajax({
				url:'/admin/download/analyzeAttendance',
				type:'POST',
				Accept:"application/json; charset=utf-8","Content-Type": "application/json; charset=utf-8",
				data: data,
				cache:false,
				headers:  { 'X-CSRF-TOKEN': token },
				processData: false,
				async:true,
				beforeSend:beforeSend,
				success: successDownloadEntranceExitCSV,
				error: function(response)
				{
					alert("ダウンロードできませんでした。")
				}
				})
	}

})(window, jQuery);
//================================================================================
//	更新成功
//================================================================================
function successDownloadCSV(data, dataType)
{					
	if(data.success == true)
	{
		window.location.assign(data.html);
	}else{
		alert(data.message);		
	}

}
//================================================================================
//	入退館CSVダウンロード成功
//================================================================================
function successDownloadEntranceExitCSV(data, dataType)
{	
	if(data.success == true)
	{
		window.location.assign(data.html);
	}else{
		alert(data.message);		
	}
}
//================================================================================
// Ajax通信開始前処理
//================================================================================
function beforeSend(xhr)
{
	var token = $('meta[name="csrf-token"]').attr('content');

	if(token)
	{
		return xhr.setRequestHeader('X-XSRF-TOKEN', token);
	}
}