$(document).ready(function()
{
	$.ajaxSetup({
        headers: { "X-CSRF-TOKEN": $.cookie("csrf-token") }
    });

});

//---------------------------------------------------------------------------------------------------------------
//	バリデーション完了ハンドラ
//---------------------------------------------------------------------------------------------------------------
function onValidationComplet(form, status)
{
	if(status == true)
	{
		addUpdateEmployee();
	}
}

//================================================================================
//	Ajaxで社員情報送信
//================================================================================
function addUpdateEmployee()
{
	var $form = $("#empForm");

	var mode = $('meta[name="mode"]').attr('content');

	var url = "/admin/employee/" + mode;
	var data = $form.serialize();
	new $.ajax({
			url:url,
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:data,
			cache:false,
			type:"POST",
			dataType: "json",
			processData:false,
			async:true,
			beforeSend:beforeSend,
			success:successAddRemoveEmployee,
			error:errorAjax
	});
}


//================================================================================
//	Ajaxで社員情報削除
//================================================================================
function removeEmployee(emp_no)
{
	var url = "/admin/employee/remove";
	var data = "emp_no="+emp_no;
	new $.ajax({
			url:url,
			headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data:data,
			cache:false,
			type:"POST",
			dataType: "json",
			processData:false,
			async:true,
			beforeSend:beforeSend,
			success:successAddRemoveEmployee,
			error:errorAjax
	});
}
//================================================================================
// Ajax通信開始前処理
//================================================================================
function beforeSend(xhr)
{
	var token = $('meta[name="csrf-token"]').attr('content');

	if(token)
	{
		return xhr.setRequestHeader('X-XSRF-TOKEN', token);
	}
}
//================================================================================
//	追加成功
//================================================================================
function successAddRemoveEmployee(data, dataType)
{
	//
	alert(data.message);
	//
	if(data.success == true)
	{
		//	社員情報一覧に遷移
		window.location.href = "/admin/employee";
	}
}
//================================================================================
//	Ajax通信エラー
//================================================================================
function errorAjax(XMLHttpRequest, textStatus, errorThrown)
{
	alert(XMLHttpRequest.statusText + " " + XMLHttpRequest.status);
}
//---------------------------------------------------------------------------------------------------------------
//	追加・更新ボタンクリックハンドラ
//---------------------------------------------------------------------------------------------------------------
function clickAddUpdate(mode)
{
	$("#empForm").validationEngine("hideAll");

	var message = mode == "add" ? "新規に社員を追加します。\nよろしいでしょうか？" : "社員情報を更新します。\nよろしいでしょうか？"

	if(confirm(message))
	{
		$("#empForm").validationEngine('validate');
	}
}
//---------------------------------------------------------------------------------------------------------------
//	キャンセルボタンクリックハンドラ
//---------------------------------------------------------------------------------------------------------------
function clickCancel()
{
	if (confirm("入力中の情報が破棄されます。\nよろしいでしょうか？"))
	{
		//	社員情報一覧に遷移
		window.location.href = "/admin/employee";
	}
}
//---------------------------------------------------------------------------------------------------------------
//	削除ボタンクリックハンドラ
//---------------------------------------------------------------------------------------------------------------
function clickRemove(emp_no)
{
	if(confirm("社員情報を削除します。\nよろしいでしょうか？"))
	{
		//	社員情報を削除
		removeEmployee(emp_no);
	}
}
//---------------------------------------------------------------------------------------------------------------
//	フィルタ変更 ハンドラ
//---------------------------------------------------------------------------------------------------------------
function changeStatus()
{
	var status = $("#emp_status").val();
	var url = "/admin/employee/";

	if(status > 0)
	{
		//
		url += status;
	}
	//	社員情報一覧に遷移
	window.location.href = url;
}
