(function(window, $){

	//==================================================
	//	JIRAに実働時間をエクスポートするボタンをデリゲート
	//==================================================
	$('#upload1-btn').click(function()
	{
		uploadWorkingHours2JIRA();
	})

	//==================================================
	//	JIRAに実働時間をアップロードする
	//==================================================
	uploadWorkingHours2JIRA = function()
	{
		
		if(window.confirm("JIRAに実働時間をアップロードします。よろしいですか?") == false)
		{
			return ;
		}

		// FormDataオブジェクトを用意
		//var fd = $form.serialize();
		var data = "emp_no=" + $("#select_emp").val();
		// csrftoken取得
		var token = $('meta[name="csrf-token"]').attr('content')

		$.ajax({
				url:'/admin/upload/uploadWorkingHours2JIRA',
				type:'POST',
				Accept:"application/json; charset=utf-8","Content-Type": "application/json; charset=utf-8",
				data: data,
				cache:false,
				headers:  { 'X-CSRF-TOKEN': token },
				processData: false,
				async:true,
				beforeSend:beforeSend,
				success: successExportWorkingHours2JIRA,
				error: function(response)
				{
					alert("アップロードできませんでした。")
				}
				})
	}

})(window, jQuery);
//================================================================================
// Ajax通信開始前処理
//================================================================================
function beforeSend(xhr)
{
	var token = $('meta[name="csrf-token"]').attr('content');

	if(token)
	{
		return xhr.setRequestHeader('X-XSRF-TOKEN', token);
	}
}
//================================================================================
//	エクスポート成功
//================================================================================
function successExportWorkingHours2JIRA(data, dataType)
{					
	if(data.success == true)
	{
		$("#result").html(data.html);
	}else{
		alert(data.message);		
	}

}