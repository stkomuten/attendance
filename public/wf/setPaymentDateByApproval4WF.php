<?php
	
	$AUTH_URL			= "https://workflow.nextset.jp/st2020/api/public/auth";
	$UPDATE_URL			= "https://workflow.nextset.jp/st2020/api/public/actions/update";
	
	$api_key			= "59437708fc891ab343f3889598320b30";
	$access_token		= "";
	$impersonate_email	= "mine@starttoday.jp";
	$application_id		= "default";
	$viewer_email		= "yumi.suzuki@starttoday.jp";
	
	//	認証
	$data = "api_key=" . $api_key . "&impersonate_email=" . $impersonate_email;
	
	$result = json_decode(connectionByCURL($AUTH_URL, $data),true);
	//	トークン格納
	$access_token = (string)$result["access_token"];
	//	書類版後取得
	$doc_id = $_GET["doc_id"];
	//	東京での今日の日付取得
	date_default_timezone_set('Asia/Tokyo');
	$finalApprovedDate = date("Y-m-d");

	$payment_date_list = getPaymentDate($finalApprovedDate); //10日 もしくは 25日

	$data = "access_token=" . $access_token ."&impersonate_email=" . $impersonate_email . "&application_id=" . $application_id . "&viewer_email=" . $viewer_email . "&doc_id=" . $doc_id . "&doc_values=" . json_encode(array("payment_date_list"=>$payment_date_list));
	
	connectionByCURL($UPDATE_URL, $data);
	
	//================================================================================
	//
	//	CURL
	//
	//================================================================================
	function connectionByCURL($url, $data)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		curl_close($ch);
		
		return $result;
	}
	//================================================================================
	//
	//	支払い日の取得
	//
	//================================================================================
	function getPaymentDate($approvalDate)
	{
		$holidays = [
					'2015-01-01' => '元日',
					'2015-01-12' => '成人の日',
					'2015-02-11' => '建国記念の日',
					'2015-03-21' => '春分の日',
					'2015-04-29' => '昭和の日',
					'2015-05-03' => '憲法記念日',
					'2015-05-04' => 'みどりの日',
					'2015-05-05' => 'こどもの日',
					'2015-05-06' => '振替休日',
					'2015-07-20' => '海の日',
					'2015-09-21' => '敬老の日',
					'2015-09-22' => '国民の休日',
					'2015-09-23' => '秋分の日',
					'2015-10-12' => '体育の日',
					'2015-11-03' => '文化の日',
					'2015-11-23' => '勤労感謝の日',
					'2015-12-23' => '天皇誕生日',
					'2016-01-01' => '元日',
					'2016-01-11' => '成人の日',
					'2016-02-11' => '建国記念の日',
					'2016-03-20' => '春分の日',
					'2016-03-21' => '振替休日',
					'2016-04-29' => '昭和の日',
					'2016-05-03' => '憲法記念日',
					'2016-05-04' => 'みどりの日',
					'2016-05-05' => 'こどもの日',
					'2016-07-18' => '海の日',
					'2016-08-11' => '山の日',
					'2016-09-19' => '敬老の日',
					'2016-09-22' => '秋分の日',
					'2016-10-10' => '体育の日',
					'2016-11-03' => '文化の日',
					'2016-11-23' => '勤労感謝の日',
					'2016-12-23' => '天皇誕生日',
					'2017-01-01' => '元日',
					'2017-01-02' => '振替休日',
					'2017-01-09' => '成人の日',
					'2017-02-11' => '建国記念の日',
					'2017-03-20' => '春分の日',
					'2017-04-29' => '昭和の日',
					'2017-05-03' => '憲法記念日',
					'2017-05-04' => 'みどりの日',
					'2017-05-05' => 'こどもの日',
					'2017-07-17' => '海の日',
					'2017-08-11' => '山の日',
					'2017-09-18' => '敬老の日',
					'2017-09-23' => '秋分の日',
					'2017-10-09' => '体育の日',
					'2017-11-03' => '文化の日',
					'2017-11-23' => '勤労感謝の日',
					'2017-12-23' => '天皇誕生日',
					'2018-01-01' => '元日',
					'2018-01-08' => '成人の日',
					'2018-02-11' => '建国記念の日',
					'2018-02-12' => '振替休日',
					'2018-03-21' => '春分の日',
					'2018-04-29' => '昭和の日',
					'2018-04-30' => '振替休日',
					'2018-05-03' => '憲法記念日',
					'2018-05-04' => 'みどりの日',
					'2018-05-05' => 'こどもの日',
					'2018-07-16' => '海の日',
					'2018-08-11' => '山の日',
					'2018-09-17' => '敬老の日',
					'2018-09-23' => '秋分の日',
					'2018-09-24' => '振替休日',
					'2018-10-08' => '体育の日',
					'2018-11-03' => '文化の日',
					'2018-11-23' => '勤労感謝の日',
					'2018-12-23' => '天皇誕生日',
					'2018-12-24' => '振替休日',
					'2019-01-01' => '元日',
					'2019-01-14' => '成人の日',
					'2019-02-11' => '建国記念の日',
					'2019-03-21' => '春分の日',
					'2019-04-29' => '昭和の日',
					'2019-05-03' => '憲法記念日',
					'2019-05-04' => 'みどりの日',
					'2019-05-05' => 'こどもの日',
					'2019-05-06' => '振替休日',
					'2019-07-15' => '海の日',
					'2019-08-11' => '山の日',
					'2019-08-12' => '振替休日',
					'2019-09-16' => '敬老の日',
					'2019-09-23' => '秋分の日',
					'2019-10-14' => '体育の日',
					'2019-11-03' => '文化の日',
					'2019-11-04' => '振替休日',
					'2019-11-23' => '勤労感謝の日',
					'2019-12-23' => '天皇誕生日',
					'2020-01-01' => '元日',
					'2020-01-13' => '成人の日',
					'2020-02-11' => '建国記念の日',
					'2020-03-20' => '春分の日',
					'2020-04-29' => '昭和の日',
					'2020-05-03' => '憲法記念日',
					'2020-05-04' => 'みどりの日',
					'2020-05-05' => 'こどもの日',
					'2020-05-06' => '振替休日',
					'2020-07-20' => '海の日',
					'2020-08-11' => '山の日',
					'2020-09-21' => '敬老の日',
					'2020-09-22' => '秋分の日',
					'2020-10-12' => '体育の日',
					'2020-11-03' => '文化の日',
					'2020-11-23' => '勤労感謝の日',
					'2020-12-23' => '天皇誕生日',
					'2021-01-01' => '元日',
					'2021-01-11' => '成人の日',
					'2021-02-11' => '建国記念の日',
					'2021-03-20' => '春分の日',
					'2021-04-29' => '昭和の日',
					'2021-05-03' => '憲法記念日',
					'2021-05-04' => 'みどりの日',
					'2021-05-05' => 'こどもの日',
					'2021-07-19' => '海の日',
					'2021-08-11' => '山の日',
					'2021-09-20' => '敬老の日',
					'2021-09-23' => '秋分の日',
					'2021-10-11' => '体育の日',
					'2021-11-03' => '文化の日',
					'2021-11-23' => '勤労感謝の日',
					'2021-12-23' => '天皇誕生日',
					'2022-01-01' => '元日',
					'2022-01-10' => '成人の日',
					'2022-02-11' => '建国記念の日',
					'2022-03-21' => '春分の日',
					'2022-04-29' => '昭和の日',
					'2022-05-03' => '憲法記念日',
					'2022-05-04' => 'みどりの日',
					'2022-05-05' => 'こどもの日',
					'2022-07-18' => '海の日',
					'2022-08-11' => '山の日',
					'2022-09-19' => '敬老の日',
					'2022-09-23' => '秋分の日',
					'2022-10-10' => '体育の日',
					'2022-11-03' => '文化の日',
					'2022-11-23' => '勤労感謝の日',
					'2022-12-23' => '天皇誕生日',
					'2023-01-01' => '元日',
					'2023-01-02' => '振替休日',
					'2023-01-09' => '成人の日',
					'2023-02-11' => '建国記念の日',
					'2023-03-21' => '春分の日',
					'2023-04-29' => '昭和の日',
					'2023-05-03' => '憲法記念日',
					'2023-05-04' => 'みどりの日',
					'2023-05-05' => 'こどもの日',
					'2023-07-17' => '海の日',
					'2023-08-11' => '山の日',
					'2023-09-18' => '敬老の日',
					'2023-09-23' => '秋分の日',
					'2023-10-09' => '体育の日',
					'2023-11-03' => '文化の日',
					'2023-11-23' => '勤労感謝の日',
					'2023-12-23' => '天皇誕生日',
					'2024-01-01' => '元日',
					'2024-01-08' => '成人の日',
					'2024-02-11' => '建国記念の日',
					'2024-02-12' => '振替休日',
					'2024-03-20' => '春分の日',
					'2024-04-29' => '昭和の日',
					'2024-05-03' => '憲法記念日',
					'2024-05-04' => 'みどりの日',
					'2024-05-05' => 'こどもの日',
					'2024-05-06' => '振替休日',
					'2024-07-15' => '海の日',
					'2024-08-11' => '山の日',
					'2024-08-12' => '振替休日',
					'2024-09-16' => '敬老の日',
					'2024-09-22' => '秋分の日',
					'2024-09-23' => '振替休日',
					'2024-10-14' => '体育の日',
					'2024-11-03' => '文化の日',
					'2024-11-04' => '振替休日',
					'2024-11-23' => '勤労感謝の日',
					'2024-12-23' => '天皇誕生日',
					'2025-01-01' => '元日',
					'2025-01-13' => '成人の日',
					'2025-02-11' => '建国記念の日',
					'2025-03-20' => '春分の日',
					'2025-04-29' => '昭和の日',
					'2025-05-03' => '憲法記念日',
					'2025-05-04' => 'みどりの日',
					'2025-05-05' => 'こどもの日',
					'2025-05-06' => '振替休日',
					'2025-07-21' => '海の日',
					'2025-08-11' => '山の日',
					'2025-09-15' => '敬老の日',
					'2025-09-23' => '秋分の日',
					'2025-10-13' => '体育の日',
					'2025-11-03' => '文化の日',
					'2025-11-23' => '勤労感謝の日',
					'2025-11-24' => '振替休日',
					'2025-12-23' => '天皇誕生日',
					'2026-01-01' => '元日',
					'2026-01-12' => '成人の日',
					'2026-02-11' => '建国記念の日',
					'2026-03-20' => '春分の日',
					'2026-04-29' => '昭和の日',
					'2026-05-03' => '憲法記念日',
					'2026-05-04' => 'みどりの日',
					'2026-05-05' => 'こどもの日',
					'2026-05-06' => '振替休日',
					'2026-07-20' => '海の日',
					'2026-08-11' => '山の日',
					'2026-09-21' => '敬老の日',
					'2026-09-22' => '国民の休日',
					'2026-09-23' => '秋分の日',
					'2026-10-12' => '体育の日',
					'2026-11-03' => '文化の日',
					'2026-11-23' => '勤労感謝の日',
					'2026-12-23' => '天皇誕生日',
					'2027-01-01' => '元日',
					'2027-01-11' => '成人の日',
					'2027-02-11' => '建国記念の日',
					'2027-03-21' => '春分の日',
					'2027-03-22' => '振替休日',
					'2027-04-29' => '昭和の日',
					'2027-05-03' => '憲法記念日',
					'2027-05-04' => 'みどりの日',
					'2027-05-05' => 'こどもの日',
					'2027-07-19' => '海の日',
					'2027-08-11' => '山の日',
					'2027-09-20' => '敬老の日',
					'2027-09-23' => '秋分の日',
					'2027-10-11' => '体育の日',
					'2027-11-03' => '文化の日',
					'2027-11-23' => '勤労感謝の日',
					'2027-12-23' => '天皇誕生日',
					'2028-01-01' => '元日',
					'2028-01-10' => '成人の日',
					'2028-02-11' => '建国記念の日',
					'2028-03-20' => '春分の日',
					'2028-04-29' => '昭和の日',
					'2028-05-03' => '憲法記念日',
					'2028-05-04' => 'みどりの日',
					'2028-05-05' => 'こどもの日',
					'2028-07-17' => '海の日',
					'2028-08-11' => '山の日',
					'2028-09-18' => '敬老の日',
					'2028-09-22' => '秋分の日',
					'2028-10-09' => '体育の日',
					'2028-11-03' => '文化の日',
					'2028-11-23' => '勤労感謝の日',
					'2028-12-23' => '天皇誕生日',
					'2029-01-01' => '元日',
					'2029-01-08' => '成人の日',
					'2029-02-11' => '建国記念の日',
					'2029-02-12' => '振替休日',
					'2029-03-20' => '春分の日',
					'2029-04-29' => '昭和の日',
					'2029-04-30' => '振替休日',
					'2029-05-03' => '憲法記念日',
					'2029-05-04' => 'みどりの日',
					'2029-05-05' => 'こどもの日',
					'2029-07-16' => '海の日',
					'2029-08-11' => '山の日',
					'2029-09-17' => '敬老の日',
					'2029-09-23' => '秋分の日',
					'2029-09-24' => '振替休日',
					'2029-10-08' => '体育の日',
					'2029-11-03' => '文化の日',
					'2029-11-23' => '勤労感謝の日',
					'2029-12-23' => '天皇誕生日',
					'2029-12-24' => '振替休日',
					'2030-01-01' => '元日',
					'2030-01-14' => '成人の日',
					'2030-02-11' => '建国記念の日',
					'2030-03-20' => '春分の日',
					'2030-04-29' => '昭和の日',
					'2030-05-03' => '憲法記念日',
					'2030-05-04' => 'みどりの日',
					'2030-05-05' => 'こどもの日',
					'2030-05-06' => '振替休日',
					'2030-07-15' => '海の日',
					'2030-08-11' => '山の日',
					'2030-08-12' => '振替休日',
					'2030-09-16' => '敬老の日',
					'2030-09-23' => '秋分の日',
					'2030-10-14' => '体育の日',
					'2030-11-03' => '文化の日',
					'2030-11-04' => '振替休日',
					'2030-11-23' => '勤労感謝の日',
					'2030-12-23' => '天皇誕生日',
					'2031-01-01' => '元日',
					'2031-01-13' => '成人の日',
					'2031-02-11' => '建国記念の日',
					'2031-03-21' => '春分の日',
					'2031-04-29' => '昭和の日',
					'2031-05-03' => '憲法記念日',
					'2031-05-04' => 'みどりの日',
					'2031-05-05' => 'こどもの日',
					'2031-05-06' => '振替休日',
					'2031-07-21' => '海の日',
					'2031-08-11' => '山の日',
					'2031-09-15' => '敬老の日',
					'2031-09-23' => '秋分の日',
					'2031-10-13' => '体育の日',
					'2031-11-03' => '文化の日',
					'2031-11-23' => '勤労感謝の日',
					'2031-11-24' => '振替休日',
					'2031-12-23' => '天皇誕生日',
					'2032-01-01' => '元日',
					'2032-01-12' => '成人の日',
					'2032-02-11' => '建国記念の日',
					'2032-03-20' => '春分の日',
					'2032-04-29' => '昭和の日',
					'2032-05-03' => '憲法記念日',
					'2032-05-04' => 'みどりの日',
					'2032-05-05' => 'こどもの日',
					'2032-07-19' => '海の日',
					'2032-08-11' => '山の日',
					'2032-09-20' => '敬老の日',
					'2032-09-21' => '国民の休日',
					'2032-09-22' => '秋分の日',
					'2032-10-11' => '体育の日',
					'2032-11-03' => '文化の日',
					'2032-11-23' => '勤労感謝の日',
					'2032-12-23' => '天皇誕生日'
				];

		$year =  date("Y", strtotime($approvalDate));
		$month = date("m", strtotime($approvalDate));
		$toDate = (int)date("j", strtotime($approvalDate));

		$dateStr;
		$day;
		$count = 0;
		$paymentDate = "10日";

		for($i=1;$i<=$toDate;$i++)
		{
			$dateStr = $year . "-" . $month . "-" . ($i < 10 ? "0" . $i : $i);
			$day = date("w", strtotime($dateStr));

			// 休日・祝日であれば次のループ
			if (isset($holidays[$dateStr]) || $day === "0" || $day === "6") 
			{
				continue;
			}else{
				$count++;
			}
		}

		if($count > 3 && $toDate < 16){		
			$paymentDate = "25日";
		}

		return $paymentDate;
}