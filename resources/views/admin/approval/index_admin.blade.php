@extends('layout')
@section('content')

<style>
    thead
    {
        background-color: #333333;
        color: #FFFFFF;
    }

    th td
    {
        text-align: center;
    }

	.dept
    {
        background-color: #666666;
        color: #FFFFFF;
		font-weight:bold;
		padding-left:16px;
		text-align:left;
    }
	
	.dept_mgr
    {
		border:none;
        background-color: #666666;
        color: #FFFFFF;
		font-weight:bold;
		padding-right:16px;
		text-align:right;
    }
	

    .square_button{
        border-radius: 0 !important;
        color:#000;
        background-color:#FFF;
        border-color:#8C8C8C;
    }

    .square_button:hover
    {
        color: #FC0;
        background-color: #000;
        border-color: #888;
    }

    .btn-default.active, .btn-default.active:hover, .btn-default.active:focus
    {
        color: #333;
        background-color: #e6e6e6;
        border-color: #adadad;
    }

    .ui-datepicker-trigger {
        width: 32px;
        padding-left: 3px;
    }

    td
    {
        word-break: break-all;
    }
	
	.mgr
	{
		color:#ff0077;
		font-weight:bold;
	}
	
</style>

<div class="page-header" style="margin-top:0px;padding-bottom:0px;">
    <div>
        <a href="/admin/attendance/daily" class="btn btn-default square_button" role="button">日別勤怠</a><!--
        --><a href="/admin/attendance/employees" class="btn btn-default square_button" role="button">社員別勤怠</a><!--
        --><a href="/admin/attendance/approval" class="btn btn-default square_button active" role="button">月次承認</a>
    </div>
</div>


<div class="form-group">

    <!--select id="selectYear" class="form-control" style="width: 25%; display: inline;">
        @for ($i = 2015; $i <= 2015 + (date('Y') - 2015); $i++)
            @if ($selectYear == $i)
                <option value="{{ $i }}" selected="selected">{{ $i }}年</option>
            @else
                <option value="{{ $i }}">{{ $i }}年</option>
            @endif
        @endfor
    </select>

    <select id="selectMonth" class="form-control" style="width: 20%; display: inline;">
        @for ($i = 1; $i <= 12; $i++)
            @if ($selectMonth == $i)
                <option value="{{ sprintf('%02d', $i) }}" selected="selected">{{ $i }}月</option>
            @else
                <option value="{{ sprintf('%02d', $i) }}">{{ $i }}月</option>
            @endif
        @endfor
    </select-->
	
	
 	<select id="selectYearMonth" class="form-control" style="width: 25%; display: inline;">		
		@for ($i=0;$i<count($yearMonth);$i++)
			
			<option value="{{  $yearMonth[$i]['year'] .  $yearMonth[$i]['month'] }}" {{ $selectDate === ($yearMonth[$i]['year'] .  $yearMonth[$i]['month']) ? "selected" : ""}}>{{  $yearMonth[$i]['year'] . '年' .  (int)$yearMonth[$i]['month'] . '月' }}</option>
			
		@endfor
		
    </select>


    <select id="select_type" class="form-control" style="display:inline; width:30%; float: right;margin-bottom: 10px;">
        @foreach (array(-1 => 'すべて', 0 => '未承認', 1 => '承認済', 2 => '承認取消') as $key => $val)

        @if ($filter == $key)
            <option value="{{ $key }}" selected="selected">{{ $val }}</option>
        @else
            <option value="{{ $key }}">{{ $val }}</option>
        @endif
        @endforeach
    </select>
    <label for="select_type" style="float: right;"><i class="glyphicon glyphicon-filter"  style="font-size: 20px;top:8px;padding-right:7px;"></i></label>
</div>

<div>
	<table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th class="text-center col-ms-1">社員番号</th>
				 <th class="text-center col-ms-1">役職</th>
                <th class="text-center col-ms-1">氏名</th>
                <th class="text-center col-ms-1">変更者</th>
                <th class="text-center col-ms-1">変更日</th>
				<th class="text-center col-ms-3">備考</th>
                <th class="text-center col-ms-1">状態</th>
            </tr>
        </thead>
        <tbody>
		@foreach ($departments as $info)
		{{-- */
			$employees = array();
			$mgrInfo = null;
			$managerInfo = null;
			$gManagerInfo = null;
			
			$managerName = "";
			$relations = new app\DepartmentRelations;
			$departmentRelate = $relations->findByDepartmentId($info->id);
			
			foreach($departmentRelate as $relateInfo)
			{
				if($info->id !== 21)
				{
					if($relateInfo->position_id === 110 || $relateInfo->position_id === 120) $empInfo = $availableUserList[$relateInfo->emp_no];
				
					if($relateInfo->position_id === 110)
					{
							$managerName = getPositionName($relateInfo->position_id)  . "		:	" .  $empInfo->last_name . " " . $empInfo->first_name;
					}else if($relateInfo->position_id === 120 && $info->id !== 2){
							$managerName = getPositionName($relateInfo->position_id)  . "		:	" .  $empInfo->last_name . " " . $empInfo->first_name;
					}
				}			
			}
			
			foreach($listData as $emp)
			{
				if($emp->department_id === $info->id)
				{
					if($emp->department_id === 21)
					{
						$employees[] = $emp;
					}else{
						if($emp->position_id === 110)
						{
							$managerInfo = $emp;
						}else if($emp->position_id === 120){
							$gManagerInfo = $emp;
							$employees[] = $emp;
						}else{
							$employees[] = $emp;
						}
					}
				}
				//
				$gManagerInfo = null;
			}
			
			if(isset($managerInfo)) array_unshift($employees, $managerInfo);
			if(isset($gManagerInfo)) array_unshift($employees, $gManagerInfo);
		/* --}}
		@if(count($employees) > 0)
		<tr class="text-center">
			<td  class="dept" style="padding-left:16px;border-width:0;" colspan="5">{{ $info->name }}</td>
			<td  class="dept_mgr"  style="padding-right:16px;border-width:0;" colspan="2">{{ $managerName }}</td>
		</tr>
		@endif
		
		@foreach($employees as $employeeInfo)
		
			@if($employeeInfo->department_id !== 21 && ($employeeInfo->position_id == 110 || is_null($managerInfo) && $employeeInfo->position_id == 120))
				{{-- */  $isManager = TRUE; /* --}}
			<tr class="text-center">
			@else
				{{-- */  $isManager = FALSE; /* --}}
			<tr class="text-center">
			@endif
			
                <td>{{ $employeeInfo->emp_no }}</td>
				<td class="{{ $isManager === TRUE ? 'mgr' : '' }}">{{ getPositionName($employeeInfo->position_id) }}</td>
                
				<td>
					<a class="{{ $isManager === TRUE ? 'mgr' : '' }}" href="/admin/employee/edit/{{$employeeInfo->emp_no}}">{{ $employeeInfo->name }}</a>
				</td>
				
				@if(isset($employeeInfo->upd_mail))
					<td>
						<a  href="mailto:{{$employeeInfo->upd_mail}}">{{ $employeeInfo->upd_name }}</a>
					</td>
				@else
					<td>{{ $employeeInfo->upd_name }}</td>
				@endif
                
                <td>{{ $employeeInfo->upd_date }}</td>
				<td>{{ $employeeInfo->note }}</td>

            @if ($employeeInfo->status == 2)
                <td style="font-weight: bold; background-color: crimson;">
                @else
                <td style="font-weight: bold;">
                @endif
                    <a href="/admin/attendance/employees/{{ $employeeInfo->emp_no }}/{{ $selectDate }}" style="color:{{ $employeeInfo->status_color }}">{{ $employeeInfo->status_name }}</a>
                </td>
            </tr>
			@endforeach
        @endforeach

		</tbody>
	</table>
</div>


<script>


/**
 * 年月リスト変更イベント
 */
$('#selectYearMonth').change(function(e)
{
    var date   = e.target.value;
    var filter = $('#select_type')[0].value
    window.location.href = "/admin/attendance/approval/" + date + "/" + filter
});



/**
 * 年リスト変更イベント
 */
$('#selectYear').change(function(e) {
    var date   = e.target.value + $('#selectMonth')[0].value
    var filter = $('#select_type')[0].value
    window.location.href = "/admin/attendance/approval/" + date + "/" + filter
});


/**
 * 月リスト変更イベント
 */
$('#selectMonth').change(function(e) {
    var date   = $('#selectYear')[0].value + e.target.value
    var filter = $('#select_type')[0].value
    window.location.href = "/admin/attendance/approval/" + date + "/" + filter
});


/**
 * フィルタ変更イベント
 */
$('#select_type').change(function(e) {
   //var date   = $('#selectYear')[0].value + $('#selectMonth')[0].value;
   
   var date = $('#selectYearMonth')[0].value
   var filter = e.target.value
   window.location.href = "/admin/attendance/approval/" + date + "/" + filter
})

</script>

@endsection