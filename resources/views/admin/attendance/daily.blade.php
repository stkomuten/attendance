@extends('layout')
@section('content')

<style>
	thead
	{
		background-color: #333333;
		color: #FFFFFF;
	}

	th td
	{
		text-align: center;
	}

	.square_button{
		border-radius: 0 !important;
		color:#000;
		background-color:#FFF;
		border-color:#8C8C8C;
	}

	.square_button:hover
	{
		color: #FC0;
		background-color: #000;
		border-color: #888;
	}

	.btn-default.active, .btn-default.active:hover, .btn-default.active:focus
	{
		color: #333;
		background-color: #e6e6e6;
		border-color: #adadad;
	}

	.ui-datepicker-trigger {
		width: 32px;
		padding-left: 3px;
	}

	td
	{
		word-break: break-all;
	}
</style>

<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="page-header" style="margin-top:0px;padding-bottom:0px;">
	<!--h1><small>勤務記録</small></h1-->

	<div>
	    <a href="/admin/attendance/daily" class="btn btn-default active square_button" role="button">日別勤怠</a><!--
		--><a href="/admin/attendance/employees" class="btn btn-default square_button" role="button">社員別勤怠</a><!--
		--><a href="/admin/attendance/approval" class="btn btn-default square_button" role="button">月次承認</a>
	</div>
</div>



<div class="form-group">
	<input type="text" class="form-control" id="daily-datepicker" value="{{ $selectDate }}" style="width: 40%; display:inline;" readonly>

    <select id="select_type" class="form-control" style="display:inline; width:30%; float: right;">
    	@foreach (array(1 => 'すべて', 2 => '記録済', 3 => '未記録') as $key => $val)

		@if ($filter == $key)
			<option value="{{ $key }}" selected="selected">{{ $val }}</option>
		@else
			<option value="{{ $key }}">{{ $val }}</option>
		@endif
        @endforeach
    </select>
	<label for="select_type" style="float: right;"><i class="glyphicon glyphicon-filter"  style="font-size: 20px;top:8px;padding-right:7px;"></i></label>
</div>

<div>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th class="text-center" rowspan="2"></th>
				<th class="text-center" rowspan="2">社員番号</th>
				<th class="text-center" rowspan="2">氏名</th>
				<th class="text-center" colspan="6">勤務記録</th>
				<th class="text-center" rowspan="2">有給休暇</th>
				<th class="text-center" rowspan="2">備考</th>
				<th class="text-center" rowspan="2">操作</th>
			</tr>
			<tr>
				<th class="text-center">出勤時間</th>
				<th class="text-center">退勤時間</th>
				<th class="text-center">始業時間</th>
				<th class="text-center">終業時間</th>
				<th class="text-center">実働時間</th>
				<th class="text-center">残業時間</th>
			</tr>
		</thead>
		<tbody>
		{{-- */
				$count = 1 + ($listPager->currentPage() - 1) * $listPager->perPage();
				$dayIndex = getDayIndex(substr($formDate, 0, 4), (int)substr($formDate, 4, 2), substr($formDate, 6, 2));
		/* --}}
		@foreach ($listPager as $data)
			<tr class="text-center">
				<td class="text-center" style="width:3%">{{ $count }}</td>
				<td>{{ $data->emp_no }}</td>
				<td><a href="/admin/attendance/employees/{{$data->emp_no}}" style="font-weight:bold;" title="社員別勤怠を開く">{{ $data->last_name }} {{ $data->first_name }}</a></td>
				<!--td>{!! getTimeRecorderStartDate($data->start_date) !!}</td-->
				<!--td>{!! getTimeRecorderEndDate($data->end_date) !!}</td-->




				{{-- */
						
								$start_bgColor = "";
								$end_bgColor = "";
								
								$division_bgColor = "";
								
								$start_style	= $data->start_date !== $data->init_start_date ? "style=color:#ff8080" : '';
								$end_style	= $data->end_date !== $data->init_end_date ? "style=color:#ff8080" : '';
								
								$startText	= " -- ";
								$endText	= " -- ";
								
								$divisionText = " -- ";
								
								//	過去日のみ
								if(strtotime($selectDate) < strtotime("today") && $dayIndex !== "0" && $dayIndex !== "6" &&  $data->emp_type === 1)
								{
								
									$isEnabledHoliday = false;
									
									if(isset($data->paid_holiday))
									{
										$paid_holidays = unserialize($data->paid_holiday);
										
										$p1 = $paid_holidays[0];
										
										if($p1 === "1" || $p1 === "10" ||  $p1 === "11"  ||  $p1 === "12"  ||  $p1 === "14" ||  $p1 === "15" ||  $p1 === "16" ||  $p1 === "17" ||  $p1 === "18" ||  $p1 === "19")
										{
											$isEnabledHoliday = true;
										}
									}
									
									if(isset($data->start_date) === false && isset($data->end_date) === false && $isEnabledHoliday === false)
									{
										$start_bgColor = $end_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
									}else if(isset($data->start_date) === false && $isEnabledHoliday === false) {
										$start_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
									}else if(isset($data->end_date) === false && $isEnabledHoliday === false) {
										$end_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
									}
									
									
									$startText = $start_bgColor === "" ?  " -- " : "未";
									$endText = $end_bgColor === "" ?  " -- " : "未";
									
									$division_bgColor = (isset($data->division_id) === false || $data->division_id === 0) && $isEnabledHoliday === false ? 'style=color:#FFF;font-weight:bold bgcolor=#FF0000' : '';
									$divisionText = $division_bgColor === "" ?  " -- " : "未";
									
									if($start_bgColor !== "" || $end_bgColor !== "" || $division_bgColor !== "")
									{
										$isUnRecorded = TRUE;
									}
								}
								
								
							
				/* --}}



				<td {{$start_bgColor}} {{$start_style}}>{!! isset($data->start_date) ? getExcelStartDate($data->start_date) : $startText  !!}</td>
				<td {{$end_bgColor}} {{$end_style}}>{!! isset($data->end_date) ? getExcelEndDate($data->start_date, $data->end_date) : $endText !!}	</td>
				<td class="col-md-1" {{ $division_bgColor }}>
				
					@if(isset($data->division_id) && $data->division_id !== 0)
						{{-- */$timeSheetAttr = findDivisionAttrByID($data->division_id, $timeSheet) /* --}}
						{{ convertDisplayTime($timeSheetAttr['opening_time'])}}
					@else
						{{ $divisionText }}
					@endif
				</td>


				<td class="col-md-1" {{ $division_bgColor }}>
					@if(isset($data->division_id) && $data->division_id !== 0)
						{{ convertDisplayTime($timeSheetAttr['closing_time'])}}
					@else
						{{ $divisionText }}
					@endif
				</td>
				
				{{-- */ $originWorkDate = isset($data->division_id) && $data->division_id !== 0 ? getExcelOriginWorkDate($data->start_date, $data->end_date, $timeSheetAttr) :  "--" /* --}}
				
				{{-- */
					$paidHolidayID = null;
					$phCount = 0;
					if(isset($data->paid_holiday))
					{
						$paidHolidayID = unserialize($data->paid_holiday);
						$phCount = count($paidHolidayID);
					}
				/* --}}
				<td>{!! $originWorkDate !!}</td>
				<!-- 残業 -->
				<td>{!! $originWorkDate !== "--" && isset($data->division_id) && $data->division_id !== 0 ? getOver8HourWork($originWorkDate, $paidHolidayID, $paidHolidays, $data->start_date, $data->end_date, $timeSheetAttr) :  "--" !!}</td>
				
				<!-- 有休 -->
				 	
				@if(isset($paidHolidayID) && $phCount > 0)
					<td class="col-md-1" style="font-weight:bold;">
					
					@for ($i = 0; $i<$phCount ; $i++)
							
								<font style="color:{{ getPaidHolidayColor((int)$paidHolidayID[$i]) }};">{{$paidHolidays[(int)$paidHolidayID[$i] - 1]->name}}</font>
								@if($i < $phCount - 1 ) <br>
								@endif
					@endfor
					</td>	
				@else
					<td class="col-md-1">--</td>
				@endif
		<td class="col-md-3">{{ is_null($data->note) ? "--" : $data->note }}</td>
			<!--td>{{$data->init_start_date}}</td-->
		<td>
				@if(isset($data->approval) && $data->approval->status === 1)
					
					@if (\Auth::user()->haveSystemAdministratorRole() || \Auth::user()->haveManagimentTeamRole())
								
						@if($data->isNew)
							<a href="/admin/attendance/create/{{ $data->emp_no }}/{{ $formDate }}" class="btn btn-warning btn-xs">新規</a>
						@else
							<button data-number="{{ $data->id }}" class="btn btn-danger btn-xs deleteButton">削除</button>
							<a href="/admin/attendance/edit/{{ $data->id }}" class="btn btn-info btn-xs">編集</a>
						@endif
						
					@else
						&nbsp;--&nbsp;
					@endif
					
				@else
					
					@if ($data->isNew)
						<a href="/admin/attendance/create/{{ $data->emp_no }}/{{ $formDate }}" class="btn btn-warning btn-xs">新規</a>
					@else
						<button data-number="{{ $data->id }}" class="btn btn-danger btn-xs deleteButton">削除</button>
						<a href="/admin/attendance/edit/{{ $data->id }}" class="btn btn-info btn-xs">編集</a>
					@endif
				
				@endif
		</td>
			</tr>
			{{-- */ $count++ /* --}}
		@endforeach
	    </tbody>
	</table>

</div>

 <div class="text-center">
{!! $listPager->render() !!}
</div>

<script src="/js/attendance/util.js"></script>
<script src="/js/attendance/daily.js"></script>


@endsection