@extends('layout')
@section('content')

<style>
	.table_bottom_space
	{
		margin-bottom:30px;
	}

	thead
	{
		background-color: #333333;
		color: #FFFFFF;
	}

	th td
	{
		text-align: center;
	}

	.square_button {
		border-radius: 0 !important;
		color:#666;
		background-color:#FFF;
		border-color:#8C8C8C;
	}

	.square_button:hover
	{
		color: #FC0;
		background-color: #000;
		border-color: #888;
	}

	.btn-default.active, .btn-default.active:hover, .btn-default.active:focus
	{
		color: #333;
		background-color: #e6e6e6;
		border-color: #adadad;
	}

	.popover-title
	{
		color: #000;
	}

	.popover
	{
		min-width: 215px;
	}

	td
	{
		word-break: break-all;
	}

	/* フッターの固定 */
	div#footer-fixed
	{
		position: fixed;            /* フッターの固定 */
		bottom: 0px;                /* 位置(下0px) */
		left: 0px;                  /* 位置(左0px) */
		width: 100%;                /* 横幅100%　*/
		opacity: 0.9;
	}

	div#footer-bk
	{
		background-color:#333333;     /* 背景色(黒) */
		height:40px;                  /* 縦の高さ140px */
		width:100%;                   /* 横の幅を100% */
	}

	div#footer
	{
		text-align: right;
		padding: 10px;
		color: #FFFFFF;
	}


	img.neko
	{
		width:200px;
		height:200px;
	}

</style>


<meta name="csrf-token" content="{{ csrf_token() }}" />


	<div class="page-header" style="margin-top:0px;padding-bottom:0px;height:36px;">
	    <!--h1><small>勤務記録</small></h1-->

		<div>
			@if (!\Auth::user()->haveNormalRole())

				<a href="/admin/attendance/daily" class="btn btn-default square_button" style="float:left;" role="button">日別勤怠</a>
				<!----><a href="/admin/attendance/employees" class="btn btn-default square_button active" style="float:left;" role="button">社員別勤怠</a>
				<!----><a href="/admin/attendance/approval" class="btn btn-default square_button" style="float:left;" role="button">月次承認</a>

			@endif
			<button class="btn btn-default square_button" style="float:right;" onclick="clickPrintLink()"><i class="glyphicon glyphicon-print"></i> 印刷画面</button>

		</div>


	</div>

<div class="form-group">

    <select id="selectYear" class="form-control" style="width: 13%; display: inline;">
        @for ($i = 2015; $i <= date('Y') + 1; $i++)
            @if ($selectYear == $i)
                <option value="{{ $i }}" selected="selected">{{ $i }}年</option>
            @else
                <option value="{{ $i }}">{{ $i }}年</option>
            @endif
        @endfor
    </select>

    <select id="selectMonth" class="form-control" style="width: 11%; display: inline;">
        @for ($i = 1; $i <= 12; $i++)
            @if ($selectMonth == $i)
                <option value="{{ sprintf('%02d', $i) }}" selected="selected">{{ $i }}月</option>
            @else
                <option value="{{ sprintf('%02d', $i) }}">{{ $i }}月</option>
            @endif
        @endfor
    </select>
@if (count($userList) > 1)
<button class="btn btn-default square_button" style="float:right;" onclick="clickNextEmployee()"><i class="glyphicon glyphicon-triangle-right"></i></button>
@endif
    <select id="select_user" class="form-control" style="width: 28%; display: inline; float:right;">
        @foreach ($userList as $id => $name)
            @if ($userData->emp_no == $id)
                <option value="{{ $id }}" selected="selected">{{ $id }} : {{ $name }}</option>
            @else
                <option value="{{ $id }}">{{ $id }} : {{ $name }}</option>
            @endif
        @endforeach
    </select>
@if (count($userList) > 1)
<button class="btn btn-default square_button" style="float:right;" onclick="clickPreviousEmployee()"><i class="glyphicon glyphicon-triangle-left"></i></button>
@endif
</div>

<div>

	<table class="table table-striped table-bordered table_bottom_space">
		<thead>
			<tr>
				<th class="text-center" rowspan="2">日付</th>
				<th class="text-center" rowspan="2">曜日</th>
				<!--th class="text-center" colspan="2">記録時間</th-->
				<th class="text-center" colspan="6">
				    勤務記録
				    <!--span style="float: right; margin-left: -55px;">
				        <a class="btn btn-default btn-xs" href="#staticModal" data-toggle="modal">csv表示</a>
				    </span-->
				</th>
				{{-- */
					use App\PaidHolidayBalance;
					$hbInfo = PaidHolidayBalance::getEmployeeByDate($userData->emp_no, $selectYear, $selectMonth);

					$dayBalance = $dayHoursBalance = $hourBalance = "--";

					$dayBalance = $hbInfo["days"];

					if(substr($dayBalance, -1) == "0") $dayBalance = substr($dayBalance, 0, strlen($dayBalance) - 1);

					$dayHoursBalance = substr($hbInfo["day_hours"], 0, 5);

					if(substr($dayHoursBalance, 0, 1) == "0") $dayHoursBalance = substr($dayHoursBalance, 1, 5);

					$hourBalance = substr($hbInfo["hours"], 0, 5);

				/* --}}
				<th class="text-center"><button style="width:20px;height:20px;padding:0px;background-color:rgba(0,0,0,0);border:none;float:left;padding-top: 3px;" name="paidholiday-help-button"  data-toggle="popover-paidholiday-help" tabindex="999"><i name="paidholiday-help-icon" class="glyphicon glyphicon-question-sign" style="pointer-events : none;"></i></button>有休残: <font id="dayBalance" size="3"></font>日<font id="dayHoursBalance" size="3"></font><br>時間有休残: <font id="hourBalance" size="3"></font></th>

				<th class="text-center" rowspan="2">備考</th>


				<th class="text-center" rowspan="2">操作</th>

				</tr>
				<tr>
				<!--th class="text-center">出勤時間</th-->
				<!--th class="text-center">退勤時間</th-->
				<th class="text-center">出勤時間</th>
				<th class="text-center">退勤時間</th>
				<th class="text-center">始業時間</th>
				<th class="text-center">終業時間</th>
				<th class="text-center">実働時間</th>
				<th class="text-center">残業時間</th>
				<th class="text-center" style="width:15%">有給休暇</th>
			</tr>
		</thead>
        <tbody>
		{{-- */

				$tabCount = 0;
				$count = 0;
				$unRecordedCount = 0;
				$underOfficialWorkingHourCount = 0;
				$OriginWorkDateTotalArray = array();
				$OverWorkDateTotalArray = array();
				$paidHolidayTotal = 0;
				$originWorkDate = null;

				$countUsedPaidHoliday = 0;
				$countUsedTime = 0;

		/* --}}
        @foreach ($listData as $key => $data)
	<tr class="text-center" style="background-color: {{ $data['week_color'] }};color: {{ $data['text_color']}}">

		<td>{{ $data['day_str'] }}</td>
		<td>{{ $data['week'] }}</td>

		{{-- */
				$isUnRecorded = FALSE;
		/* --}}

		@if (isset($data['detail']))

		<!--td>{!! getTimeRecorderStartDate($data['detail']->start_date) !!}</td-->
		<!--td>{!! getTimeRecorderEndDate($data['detail']->end_date) !!}</td-->

		{{-- */

				$start_bgColor = "";
				$end_bgColor = "";

				$division_bgColor = "";

				$start_style = $data['detail']->start_date !== $data['detail']->init_start_date ? "style=color:#ff8080" : '';
				$end_style	= $data['detail']->end_date !== $data['detail']->init_end_date ? "style=color:#ff8080" : '';

				$startText	= " -- ";
				$endText	= " -- ";

				$divisionText = " -- ";

				//	過去日のみ
				//if(strtotime($data['detail']->reg_date) < strtotime("today"))
				//{
					$isEnabledHoliday = false;

					if(isset($data['detail']->paid_holiday))
					{
						$paid_holidays = unserialize($data['detail']->paid_holiday);
						$count = count($paid_holidays);

						$p1 = $paid_holidays[0];

						if($p1 === "1" || $p1 === "10" ||  $p1 === "11"  ||  $p1 === "12"  ||  $p1 === "14" ||  $p1 === "15" ||  $p1 === "16" ||  $p1 === "17" ||  $p1 === "18" ||  $p1 === "19" ||  $p1 === "20"  ||  $p1 === "21"  ||  $p1 === "22")
						{
							$isEnabledHoliday = true;
						}

						//	使用日数
						if($p1 === "1"){
							$countUsedPaidHoliday += 1;
						}else if($p1 === "2" ||  $p1 === "3"){
							$countUsedPaidHoliday += 0.5;
						}

						//	使用時間
						//	使用日数

						if($p1 === "4"){
								$countUsedTime += 1;
						}else if($p1 === "5"){
								$countUsedTime += 2;
						}else if($p1 === "6"){
								$countUsedTime += 3;
						}else if($p1 === "7"){
								$countUsedTime += 5;
						}else if($p1 === "8"){
								$countUsedTime += 6;
						}else if($p1 === "9"){
								$countUsedTime += 7;
						}
					}


				if(strtotime($data['detail']->reg_date) < strtotime("today"))
				{

					if(isset($data['detail']->start_date) === false && isset($data['detail']->end_date) === false && $isEnabledHoliday === false && $data['week_color'] === "")
					{
						$start_bgColor = $end_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
					}else if(isset($data['detail']->start_date) === false && $isEnabledHoliday === false && $data['week_color'] === "") {
						$start_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
					}else if(isset($data['detail']->end_date) === false && $isEnabledHoliday === false && $data['week_color'] === "") {
						$end_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
					}


					$startText = $start_bgColor === "" ?  " -- " : "未";
					$endText = $end_bgColor === "" ?  " -- " : "未";

					$division_bgColor = (isset($data['detail']->division_id) === false || $data['detail']->division_id === 0) && $isEnabledHoliday === false ? 'style=color:#FFF;font-weight:bold bgcolor=#FF0000' : '';
					$divisionText = $division_bgColor === "" ?  " -- " : "未";

					if($start_bgColor !== "" || $end_bgColor !== "" || $division_bgColor !== "")
					{
						$isUnRecorded = TRUE;
					}
				}
				if(count($paid_holidays) > 1)
				{
				    $p1 = $paid_holidays[1];

				    if($p1 === "1" || $p1 === "10" ||  $p1 === "11"  ||  $p1 === "12"  ||  $p1 === "14" ||  $p1 === "15" ||  $p1 === "16" ||  $p1 === "17" ||  $p1 === "18" ||  $p1 === "19"  ||  $p1 === "20"  ||  $p1 === "21"  ||  $p1 === "22")
				    {
				        $isEnabledHoliday = true;
				    }

				    //    使用日数
				    if($p1 === "1"){
				        $countUsedPaidHoliday += 1;
				    }else if($p1 === "2" ||  $p1 === "3"){
				        $countUsedPaidHoliday += 0.5;
				    }

				    //    使用時間
				    //    使用日数

				    if($p1 === "4"){
				            $countUsedTime += 1;
				    }else if($p1 === "5"){
				            $countUsedTime += 2;
				    }else if($p1 === "6"){
				            $countUsedTime += 3;
				    }else if($p1 === "7"){
				            $countUsedTime += 5;
				    }else if($p1 === "8"){
				            $countUsedTime += 6;
				    }else if($p1 === "9"){
				            $countUsedTime += 7;
				    }
				}

		/* --}}

		<td {{$start_bgColor}} {{$start_style}}>{{ isset($data['detail']->start_date) ? getExcelStartDate($data['detail']->start_date) : $startText }}</td>
		<td {{$end_bgColor}} {{$end_style}}>{{ isset($data['detail']->end_date) ? getExcelEndDate($data['detail']->start_date, $data['detail']->end_date) : $endText }}</td>

		<td class="col-md-1" {{ $division_bgColor }}>
			@if(isset($data['detail']->division_id) && $data['detail']->division_id !== 0)
				{{-- */ $timeSheetAttr = findDivisionAttrByID($data['detail']->division_id, $timeSheet);/* --}}
				{{ convertDisplayTime($timeSheetAttr['opening_time'])}}
			@else
				{{$divisionText}}
			@endif
		</td>


		<td class="col-md-1" {{ $division_bgColor }}>
			@if(isset($data['detail']->division_id) && $data['detail']->division_id !== 0)
				{{ convertDisplayTime($timeSheetAttr['closing_time'])}}
			@else
				{{$divisionText}}
			@endif
		</td>
		{{-- */

			//-- 有休 --
			$paid_holidays = null;
			$count = 0;
			if(isset($data['detail']->paid_holiday))
			{
				$paid_holidays = unserialize($data['detail']->paid_holiday);
				$count = count($paid_holidays);
			}

			$originWorkDate = null;
			$originWorkDateColor = '';

			if(isset($data['detail']->start_date) && isset($data['detail']->end_date) && isset($data['detail']->division_id) && $data['detail']->division_id !== 0)
			{
		 		$originWorkDate = getExcelOriginWorkDate($data['detail']->start_date, $data['detail']->end_date, $timeSheetAttr);

				//=====	実働時間のバリデーション ======

				$paidHoliday1Hour = null;
				$paidHoliday2Hour = null;

				if(isset($paid_holidays) && $count > 0)
				{
					$paidHoliday1Hour = $paidHolidays[(int)$paid_holidays[0]-1]->hour;
					if($count > 1) $paidHoliday2Hour = $paidHolidays[(int)$paid_holidays[1]-1]->hour;
				}

				$isOverOfficialWorkingHour = validateOverOfficialWorkingHour($originWorkDate, $timeSheetAttr['official_working_hour'], $paidHoliday1Hour, $paidHoliday2Hour);

				$originWorkDateColor = !$isOverOfficialWorkingHour ? 'style=color:#000;font-weight:bold bgcolor=#FFCC00' : "";

				if($isOverOfficialWorkingHour === FALSE) $underOfficialWorkingHourCount++;
				//
				//==============================

				$OriginWorkDateTotalArray[] = $originWorkDate;
			}

			$overWorkDate = null;

			if(isset($originWorkDate))
			{
				$overWorkDate = getOver8HourWork($originWorkDate, $paid_holidays, $paidHolidays, $data['detail']->start_date, $data['detail']->end_date, $timeSheetAttr);
				$OverWorkDateTotalArray[] = $overWorkDate;
			}

			//$twoTimesIcon = $data["detail"]->two_times === 1 ? '<img src="/images/two_times_icon.png" width="18" height="18"/>' : '' ;

		/* --}}


		<td {{$originWorkDateColor}}>{{ $originWorkDate == null ? "--" : $originWorkDate }}</td>

		<!-- 残業 -->
		<td>{!! isset($overWorkDate) ? $overWorkDate :  "--" !!}</td>

		<!-- 有休 -->
		@if(isset($paid_holidays) && $count > 0)
			<td class="col-md-1" style="font-weight:bold;">

				<input type="hidden" name="paidHolidayData" value="{{ json_encode($paid_holidays)}}"/>

			@for ($i = 0; $i<$count ; $i++)

				<font style="color:{{ getPaidHolidayColor((int)$paid_holidays[$i]) }};">
				{{-- */
					$countJ = count($paidHolidays);
					$paidHolidayName = "";
					for ($j = 0; $j<$countJ ; $j++)
					{
						if((int)$paid_holidays[$i] === (int)$paidHolidays[$j]->id)
						{
							$paidHolidayName = $paidHolidays[$j]->name;
							break;
						}
					}
				/* --}}
				{{$paidHolidayName}}</font>
				@if($i < $count - 1 ) <br>
				@endif

			@endfor
			</td>
		@else
			<td class="col-md-1">--</td>
		@endif




		<td class="col-md-3">{{ isset($data['detail']->note) ? $data['detail']->note : ' -- ' }}</td>

			@if ($approvalFooterStatus == 1)

				@if (\Auth::user()->haveSystemAdministratorRole() || \Auth::user()->haveManagimentTeamRole())

					<td>
						<button data-number="{{ $data['detail']->id }}" class="btn btn-danger btn-xs deleteButton">削除</button>
						<a href="/admin/attendance/edit/{{ $data['detail']->id }}" class="btn btn-info btn-xs">編集</a>
					</td>
				@else
					<td> -- </td>
				@endif
			@else
				<!-- 未承認 -->

				@if (!\Auth::user()->haveNormalRole())
					<td>
						<button data-number="{{ $data['detail']->id }}" class="btn btn-danger btn-xs deleteButton">削除</button>
						<a href="/admin/attendance/edit/{{ $data['detail']->id }}" class="btn btn-info btn-xs">編集</a>
					</td>
				@else
					<td>
						<a name="popover_anchor" data-toggle="popover" tabindex="{{ $tabCount }}" data-id="{{ $data['detail']->id }}" data-date="{{  $data['date'] }}" data-date-label="{{  $data['day_str'] }}" data-day="{{ $data['week'] }}" data-paid-holiday="{{ isset($data['detail']->paid_holiday) ? json_encode($paid_holidays) : '' }}" data-text="{{ $data['detail']->note }}" }" class="btn btn-info btn-xs editNoteButton">編集</button>
						<!--a href="javascript:void(0)" onclick="clickEditNote('{{ $data['detail']->id }}')" class="btn btn-info btn-xs">編集</a-->
					</td>
				@endif

			@endif
                @else
				{{-- */
							$bgColor = $userData->emp_type === 1 && strtotime($data['date']) < strtotime("today") && $data['week_color'] === "" ? 'style=color:#FFF;font-weight:bold bgcolor=#FF0000' : "";
							$attendText = $bgColor === "" ?  " -- " : "未";

							$division_bgColor = $userData->emp_type === 1 && strtotime($data['date']) < strtotime("today") && $data['week_color'] === "" ? 'style=color:#FFF;font-weight:bold bgcolor=#FF0000' : '';
							$divisionText = $division_bgColor === "" ?  " -- " : "未";

							if($bgColor !== "" || $division_bgColor !== "")
							{
								$isUnRecorded = TRUE;
							}

				/* --}}
		<td {{$bgColor}}>{{ $attendText }}</td>
		<td {{$bgColor}}>{{ $attendText }}</td>
		<td {{$division_bgColor}}>{{ $divisionText }}</td>
		<td {{$division_bgColor}}>{{ $divisionText }}</td>
		<td> -- </td>
		<td> -- </td>
		<td> -- </td>
		<td> -- </td>
			@if ($approvalFooterStatus == 1)

				@if (\Auth::user()->haveSystemAdministratorRole() || \Auth::user()->haveManagimentTeamRole())
						<td>
							<a href="/admin/attendance/create/{{ $userData->emp_no }}/{{ $selectYear }}{{ $selectMonth }}{{ $data['day'] }}" class="btn btn-warning btn-xs">新規</a>
						</td>
				@else

					<td> -- </td>

				@endif




			@else

				@if (!\Auth::user()->haveNormalRole())
					<td>
						<a href="/admin/attendance/create/{{ $userData->emp_no }}/{{ $selectYear }}{{ $selectMonth }}{{ $data['day'] }}" class="btn btn-warning btn-xs">新規</a>
					</td>
				@else
					<td>
						<a name="popover_anchor" data-toggle="popover" tabindex="{{ $tabCount }}" data-date="{{  $data['date'] }}"  data-date-label="{{  $data['day_str'] }}" data-day="{{ $data['week'] }}" data-paid-holiday="0" data-text="" class="btn btn-info btn-xs editNoteButton">編集</button>
					</td>
				@endif

			@endif

                @endif
            </tr>
		{{-- */
				$tabCount++;
				$count++;
				if($isUnRecorded === TRUE) $unRecordedCount++;

		/* --}}
        @endforeach


		{{-- */

			if($dayBalance < 0) $dayBalance = 0;
			//	有休残時間をタイムスタンプ化
			$dayHoursBalanceTimeStamp = strtotime("1970-01-01 " .  $dayHoursBalance);
			//	有休残時間をタイムスタンプ化
			$countUsedTimeStamp  = strtotime("1970-01-01 " .  $countUsedTime . ":00");
			//	時間有休残をタイムスタンプ化
			$hourBalanceStamp  = strtotime("1970-01-01 " .  $hourBalance);

			$dayHours = (int)mb_substr($dayHoursBalance, 0, 2);

			// 有休残時間が時間休使用数より小さい
			//if($dayHoursBalanceTimeStamp < $countUsedTimeStamp)

			if($dayHours < $countUsedTime)
			{
				//$dayHoursBalance = date("g:i", strtotime("1970-01-01 8:00") - ($countUsedTimeStamp - $dayHoursBalanceTimeStamp));

				if($countUsedTime > 8)
				{
					$HourRemainder = $countUsedTime % 8;

					$HourByDate = floor($countUsedTime / 8);
				}else{
					$HourRemainder = $countUsedTime;

					$HourByDate = 0;
				}

				$dayBalance -= $HourByDate;

				if($dayHours < $HourRemainder)
				{

					$dayHours = 8 - ($HourRemainder - $dayHours);
					$dayBalance -= 1;
				}else{
					$dayHours = $dayHours - $HourRemainder;
				}

				if($dayBalance < 0 && $dayHours < 0)
				{
					$dayBalance = 0;
					$dayHoursBalance = "0:00";
				}else{
					$dayHoursBalance = $dayHours . ":00";
				}
			}else{
				$dayHoursBalance = $dayHoursBalance - $countUsedTime;
				$dayHoursBalance .= ":00";
			}

			if($dayBalance < $countUsedPaidHoliday && $dayHours >= 4)
			{
				$dayHours -= 4;
				$dayHoursBalance = $dayHours . ":00";
				$dayBalance += 0.5;
			}


			$dayBalance = $dayBalance - $countUsedPaidHoliday;

			if($dayBalance < 0)
			{
				$dayBalance = 0;
				$dayHoursBalance = "0:00";
			}else{
				if(($hourBalance * 24 * 60) - ($countUsedTime * 24 * 60) < 0)
				{
					$dayBalance = $dayBalance - ($countUsedTime - $hourBalance)/8;
				}
			}

			$hourBalance = $hourBalance - $countUsedTime;

			if($dayBalance == 0 && $dayHoursBalance == 0)
			{
				$hourBalance = 0;
			}else if($hourBalance/8 > $dayBalance +  $dayHoursBalance / 8 ){

				$hourBalance = $dayBalance * 8 + $dayHours;
			}

			$hourBalance .= ":00";

		/* --}}


			<tr class="text-center">
				<td colspan="6" style="background-color:#BBBBBB;color:#FFFFFF;font-weight:bold"> 合計 </td>
				<td>{{ count($OriginWorkDateTotalArray) > 0 ? getExcelOriginWorkDateTotal($OriginWorkDateTotalArray) : "--" }}</td>
				<td>{{ count($OverWorkDateTotalArray) > 0 ? getExcelOriginWorkDateTotal($OverWorkDateTotalArray) : "--" }}</td>
				<td colspan="3"></td>
			</tr>
        </tbody>
    </table>
	<div style="text-align: center;">
		<img src="/images/neko/{{ mt_rand(1, 12)}}.png" class="neko"/>
	</div>


    @if (!Auth::user()->haveNormalRole() && $approvalFooterStatus !== false)
    <!-- フッター -->
    <div id="footer-fixed">
        <div id="footer-bk">
            <div id="footer">
				{{ isset($approvalNote) && strlen($approvalNote) > 0 ? "備考 : " .  $approvalNote : "" }}
				&nbsp;&nbsp;
                {{ $userData->emp_no }}
				&nbsp;:&nbsp;
                &nbsp;{{ $userData->last_name }}
                &nbsp;{{ $userData->first_name }}
                &nbsp;─&nbsp;
                {{ $selectYear }}年{{ (int)$selectMonth }}月

                @if ($approvalFooterStatus == 1)
                    @if (\Auth::user()->haveSystemAdministratorRole() || \Auth::user()->haveManagimentTeamRole())
                    &nbsp;<button class="btn btn-danger btn-xs approval-cancel-button" name="approval-cancel-button" data-toggle="approval-cancel-popover" style="margin-right: 30px;">承認取消</button>
                    @else
                    &nbsp;<span style="font-weight: bold; color: steelblue;margin-right: 30px;"><a href="/admin/attendance/approval/{{ $yearMonth }}">承認済</a></span>
                    @endif
                @else
                    &nbsp;<button class="btn btn-info btn-xs approval-save-button" style="margin-right: 30px;">承認</button>
                @endif
            </div>
        </div>
    </div>
    <!-- /フッター -->
    @endif

</div>

<div id='popover-markup' class='popover-markup' style="visibility:hidden">

		<!--link href="/css/funky-radio-buttons.css" rel="stylesheet"/-->



	<input type='hidden' name='date' value='#####DATE#####'>
	<div class='content'>

		<div>#####DATE_DISPLAY#####<button id="addSelectGroupBtn" class="btn btn-warning btn-sm"  style="float:right; margin-bottom:8px;">追加 <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></button>

		<button id="removeSelectGroupBtn" class="btn btn-danger btn-sm"  style="width:0; display: none; float:right; margin-bottom:8px;">削除 <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span></button></div>



		<div>
			<select id='paidHoliday1' class='form-control' name='paidHoliday'>
				<option value="0" selected>　</option>
			@foreach ($paidHolidays as $holiday)

        		<option value="{{ $holiday->id }}" style="font-weight:bold; color:{{ getPaidHolidayColor($holiday->id) }};">{{ $holiday->name }}</option>

			@endforeach
			</select>
		</div>

		<!--div class="funkyradio" id="checkbox2timesContainer">
			<div class="funkyradio-danger">
				<input type="checkbox" name="checkbox2times" id="checkbox2times"/>
				<label for="checkbox2times" style='margin-top:12px;margin-bottom:8px;'  >2回出勤</label>
			</div>
		</div-->

		<textarea class='form-control' rows='3' style='margin-top:6px;margin-bottom:10px;'  name='note' id="note">#####NOTE#####</textarea>


		<div class='text-center'>
				<button name='updateNoteBtn' class='btn btn-primary' type='button' onClick='clickUpdateNote("#####ID#####")'>更新</button>&nbsp;
				<button class='btn btn-default' type='button' onclick='hidePopover()'>キャンセル</button>
		</div>
	</div>
</div>

<div id='approval-cancel-markup' class='popover-markup' style="visibility:hidden">
	<div class='content'>
		<div>
			<textarea class='form-control' rows='3' style='margin-top:10px;margin-bottom:10px;'  name='approval-cancel-note' id="approval-cancel-note">{{ $approvalNote }}</textarea>
			<div>
				<button name='updateNoteBtn' class='btn btn-danger' type='button' onClick='clickApprovalCancel()' >承認取消</button>&nbsp;
				<button class='btn btn-default' type='button' onclick='hidePopover()'>キャンセル</button>
			</div>
		</div>
	</div>
</div>

<div id="popover-paidholiday-help"  class="panel panel-default" style="visibility:hidden;">
	<div class="panel-body" style="color:#333333">
	<img src="/images/paidholiday_balance.png"/>
		<br>
		<br>
		■ 数字は<font color="#FF0000" ><b>当月使用する有休を引いた</b></font>残日数<br>
		■ 時間有休残は有休残日時に含まれる(有休残日時がすべての残り)<br>
		■ 時間有休残は40時間(1年に使える時間)<br>
		■ 時間有休残が0時間になった →  時間有休は使えない<br>
		■ 有休残日時 < 時間有休残 → 有休残日時 = 時間有休残(減る) <br>
	</div>
</div>



<script>

	$("#dayBalance").text("{{$dayBalance}}");
	$("#dayHoursBalance").text("{{$dayHoursBalance}}");
	$("#hourBalance").text("{{$hourBalance}}");

	$("#dayBalance").css("color", "{{getPaidHolidayBalanceColor($dayBalance)}}");
	$("#dayHoursBalance").css("color", "{{getPaidHolidayBalanceColor($dayBalance)}}");
	$("#hourBalance").css("color", "{{getPaidHourBalanceColor($hourBalance)}}");

	var unRecordedCount = {{ $unRecordedCount }};
	var underOfficialWorkingHourCount = {{ $underOfficialWorkingHourCount }};

	$(function(){
		// JavaScript で表示
		$('#staticModalButton').on('click', function()
		{
			$('#staticModal').modal();
		});
		// ダイアログ表示前にJavaScriptで操作する
		$('#staticModal').on('show.bs.modal', function(event)
		{
			var button = $(event.relatedTarget);
			var recipient = button.data('whatever');
			var modal = $(this);
			modal.find('.modal-body .recipient').text(recipient);
			//modal.find('.modal-body input').val(recipient);
		});
		// ダイアログ表示直後にフォーカスを設定する
		$('#staticModal').on('shown.bs.modal', function(event)
		{
			$(this).find('.modal-footer .btn-default').focus();
		});
		$('#staticModal').on('click', '.modal-footer .btn-primary', function() {
		$('#staticModal').modal('hide');
			alert('変更を保存をクリックしました。');
		});

		@if (\Auth::user()->haveSystemAdministratorRole() || \Auth::user()->haveManagimentTeamRole())

		$("#select_user").select2({
			width:"260px",
			closeOnSelect: false,
			placeholder: "社員を選択してください",
			allowClear: false
		});

		@endif

		$("#select_user").next(".select2'").find('.select2-selection')
.css("border-radius", "0px");

		$("#select_user").next(".select2'").find('.select2-selection')
.css("height", "32px");
	});
</script>

<script src="/js/attendance/util.js"></script>
<script src="/js/attendance/employees.js"></script>

@if (\Auth::user()->haveSystemAdministratorRole() || \Auth::user()->haveManagimentTeamRole())
<link href="/css/select2.min.css" rel="stylesheet" />
<script src="/js/select2.full.min.js"></script>
@endif


@endsection
