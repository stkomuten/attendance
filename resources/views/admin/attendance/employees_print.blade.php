@extends('layout')
@section('content')

<style>
	.table_bottom_space
	{
		margin-bottom:50px;		
	}

    thead
    {
        background-color: #333333;
        color: #FFFFFF;
    }

    th td
    {
        text-align: center;
    }

    .square_button {
        border-radius: 0 !important;
        color:#000;
        background-color:#FFF;
        border-color:#8C8C8C;
    }

    .square_button:hover
    {
        color: #FC0;
        background-color: #000;
        border-color: #888;
    }

    .btn-default.active, .btn-default.active:hover, .btn-default.active:focus
    {
        color: #333;
        background-color: #e6e6e6;
        border-color: #adadad;
    }

    td
    {
        word-break: break-all;
    }

    /* フッターの固定 */
    div#footer-fixed
    {
        position: fixed;            /* フッターの固定 */
        bottom: 0px;                /* 位置(下0px) */
        left: 0px;                  /* 位置(左0px) */
        width: 100%;                /* 横幅100%　*/
        opacity: 0.9;
    }

    div#footer-bk
    {
        background-color:#333333;     /* 背景色(黒) */
        height:40px;                  /* 縦の高さ140px */
        width:100%;                   /* 横の幅を100% */
    }

    div#footer
    {
        text-align: right;
        padding: 10px;
        color: #FFFFFF;
    }

	span#left-label
	{
		float: left;
		font-weight: bold;
		font-size:18px;
		margin: 0px,0px,0px,20px;
	}

	span#right-label
	{
		float: right;
		font-weight: bold;
		font-size:18px;
		margin: 0px,20px,0px,0px;
	}

</style>

<meta name="csrf-token" content="{{ csrf_token() }}" />


	<div class="form-group" style="margin-top:20px;">

	    <span id="left-label">{{ $selectYear }}年{{ (int)$selectMonth}}月</span>

	    <span id="right-label">

		@foreach ($userList as $id => $name)
			@if ($userData->emp_no == $id)
				{{ $id }} : {{ $name }}
            		@endif
		@endforeach
	    </span>

	</div>

	<div>
	<table class="table table-striped table-bordered table_bottom_space">
		<thead>
			<tr>
				<th class="text-center" rowspan="2">日付</th>
				<th class="text-center" rowspan="2">曜日</th>
				<!--th class="text-center" colspan="2">記録時間</th-->
				<th class="text-center" colspan="6">
				    勤務記録
				    <!--span style="float: right; margin-left: -55px;">
				        <a class="btn btn-default btn-xs" href="#staticModal" data-toggle="modal">csv表示</a>
				    </span-->
				</th>
				<th class="text-center" rowspan="2">有給休暇</th>
				<th class="text-center" rowspan="2">備考</th>

				</tr>
				<tr>
				<th class="text-center">出勤時間</th>
				<th class="text-center">退勤時間</th>
				<th class="text-center">始業時間</th>
				<th class="text-center">終業時間</th>
				<th class="text-center">実働時間</th>
				<th class="text-center">残業時間</th>
			</tr>
		</thead>
        <tbody>
		{{-- */
		
				$count = 0;
				$unRecordedCount = 0;
				$OriginWorkDateTotalArray = array();
				$OverWorkDateTotalArray = array();
				$paidHolidayTotal = 0;
				$originWorkDate = null;
		
		/* --}}
        @foreach ($listData as $key => $data)
	<tr class="text-center" style="background-color: {{ $data['week_color'] }};color: {{ $data['text_color']}}">
		<td>{{ $data['day_str'] }}</td>
		<td>{{ $data['week'] }}</td>

		{{-- */
				$isUnRecorded = FALSE;
		/* --}}


		@if (isset($data['detail']))

		<!--td>{!! getTimeRecorderStartDate($data['detail']->start_date) !!}</td-->
		<!--td>{!! getTimeRecorderEndDate($data['detail']->end_date) !!}</td-->

		{{-- */
		
				$start_bgColor = "";
				$end_bgColor = "";
				
				$division_bgColor = "";
				
				$start_style = $data['detail']->start_date !== $data['detail']->init_start_date ? "style=color:#ff8080" : '';
				$end_style = $data['detail']->end_date !== $data['detail']->init_end_date ? "style=color:#ff8080" : '';
				
				$startText	= " -- ";
				$endText	= " -- "; 

				
				//	過去日のみ
				if(strtotime($data['detail']->reg_date) < strtotime("today"))
				{
					$isEnabledHoliday = false;
					
					if(isset($data['detail']->paid_holiday))
					{
						$paid_holidays = unserialize($data['detail']->paid_holiday);
						$count = count($paid_holidays);
						
						$p1 = $paid_holidays[0];
						
						if($p1 === "1" || $p1 === "10" ||  $p1 === "11"  ||  $p1 === "12"  ||  $p1 === "14" ||  $p1 === "15" ||  $p1 === "16" ||  $p1 === "17" ||  $p1 === "18" ||  $p1 === "19")
						{
							$isEnabledHoliday = true;
						}
					}
					
					if(isset($data['detail']->start_date) === false && isset($data['detail']->end_date) === false && $isEnabledHoliday === false && $data['week_color'] === "")
					{
						$start_bgColor = $end_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
					}else if(isset($data['detail']->start_date) === false && $isEnabledHoliday === false && $data['week_color'] === "") {
						$start_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
					}else if(isset($data['detail']->end_date) === false && $isEnabledHoliday === false && $data['week_color'] === "") {
						$end_bgColor = 'style=color:#FFF;font-weight:bold bgcolor=#FF0000';
					}
					
					
					$startText = $start_bgColor === "" ?  " -- " : "未";
					$endText = $end_bgColor === "" ?  " -- " : "未";
					
					$division_bgColor = (isset($data['detail']->division_id) === false || $data['detail']->division_id === 0) && $isEnabledHoliday === false ? 'style=color:#FFF;font-weight:bold bgcolor=#FF0000' : '';
					$divisionText = $division_bgColor === "" ?  " -- " : "未";
					
					if($start_bgColor !== "" || $end_bgColor !== "" || $division_bgColor !== "")
					{
						$isUnRecorded = TRUE;
					}
				}
				
				
			
		/* --}}




		<td {{$start_bgColor}} {{$start_style}}>{{ isset($data['detail']->start_date) ? getExcelStartDate($data['detail']->start_date) : $startText }}</td>
		<td {{$end_bgColor}} {{$end_style}}>{{ isset($data['detail']->end_date) ? getExcelEndDate($data['detail']->start_date, $data['detail']->end_date) : $endText }}</td>

		<td class="col-md-1" {{ $division_bgColor }}>
			@if(isset($data['detail']->division_id) && $data['detail']->division_id !== 0)
				{{-- */$timeSheetAttr = findDivisionAttrByID($data['detail']->division_id, $timeSheet) /* --}}
				{{ convertDisplayTime($timeSheetAttr['opening_time'])}}
			@else
				{{$divisionText}}
			@endif
		</td>


		<td class="col-md-1" {{ $division_bgColor }}>
			@if(isset($data['detail']->division_id) && $data['detail']->division_id !== 0)
				{{ convertDisplayTime($timeSheetAttr['closing_time'])}}
			@else
				{{$divisionText}}
			@endif
		</td>
		{{-- */

			//-- 有休 --
			$paid_holidays = null;
			$count = 0;
			if(isset($data['detail']->paid_holiday))
			{
				$paid_holidays = unserialize($data['detail']->paid_holiday);
				$count = count($paid_holidays);
			}


			$originWorkDate = null;
			
			if(isset($data['detail']->start_date) && isset($data['detail']->end_date) && isset($data['detail']->division_id) && $data['detail']->division_id !== 0)
			{
		 		$originWorkDate = getExcelOriginWorkDate($data['detail']->start_date, $data['detail']->end_date, $timeSheet[(int)$data['detail']->division_id - 1]);
				$OriginWorkDateTotalArray[] = $originWorkDate;
			}
			
			$overWorkDate = null;

			if(isset($originWorkDate))
			{
				$overWorkDate = getOver8HourWork($originWorkDate, $paid_holidays, $paidHolidays, $data['detail']->start_date, $data['detail']->end_date, $timeSheet[(int)$data['detail']->division_id - 1]);
				$OverWorkDateTotalArray[] = $overWorkDate;
			}

		/* --}}

		<td>{{ $originWorkDate == null ? "--" : $originWorkDate }}</td>

		<!-- 残業 -->
		<td>{!! isset($overWorkDate) ? $overWorkDate :  "--" !!}</td>

		<!-- 有休 -->
		@if(isset($paid_holidays) && $count > 0)
			<td class="col-md-1" style="font-weight:bold;">

				<input type="hidden" name="paidHolidayData" value="{{ json_encode($paid_holidays)}}"/>

			@for ($i = 0; $i<$count ; $i++)
			
				<font style="color:{{ getPaidHolidayColor((int)$paid_holidays[$i]) }};">{{$paidHolidays[(int)$paid_holidays[$i]-1]->name}}</font>
				@if($i < $count - 1 ) <br>
				@endif
				
			@endfor
			</td>	
		@else
			<td class="col-md-1">--</td>
		@endif
		 
		 
		 

		<td class="col-md-3">{{ isset($data['detail']->note) ? $data['detail']->note : ' -- ' }}</td>

	
                @else			
				{{-- */
							$bgColor = $userData->emp_type === 1 && strtotime($data['date']) < strtotime("today") && $data['week_color'] === "" ? 'style=color:#FFF;font-weight:bold bgcolor=#FF0000' : "";
							$attendText = $bgColor === "" ?  " -- " : "未";
							
							$division_bgColor = $userData->emp_type === 1 && strtotime($data['date']) < strtotime("today") && $data['week_color'] === "" ? 'style=color:#FFF;font-weight:bold bgcolor=#FF0000' : '';
							$divisionText = $division_bgColor === "" ?  " -- " : "未";
							
							if($bgColor !== "" || $division_bgColor !== "")
							{
								$isUnRecorded = TRUE;
							}
							
				/* --}}
				
		<td {{$bgColor}}>{{ $attendText }}</td>
		<td {{$bgColor}}>{{ $attendText }}</td>
		<td {{$division_bgColor}}>{{ $divisionText }}</td>
		<td {{$division_bgColor}}>{{ $divisionText }}</td>
		<td> -- </td>
		<td> -- </td>
		<td> -- </td>
		<td> -- </td>
                @endif
            </tr>
		{{-- */ $count++ /* --}}
        @endforeach
		
			<tr class="text-center">
				<td colspan="6" style="background-color:#BBBBBB;color:#FFFFFF;font-weight:bold"> 合計 </td>
				<td>{{ count($OriginWorkDateTotalArray) > 0 ? getExcelOriginWorkDateTotal($OriginWorkDateTotalArray) : "--" }}</td>
				<td>{{ count($OverWorkDateTotalArray) > 0 ? getExcelOriginWorkDateTotal($OverWorkDateTotalArray) : "--" }}</td>
				<td colspan="3"></td>
			</tr>
        </tbody>
    </table>

</div>

<script>
  $(function() {
	$('.navbar').hide();
    // JavaScript で表示
    $('#staticModalButton').on('click', function() {
      $('#staticModal').modal();
    });
    // ダイアログ表示前にJavaScriptで操作する
    $('#staticModal').on('show.bs.modal', function(event) {
      var button = $(event.relatedTarget);
      var recipient = button.data('whatever');
      var modal = $(this);
      modal.find('.modal-body .recipient').text(recipient);
      //modal.find('.modal-body input').val(recipient);
    });
    // ダイアログ表示直後にフォーカスを設定する
    $('#staticModal').on('shown.bs.modal', function(event) {
      $(this).find('.modal-footer .btn-default').focus();
    });
    $('#staticModal').on('click', '.modal-footer .btn-primary', function() {
      $('#staticModal').modal('hide');
      alert('変更を保存をクリックしました。');
    });
	
	window.print();
  });
</script>

<script src="/js/attendance/util.js"></script>
<script src="/js/attendance/employees.js"></script>

@endsection