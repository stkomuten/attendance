@extends('layout')


@section('content')

<style>
	thead
	{
		background-color: #333333;
		color: #FFFFFF;
	}

	.record-label
	{
		color:#F80;
	}

	.ui-datepicker-trigger
	{
		width: 25px;
		padding-left: 3px;
		margin-top: -3px;
	}
	
	span.glyphicon-calendar
	{
		font-size: 18px;
	}
</style>

<script type="text/javascript" src="/js/jquery.cookie.js" charset="utf-8"></script>


<script>
	CANCEL_URL = "{{ $listPageUrl }}"
	TARGET_DATE = "{{ $targetDate }}000000"

	var timeSheet = 
	[
		@foreach ($timeSheet as $division)
		{
				"id": "{{$division->id}}",
				"opening_time": "{{$division->opening_time}}",
				"closing_time": "{{$division->closing_time}}",
				"overtime_start": "{{$division->overtime_start}}",
				"overtime_end": "{{$division->overtime_end}}",
				"late_night_start": "{{$division->late_night_start}}",
				"late_night_end": "{{$division->late_night_end}}",
				"break_time": "{{$division->break_time}}",
				"break_time_start": "{{$division->break_time_start}}",
				"official_working_hour": "{{$division->official_working_hour}}",
				"morning_off_time": "{{$division->morning_off_time}}",
				"afternoon_off_time": "{{$division->afternoon_off_time}}"
		},                    
		@endforeach
	{}];


</script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
	
@if ($formType == 'create')
	<input type="hidden" id="isExistStartDate" value="false"/>
	<input type="hidden" id="isExistEndDate" value="false"/>
@elseif ($formType == 'edit')
	<input type="hidden" id="isExistStartDate" value="{{ isset($attendanceData['rawStartDate']) ? true : false }}"/>
	<input type="hidden" id="isExistEndDate" value="{{ isset($attendanceData['rawEndDate']) ? true : false }}"/>
	<input type="hidden" id="rawStartDate" value="{{ $attendanceData['rawStartDate'] }}"/>
	<input type="hidden" id="rawEndDate" value="{{ $attendanceData['rawEndDate'] }}"/>
@endif

<div class="modal-header">
	<h3 id="ModalLabel">
		@if ($formType == 'create')
		新規作成
		@elseif ($formType == 'edit')
		編集
		@endif
	</h3>
</div>

<div class="form-horizontal">
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>

	<div class="form-group">
		<div class="col-md-1 col-md-offset-1">
			<label class="control-label">社員番号: </label>
		</div>
	
		<div class="col-md-4">
			<input type="text" class="form-control" id="display_emp_no" disabled="disabled" value="">
		</div>

		<div class="col-md-1">
			<label class="control-label">　　氏名: </label>
		</div>
		
		<div class="col-md-3">
			<select id="emp_no" class="form-control" disabled="disabled">
				@foreach ($userList as $key => $val)
				@if (isset($attendanceData['emp_no']) && $attendanceData['emp_no'] == $key)
				<option value="{{ $key }}" selected="selected">{{ $val }}</option>
				@else
				<option value="{{ $key }}">{{ $val }}</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-1 control-label"></div>
		
		<div class="col-md-1">
			<label class="control-label">就業時間: </label>
		</div>
		
		<div class="col-md-4">
			<select id="divisionSelect" class="form-control attendance-date-form" style="font-weight:bold;">
			<option value=""></option>
			{{-- */
				
			$isPartTime = $attendanceData['default_division_id'] > 999;
			
			/* --}}
			@for($i=0; $i < count($timeSheet);$i++)
			
				{{-- */ $selected = isset($attendanceData['division_id']) && $attendanceData['division_id'] === $timeSheet[$i]->id ? 'selected' : "";/* --}}
				
				@if(($isPartTime === true && $timeSheet[$i]->id > 999) || ($isPartTime === false && $timeSheet[$i]->id < 1000))
				
							
					<option value="{{ $timeSheet[$i]->id }}" {{$selected}} style="font-weight:bold; color:{{ getWorkingHourColor($timeSheet[$i]->official_working_hour, $timeSheet[$i]->break_time) }};">{{  convertDisplayTime($timeSheet[$i]->opening_time) }} ～ {{ convertDisplayTime($timeSheet[$i]->closing_time) }}　 {{convertWorkingHourDisplayTime($timeSheet[$i]->official_working_hour) }}　 {{convertBreakDisplayTime($timeSheet[$i]->break_time)}}</option>
					
				@endif
			@endfor
			</select>
		</div>
	</div>

	@if($formType == 'edit' &&  isset($attendanceData['rawStartDate']))
	<div class="form-group">
		<div class="col-md-1 col-md-offset-1">
			<label class="control-label">出勤記録: </label>
		</div>
		<div class="col-md-8">
			<label class="control-label record-label">{{ str_replace("-", "/", $attendanceData['rawStartDate']) }}</label>
		</div>
	</div>
	@endif

	<div class="form-group">
		<div class="col-md-1 control-label">
			<input type="checkbox" id="start-date-form-active-checkbox" />
		</div>
		
		<div class="col-md-1">
			<label class="control-label">出勤日時: </label>
		</div>

		<div class="col-md-4 ">
			<div class="input-group">
				<input type="text" class="form-control attendance-date-form" id="startDate" value="{{ isset($attendanceData['startDate']) ? $attendanceData['startDate'] : null }}">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button" data-toggle="datepicker" data-target="#startDate" style="padding-bottom:3px;padding-top:6px;">
					<span class="glyphicon glyphicon-calendar"></span></button>
				</span>
			</div>
		</div>

		<div class="col-md-2">
			<select id="startHour" class="form-control attendance-date-form">
				@foreach ($hourList as $val)
				@if (isset($attendanceData['startHour']) && $attendanceData['startHour'] == $val)
					<option value="{{ $val }}" selected="selected">{{ $val }}時</option>
				@else
					<option value="{{ $val }}">{{ $val }}時</option>
				@endif
				@endforeach
			</select>
		</div>

		<div class="col-md-2">
			<select id="startMinute" class="form-control attendance-date-form">
				@foreach ($minuteList as $val)
				@if (isset($attendanceData['startMinute']) && $attendanceData['startMinute'] == $val)
					<option value="{{ $val }}" selected="selected">{{ $val }}分</option>
				@else
					<option value="{{ $val }}">{{ $val }}分</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>

	@if($formType == 'edit' &&  isset($attendanceData['rawEndDate']))
	<div class="form-group">
		<div class="col-md-1 col-md-offset-1">
			<label class="control-label">退勤記録: </label>
		</div>
		<div class="col-md-8">
			<label class="control-label record-label">{{ str_replace("-", "/", $attendanceData['rawEndDate'])}}</label>
		</div>
	</div>
	@endif

	<div class="form-group">
		<div class="col-md-1 control-label">
			<input type="checkbox" id="end-date-form-active-checkbox" />
		</div>

		<div class="col-md-1">
			<label class="control-label">退勤日時: </label>
		</div>
		<div class="col-md-4">
			<div class="input-group">
				<input type="text" class="form-control attendance-date-form" id="endDate" value="{{ isset($attendanceData['endDate']) ? $attendanceData['endDate'] : null }}">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button" data-toggle="datepicker" data-target="#endDate" style="padding-bottom:3px;padding-top:6px;">
					<span class="glyphicon glyphicon-calendar"></span></button>
				</span>
			</div>
		</div>

		<div class="col-md-2">
			<select id="endHour" class="form-control attendance-date-form">
				@foreach ($hourList as $val)
				@if (isset($attendanceData['endHour']) && $attendanceData['endHour'] == $val)
				<option value="{{ $val }}" selected="selected">{{ $val }}時</option>
				@else
				<option value="{{ $val }}">{{ $val }}時</option>
				@endif
				@endforeach
			</select>
		</div>

		<div class="col-md-2">
			<select id="endMinute" class="form-control attendance-date-form">
				@foreach ($minuteList as $val)
				@if (isset($attendanceData['endMinute']) && $attendanceData['endMinute'] == $val)
				<option value="{{ $val }}" selected="selected">{{ $val }}分</option>
				@else
				<option value="{{ $val }}">{{ $val }}分</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>



	<div class="form-group">
		<div class="col-md-8 col-md-offset-2" style="height:60px;">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th class="text-center">日付</th>
						<th class="text-center">出勤時間</th>
						<th class="text-center">退勤時間</th>
						<th class="text-center">始業時間</th>
						<th class="text-center">終業時間</th>
						<th class="text-center">実働時間</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center">
						<td id="preview-day"></td>
						<td id="preview-start-date"></td>
						<td id="preview-end-date"></td>
						<td id="preview-division-start-date"></td>
						<td id="preview-division-end-date"></td>
						<td id="preview-origin-date"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	@if(isset($attendanceData['paid_holiday']))
	<input type="hidden" name="paidHolidayData" value="{{ json_encode($attendanceData['paid_holiday'])}}"/>
	@endif
	<div class="form-group" id="paidHolidaySelectGroup1">
		<div class="col-md-1 col-md-offset-1" style="padding:0px;">
			<label id="title" class="control-label">休暇・休業1:</label>
		</div>

		<div class="col-md-4">
			<select id="paidHoliday" name="paidHoliday" class="form-control" style="font-weight:bold;">
				<option value="0"  selected>　</option>
			@foreach ($paidHolidays as $holiday)
			
				<option value="{{ $holiday->id }}" style="font-weight:bold; color:{{ getPaidHolidayColor($holiday->id) }};">{{ $holiday->name }}</option>
      			
			@endforeach
			</select>
		</div>
		<button id="addSelectGroupBtn" type="button" class="btn btn-warning" aria-label="Left Align"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> 追加</button>
		<button id="removeSelectGroupBtn" type="button" class="btn btn-danger" aria-label="Left Align" style="display:none;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> 削除</button>
	</div>
	
	<div class="form-group" id="noteGroup">
		<div class="col-md-1 col-md-offset-1">
			<label class="control-label">　　備考: </label>
		</div>
		<div class="col-md-8">
			<textarea class="form-control" rows="4" id="note">{{ isset($attendanceData['note']) ? $attendanceData['note'] : "" }}</textarea>
		</div>
	</div>

	<div class="row text-center">
		<a data-number="{{ isset($id) ? $id : null }}" class="col-md-2 col-md-offset-3 btn btn-primary submitButton">更新</a>
		<a class="col-md-2 col-md-offset-1 btn btn-default resetButton">キャンセル</a>
	</div>
</div>

<script src="/js/attendance/util.js"></script>
<script src="/js/attendance/form.js"></script>

@endsection
