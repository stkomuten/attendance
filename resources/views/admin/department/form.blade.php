@extends('layout')

@section('content')


<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="modal-header">
    <h3 id="ModalLabel">
        {{ ($id) ? "編集" : "新規作成" }}
    </h3>
</div>

<div class="form-horizontal">
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>

    @if ($department)
    <div class="form-group">
        <label class="control-label col-sm-1">ID: </label>
        <div class="col-sm-4">
            <span style="display: block; height: 34px; width: 100%;padding: 6px 12px;">{{ $department->id }}</span>
        </div>
    </div>
    @endif

    <div class="form-group">
        <label class="control-label col-sm-1">部署名: </label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="department_name" value="{{ ($department) ? $department->name : "" }}" style="display: inline; width: 250px;">
        </div>
    </div>

    <div class="row text-center">
      <a data-number="{{ isset($id) ? $id : null }}" class="col-md-2 col-md-offset-3 btn btn-primary submitButton">更新</a>
      <a class="col-md-2 col-md-offset-1 btn btn-default resetButton">キャンセル</a>
    </div>
</div>

<script src="/js/department/form.js"></script>

@endsection
