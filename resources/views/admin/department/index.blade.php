@extends('layout')
@section('content')



<style>
	thead
	{
		background-color: #333333;
		color: #FFFFFF;
	}

	th td
	{
		text-align: center;
	}
</style>


<meta name="csrf-token" content="{{ csrf_token() }}" />

<!--div class="page-header" style="margin-top:-30px;padding-bottom:0px;">
    <h1><small>部署情報</small></h1>
</div-->

<div style="text-align: right">
    <a href="/admin/department/create" class="btn btn-warning">＋新規追加</a>
</div>

<br/>

<div>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th class="text-center">ID</th>
                <th class="text-center">部署名</th>
                <th class="text-center">所属人数</th>
                <th class="text-center">操作</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($departmentList as $data)
            <tr>
                <td class="text-center">{{ $data->id }}</td>
                <td class="text-center">{{ $data->name }}</td>
                <td class="text-center">{{ $data->attachMember }}人</td>
                <td class="text-center">
                    <a href="/admin/department/member/{{ $data->id }}" class="btn btn-default btn-xs">所属社員一覧</a>
                    &nbsp;<a href="/admin/department/edit/{{ $data->id }}" class="btn btn-info btn-xs">編集</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection