@extends('layout')

@section('content')

<style>
    thead
    {
        background-color: #333333;
        color: #FFFFFF;
    }
</style>

<script type="text/javascript" src="/js/jquery.cookie.js" charset="utf-8"></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="page-header" style="margin-top:-30px;padding-bottom:0px;">
    <h1><small>{{ $department->name }} 所属社員編集</small></h1>
</div>

<div class="form-horizontal" style="margin-bottom: 20px;">

    <div class="form-group">
        <div class="col-md-2 col-md-offset-2">
            <label class="control-label">所属</label><br />
            <select id="registUserList" class="form-control attendance-date-form" multiple style="height: 300px;">
            @foreach ($registedUserList as $val)
                <option value="{{ $val->emp_no }}">{{ $val->last_name }} {{ $val->first_name }}</option>
            @endforeach
            </select>
        </div>

        <div class="col-md-offset-1 col-md-2" style="top: 80px;margin-bottom: 140px;">
            <button type="button" id="registButton" class="btn btn-primary btn-block">←追加</button>
            <button type="button" id="deleteButton" class="btn btn-default btn-block">削除→</button>
        </div>

        <div class="col-md-offset-1 col-md-2">
            <label class="control-label">未所属</label><br />
            <select id="unRegistUserList" class="form-control attendance-date-form" multiple style="height: 300px;">
            @foreach ($unRegistedUserList as $val)
                <option value="{{ $val->emp_no }}">{{ $val->last_name }} {{ $val->first_name }}</option>
            @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
      <div class="col-md-10 col-md-offset-1">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>氏名</th>
                    <th>ポジション</th>
                </tr>
            </thead>
            <tbody id="department_position_type">
                @foreach ($registedUserList as $val)
                <tr class="department-position-list" data-empno="{{ $val->emp_no }}">
                    <td>{{ $val->last_name }} {{ $val->first_name }}</td>
                    <td>
                        <select class="form-control">
                        @foreach ($positionList as $position_id => $position_name)
                            @if ($position_id == $val->position_id)
                            <option value="{{ $position_id }}" selected="selected">{{ $position_name }}</option>
                            @else
                            <option value="{{ $position_id }}">{{ $position_name }}</option>
                            @endif
                        @endforeach
                        </select>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
      </div>
    </div>

    <div class="row text-center">
      <a data-number="{{ $department->id }}" class="col-md-2 col-md-offset-3 btn btn-primary submitButton">更新</a>
      <a class="col-md-2 col-md-offset-2 btn btn-default resetButton">キャンセル</a>
    </div>
</div>

<script>

$POSITION_TYPE_SELECT = $("<select>", { class: 'form-control' })
POSITION_LIST = {}
@foreach ($positionList as $key => $val)
    $option = $("<option>", { value: "{{$key}}", text: "{{$val}}" });
    $POSITION_TYPE_SELECT.append($option)
@endforeach

CANCEL_URL = "{{ $listPageUrl }}"

</script>

<script src="/js/department_member/edit.js"></script>

@endsection
