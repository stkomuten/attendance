@extends('layout')
@section('content')

<style>
    thead
    {
        background-color: #333333;
        color: #FFFFFF;
    }

</style>

<div class="page-header" style="margin-top:-30px;padding-bottom:0px;">
    <h1><small>{{ $department->name }} 所属社員</small></h1>
</div>

<div style="margin-bottom: 10px; text-align: right;">
    <a href="/admin/department/member/edit/{{ $department->id }}" class="btn btn-warning">所属社員変更</a>
</div>

<div>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>社員ID</th>
                <th>氏名</th>
                <th>ポジション</th>
                <th>所属日時</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($userList as $data)
            <tr>
                <td>{{ $data->emp_no }}</td>
                <td>{{ $data->last_name }} {{ $data->first_name }}</td>
                <td>{{ $data->position }}</td>
                <td>{{ $data->assign_date }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection