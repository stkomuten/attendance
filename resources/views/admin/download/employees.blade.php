@extends('layout')
@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="page-header" style="margin-top:-30px;padding-bottom:0px;">
    <h1><small>勤怠記録CSVファイル ダウンロード</small></h1>
</div>


<h2><small>対象の月次</small></h2>
<h5 id="file-detail"></h5>
<div class="row">
    <div class="col-md-5">
		<select id="yearMonth1" class="form-control">
			@for ($i=0;$i<count($data);$i++)
			
			<option value="{{  $data[$i]['year'] .  $data[$i]['month'] }}" {{ $i === count($data) - 1 ? "selected" : ""}}>{{  $data[$i]['year'] . '年' .  (int)$data[$i]['month'] . '月' }}</option>
			
			@endfor
		</select>
    </div>
	<div class="text-center col-md-offset1 col-md-6">
        <button id="download1-btn" type="button" class="btn btn-primary btn-block">ダウンロード</button>
	</div>
</div>


<br/>
<br/>
<br/>


<div class="page-header" style="margin-top:-30px;padding-bottom:0px;">
    <h1><small>入退出CSVファイル ダウンロード</small></h1>
</div>


<h2><small>対象の月次</small></h2>
<h5 id="file-detail"></h5>
<div class="row">
    <div class="col-md-5">
		<select id="yearMonth2" class="form-control">
			@for ($i=0;$i<count($data);$i++)
			
			<option value="{{  $data[$i]['year'] .  $data[$i]['month'] }}" {{ $i === count($data) - 1 ? "selected" : ""}}>{{  $data[$i]['year'] . '年' .  (int)$data[$i]['month'] . '月' }}</option>
			
			@endfor
		</select>
    </div>
	<div class="text-center col-md-offset1 col-md-6">
        <button id="download2-btn" type="button" class="btn btn-primary btn-block">ダウンロード</button>
	</div>
</div>


<br/>
<br/>
<br/>


<div class="page-header" style="margin-top:-30px;padding-bottom:0px;">
    <h1><small>勤怠分析用CSVファイル ダウンロード</small></h1>
</div>


<h2><small>対象の月次</small></h2>
<h5 id="file-detail"></h5>
<div class="row">
    <div class="col-md-5">
		<select id="yearMonth3" class="form-control">
			@for ($i=0;$i<count($data);$i++)
			
			<option value="{{  $data[$i]['year'] .  $data[$i]['month'] }}" {{ $i === count($data) - 1 ? "selected" : ""}}>{{  $data[$i]['year'] . '年' .  (int)$data[$i]['month'] . '月' }}</option>
			
			@endfor
		</select>
    </div>
	<div class="text-center col-md-offset1 col-md-6">
        <button id="download3-btn" type="button" class="btn btn-primary btn-block">ダウンロード</button>
	</div>
</div>

<script src="/js/download.js"></script>

@endsection