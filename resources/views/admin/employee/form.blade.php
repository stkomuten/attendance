@extends('layout')

@section('content')
<script type="text/javascript" src="/js/employees.js"></script>
<script type="text/javascript" src="/js/jquery.validationEngine.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/jquery.validationEngine-ja.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/swfobject.js" ></script>

<link  type="text/css" rel="stylesheet" href="/css/validationEngine.jquery.css"/>

<script type="text/javascript">

	$(document).ready(function()
	{
		$("#empForm").validationEngine('attach', {promptPosition:"topLeft", onValidationComplete:onValidationComplet});
	});

	function startScan()
	{
		var player = document.FCReader || window.FCReader;
		
		player.startScan();
		
		alert("社員証をカードリーダーにかざしてくだい。");
	}

	function successGetFeliCaId(feliCaId)
	{
		$("input[name=felica_id]").val(feliCaId);
		
		alert("読込完了");
	}

	//
	var params = {name:"FCReader", scale:"noscale", menu:"false", allowScriptAccess:"always", allowFullScreen:"true", wmode:"window"}
	//
	var attributes = {id:"FCReader", name:"FCReader", xiRedirectUrl:this.location};
	//
	swfobject.embedSWF("/FCReader.swf", "flashcontent", "0%", "0%", "10.1.0.0", "/playerProductInstall.swf", null, params, attributes);

</script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="mode" content="{{ $mode }}" />

<div class="modal-header">
  <h3 id="ModalLabel">{{ $mode == "add" ? "新規登録" : "編集"}}</h3>
</div>

<div class="form-horizontal">
	<form id="empForm" role="form" method="post">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>

		<div class="form-group">
			<label class="control-label col-md-2">社員番号: </label>
			<div class="col-md-8">
				<input type="text" class="form-control validate[required, custom[onlyLetterNumber]]" name="emp_no" placeholder="社員番号" value="{{ $mode === "update" ? $employee->emp_no : '' }}">
			</div>
		</div> <!-- /field -->

		<div class="form-group">
			<label class="control-label col-md-2">姓: </label>
			<div class="col-md-3">
				<input type="text" class="form-control validate[required]" name="last_name" placeholder="姓" value="{{ $mode === "update" ? $employee->last_name : '' }}">
			</div>
			<label class="control-label col-md-1">名: </label>
			<div class="col-md-3">
				<input type="text" class="form-control validate[required]" name="first_name" placeholder="名" value="{{ $mode === "update" ? $employee->first_name : '' }}">
			</div>
		</div> <!-- /field -->

		<div class="form-group">
			<label class="control-label col-md-2">社員証番号: </label>
			<div class="col-md-7">
				<input type="text" class="form-control validate[required, custom[onlyLetterNumber]]" name="felica_id" placeholder="社員証番号" value="{{ $mode === "update" ? $employee->felica_id : '' }}">
			</div>
			<div class="col-md-1">
				<button class="btn" data-dismiss="modal" aria-hidden="true" type="button" onClick="startScan()">←□</button>
			</div>
		</div> <!-- /field -->
		
		<div class="form-group">
			<label class="control-label col-md-2">メールアドレス: </label>
			<div class="col-md-8">
				<input type="email" class="form-control validate[required, custom[email]]" name="mail" placeholder="メールアドレス" value="{{ $mode === "update" ? $employee->mail : '' }}" value="{{ $mode === "update" ? $employee->first_name : '' }}">
			</div>
		</div> <!-- /field -->


		<div class="form-group">
			<label class="control-label col-md-2">部署: </label>
			<div class="col-md-8">
				<select name="department_id" class="form-control">
			
					@foreach($departments as $row)
					
					@if ($mode === "update" && is_null($department_relations) === FALSE && $row->id === $department_relations->department_id)
					<option value="{{ $row->id }}" selected>{{ $row->name }}</option>
					
					@else
					<option value="{{ $row->id }}">{{ $row->name }}</option>
					@endif
					@endforeach
					
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">ポジション: </label>
			<div class="col-md-8">
				<select name="position_id" class="form-control">
				
					@foreach($positions as $row)
					
					@if ($mode === "update" && is_null($department_relations) === FALSE && $row["id"] === $department_relations->position_id)
					<option value="{{ $row['id'] }}" selected>{{ $row['name'] }}</option>
					@else
					<option value="{{ $row['id'] }}">{{ $row['name'] }}</option>
					@endif
					
					@endforeach
				
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">雇用形態: </label>
			<div class="col-md-8">
				<select name="emp_type" class="form-control">
				
					@foreach($emp_types as $row)
					
					@if ($mode === "update" && $row->id === $employee->emp_type)
					<option value="{{ $row->id }}" selected>{{ $row->name }}</option>
					
					@else
					<option value="{{ $row->id }}">{{ $row->name }}</option>
					@endif
					@endforeach
				
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">状態: </label>
			<div class="col-md-8">
				<select name="status" class="form-control">
				
					@foreach($emp_status as $row)
					@if ($mode === "update" && $row->id === $employee->status)
					<option value="{{ $row->id }}" selected>{{ $row->name }}</option>
					@else
					<option value="{{ $row->id }}">{{ $row->name }}</option>
					@endif
					@endforeach
				
				</select>
			</div>
		</div>
	

		<div class="form-group">
			<label class="control-label col-md-2">就業時間: </label>
			<div class="col-md-8">
				<select name="default_division_id" class="form-control attendance-date-form">
				<option value=""></option>
					@for($i=0; $i < count($timeSheet);$i++)
					
					{{-- */ $selected = isset($employee->default_division_id) && (int)$employee->default_division_id === $timeSheet[$i]->id  ? 'selected="selected"' : "";/* --}}
					<option value="{{ $timeSheet[$i]->id }}" {{$selected}} style="font-weight:bold; color:{{ getWorkingHourColor($timeSheet[$i]->official_working_hour, $timeSheet[$i]->break_time) }};">{{  convertDisplayTime($timeSheet[$i]->opening_time) }} ～ {{ convertDisplayTime($timeSheet[$i]->closing_time) }}　 {{convertWorkingHourDisplayTime($timeSheet[$i]->official_working_hour) }}　 {{convertBreakDisplayTime($timeSheet[$i]->break_time)}}</option>
					@endfor
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">勤怠管理: </label>
			<div class="col-md-8">
				<input type="radio" name="attendance_flg" value="1" {{ $employee->attendance_flg === 1 ? 'checked' : '' }}/> する&nbsp;
				<input type="radio" name="attendance_flg" value="0" {{ $employee->attendance_flg === 0 ? 'checked' : '' }}/> しない
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">メール送信: </label>
			<div class="col-md-8">
				<input type="radio" name="send_mail_flg" value="1" {{ $employee->send_mail_flg === 1 ? 'checked' : '' }}/> する&nbsp;
				<input type="radio" name="send_mail_flg" value="0" {{ $employee->send_mail_flg === 0 ? 'checked' : '' }}/> しない
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">登録日: </label>
			<div class="col-md-8">
				<div class="input-group">
				<input type="text" class="form-control attendance-date-form" id="regDate" name="reg_date" value="{{ date('Y/m/d', strtotime($employee->reg_date)) }}">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button" data-toggle="datepicker" data-target="#regDate">
					<span class="glyphicon glyphicon-calendar"></span></button>
				</span>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">退職日: </label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" class="form-control attendance-date-form" id="resignDate" name="resign_date" value="{{ isset($employee->resign_date) ? date('Y/m/d', strtotime($employee->resign_date )) : null }}">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" data-toggle="datepicker" data-target="#resignDate">
						<span class="glyphicon glyphicon-calendar"></span></button>
					</span>
				</div>
			</div>
		</div>
						
	</form>
	
	<div class="row text-center">
		<a class="col-md-2 col-md-offset-3 btn btn-primary" data-dismiss="modal" aria-hidden="true" onClick="clickAddUpdate('{{$mode}}')">{{ $mode == "add" ? "追加" : "更新"}}</a>
		<a class="col-md-2 col-md-offset-1 btn btn-default" data-dismiss="modal" aria-hidden="true" onClick="clickCancel()">キャンセル</a>
	</div>
</div>

<div id="flashcontent"></div>

<script type="text/javascript">
	$(function()
	{
		$('[data-toggle=datepicker]').each(function () {
			var target = $(this).data('target') || '';
			if (target)
			{
				$(target).datepicker();
				$(this).bind("click", function () {
				$(target).datepicker("show");
				});
			}
		});
	});
</script>

@endsection
