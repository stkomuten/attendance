@extends('layout')
@section('content')

<style>
	thead
	{
		background-color: #333333;
		color: #FFFFFF;
	}

	td
	{
		text-align: center;
		vertical-align: middle;
	}
	
	.table thead>tr>th.middle
	{
		vertical-align:middle;
	}

	.table tbody>tr>td.middle
	{
		vertical-align:middle;
	}
	
	.even
	{
		background: #F9F9F9;
	}
	.odd
	{
		background: #FFF;
	}
	

</style>

<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="form-group">
	<label>フィルタ:</label>
	<select id="emp_status" class="form-control" onchange="changeStatus()" style="width: 30%; display:inline;">
		<option value="0">すべて</option>
		@foreach($emp_status as $row)
			@if ((int)$row->id === (int)$status)
				<option value="{{ $row->id }}" selected>{{ $row->name }}</option>
			@else
				<option value="{{ $row->id }}">{{ $row->name }}</option>{{$status . $row->id}}
			@endif
		@endforeach
	</select>

	<span style="float: right">
		<a href="/admin/user/register" class="btn btn-warning">＋新規追加</a>
	</span>
</div>

<div class="form-group">
	<table class="table table-bordered">
		<thead>
		<tr>
			<th class="text-center" rowspan="2"></th>
			<th class="text-center middle" rowspan="2">社員番号</th>
			<th class="text-center middle" rowspan="2">社員証番号</th>
			<th class="text-center middle" rowspan="2">氏名</th>
			<th class="text-center middle" rowspan="2">メールアドレス</th>
			<th class="text-center middle" rowspan="2">雇用形態</th>
			<th class="text-center middle" rowspan="2">状態</th>
			<th class="text-center middle" colspan="1" style="width:7%">登録日</th>
			<th class="text-center middle" rowspan="2">勤怠管理</th>
			<th class="text-center middle" rowspan="2">メール送信</th>
			<th class="text-center middle" rowspan="2">就業時間</th>
			<th class="text-center middle" rowspan="2">操作</th>
		</tr>
		<tr>
			<th class="text-center" rowspan="1">退職日</th>
		</tr>
		</thead>
		{{-- */$count = 1 + ($employees->currentPage() - 1) * $employees->perPage() /* --}}
		@foreach($employees as $row)
		{{-- */
			
			$regDate = date('Y/n/j', strtotime($row->reg_date));
			
			$resignDate = "--"; 
			
			if(!is_null($row->resign_date))
			{
				$resignDate = date('Y/n/j', strtotime($row->resign_date));
			}	
			
			$hitIndex = -1;
			
			 $cellClass = $count % 2 == 0 ? 'even' : 'odd';
		/* --}}
		<tr class="{{$cellClass}}">
			<td class="text-center middle" rowspan="2">{{ $count }}</td>
			<td class="text-center middle" rowspan="2">{{ $row->emp_no }}</td>
			<td class="text-center middle" rowspan="2">{{ $row->felica_id }}</td>
			<td class="text-center middle" rowspan="2">{{ $row->last_name . " " . $row->first_name }}</td>
			<td class="text-center middle" rowspan="2">{{ $row->mail }}</td>
			<td class="text-center middle" rowspan="2">{{ $row->type_name }}</td>
			<td class="text-center middle" rowspan="2">{{ $row->status_name }}</td>
			<td class="text-center middle" colspan="1" >{{ $regDate }}</td>
			<td class="text-center middle" rowspan="2">{{ $row->attendance_flg === 1 ? "する" : "しない" }}</td>
			<td class="text-center middle" rowspan="2">{{ $row->send_mail_flg === 1 ? "する" : "しない" }}</td>
			@for($i=0; $i < count($timeSheet);$i++)
				
				{{-- */ $division_id = isset($row->default_division_id) ? (int)$row->default_division_id - 1: 6 /* --}}
				@if($row->default_division_id === $timeSheet[$i]->id)
				{{-- */ $timeSheetInfo = $timeSheet[$i]; /* --}}
				<td class="text-center" rowspan="1" style="font-weight:bold; color:{{ getWorkingHourColor($timeSheet[$i]->official_working_hour, $timeSheet[$i]->break_time) }};">{{  convertDisplayTime($timeSheet[$i]['opening_time']) }} ～ {{ convertDisplayTime($timeSheet[$i]['closing_time']) }}</td>
				@endif
			@endfor
			
			<td class="text-center middle" rowspan="2">
				<a href="#" class="btn btn-danger btn-xs" onclick="clickRemove('{{ $row->emp_no }}')">削除</a>
				<a href="/admin/employee/edit/{{ $row->emp_no }}" class="btn btn-info btn-xs">編集</a>
			</td>
		</tr>
		<tr  class="{{$cellClass}}">
			<td class="text-center" rowspan="1">{{$resignDate}}</td>
			<td class="text-center" rowspan="1" style="font-weight:bold; color:{{ getWorkingHourColor($timeSheetInfo->official_working_hour, $timeSheetInfo->break_time) }};"> {{convertWorkingHourDisplayTime($timeSheetInfo->official_working_hour) }}  {{convertBreakDisplayTime($timeSheetInfo->break_time)}}</td>
		</tr>
		{{-- */ $count++ /* --}}
		@endforeach
	</table>
</div>


<div class="text-center">
	{!! $employees->render(); !!}
</div>


<script src="/js/employees.js"></script>

@endsection