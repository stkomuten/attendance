@extends('layout')
@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="page-header" style="margin-top:-30px;padding-bottom:0px;">
    <h1><small>JIRAへ実働時間 アップロード</small></h1>
</div>


<h5 id="file-detail"></h5>
<div class="row">
    <div class="col-md-5">
		<select id="select_emp" class="form-control" style="width:260px;float:left;">
			@foreach ($userList as $id => $name)
				@if ($id == "010094")
					<option value="{{ $id }}" selected="selected">{{ $id }} : {{ $name }}</option>
				@else
					<option value="{{ $id }}">{{ $id }} : {{ $name }}</option>
				@endif
			@endforeach
		</select>
    </div>
	<div class="text-center col-md-offset1 col-md-6">
        <button id="upload1-btn" type="button" class="btn btn-primary btn-block">アップロード</button>
	</div>
</div>

<div id="result"></div>



<style>
input[type="file"] {
  display: none;
}
</style>

<script src="/js/upload/view.js"></script>

@endsection