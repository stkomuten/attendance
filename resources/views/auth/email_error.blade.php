@extends('public_layout')
@section('content')

<div class="modal-header">
  <h3 id="ModalLabel">パスワード再設定</h3>
</div>

<div class="form-horizontal">
  <form role="form" method="GET" action="/admin">
    <ul></ul>

    <div clas="form-group">
        <h3>エラー！</h3>
    </div>

    <div class="form-group">
      <div class="col-md-10">
        処理に必要なトークンが存在しないか、有効期限が切れている可能性があります。<br />
        再度メール送信からやり直してください。
      </div>
    </div>

    <div class="row text-center col-md-offset-5">
      <input type="submit" class="col-md-3 btn btn-primary submitButton" value="ログイン画面へ戻る">
    </div>
  </form>
</div>

@endsection
