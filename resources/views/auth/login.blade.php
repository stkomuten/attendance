@extends('public_layout')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <!--div class="panel panel-default"-->
        <!--div class="panel-heading">Login</div-->
        <!--div class="panel-body"-->
          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

		<!--div class="alert alert-info">
		<h4>メンテナンス中です</h4>
		<p>現在メンテナンスを行っております。しばらくの間お待ちください。</p>
		</div-->



          <form class="form-horizontal" role="form" method="POST" action="/auth/login">
            {{-- CSRF対策--}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
              <label class="col-md-4 control-label">メールアドレス:</label>
              <div class="col-md-6">
                <input type="email" class="form-control" name="mail" value="{{ old('email') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">パスワード:</label>
              <div class="col-md-6">
                <input type="password" class="form-control" name="password">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="remember">サインインしたままにする
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                  サインイン
                </button>

                <!-- <a href="/password/email">Forgot Your Password?</a> -->
              </div>
            </div>
          </form>
		  
		  
		  
        <!--/div--><!-- .panel-body -->
      <!--/div--><!-- .panel -->
    </div><!-- .col -->
  </div><!-- .row -->
</div><!-- .container-fluid -->
@endsection