@extends('layout')
@section('content')



<script type="text/javascript" src="/js/swfobject.js" ></script>
<script type="text/javascript">

function startScan()
{
  var player = document.FCReader || window.FCReader;
  player.startScan();
  alert("社員証をカードリーダーにかざしてくだい。");
}

function successGetFeliCaId(feliCaId)
{
  $("input[name=felica_id]").val(feliCaId);
  alert("読込完了");
}

var params = {name:"FCReader", scale:"noscale", menu:"false", allowScriptAccess:"always", allowFullScreen:"true", wmode:"window"}
var attributes = {id:"FCReader", name:"FCReader", xiRedirectUrl:this.location};
swfobject.embedSWF("/FCReader.swf", "flashcontent", "0%", "0%", "10.1.0.0", "/playerProductInstall.swf", null, params, attributes);


</script>


<div class="modal-header">
  <h3 id="ModalLabel">新規追加</h3>
</div>


<div class="form-horizontal">
  <form role="form" method="POST" action="/admin/user/register">
    <ul>
      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <strong>エラー！</strong>入力値にエラーがあります。<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
    </ul>

    {{-- CSRF対策--}}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
      <label class="control-label col-md-2">社員番号: </label>
      <div class="col-md-8">
        <input type="text" class="form-control" name="emp_no" value="{{ old('emp_no') }}">
      </div>
    </div> <!-- /field -->

    <div class="form-group">
      <label class="control-label col-md-2">姓: </label>
      <div class="col-md-3">
        <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="姓">
      </div>
      <label class="control-label col-md-1">名: </label>
      <div class="col-md-3">
        <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="名">
      </div>
    </div> <!-- /field -->

    <div class="form-group">
      <label class="control-label col-md-2">社員証番号: </label>
      <div class="col-md-7">
        <input type="text" class="form-control" name="felica_id" value="{{ old('felica_id') }}">
      </div>
      <div class="col-md-1">
        <button class="btn" data-dismiss="modal" aria-hidden="true" type="button" onClick="startScan()">←□</button>
      </div>
    </div> <!-- /field -->

    <div class="form-group">
      <label class="control-label col-md-2">メールアドレス: </label>
      <div class="col-md-8">
        <input type="email" class="form-control" name="mail" value="{{ old('mail') }}">
      </div>
    </div> <!-- /field -->

    <div class="form-group">
      <label class="control-label col-md-2">部署: </label>
      <div class="col-md-8">
        <select name="department_id" class="form-control">
          @foreach ($departmentList as $key => $val)
          <option value="{{ $key }}">{{ $val }}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-2">ポジション: </label>
      <div class="col-md-8">
        <select name="position_id" class="form-control">
          @foreach ($positionList as $val)
          <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-2">雇用形態: </label>
      <div class="col-md-8">
        <select name="emp_type" class="form-control">
          @foreach ($typeList as $key => $val)
          <option value="{{ $key }}">{{ $val }}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-2">状態: </label>
      <div class="col-md-8">
        <select name="status" class="form-control">
          @foreach ($statusList as $key => $val)
          <option value="{{ $key }}">{{ $val }}</option>
          @endforeach
        </select>
      </div>
    </div>
	
	<div class="form-group">
		<label class="control-label col-md-2">就業時間: </label>
		<div class="col-md-8">
		<select name="default_division_id" class="form-control">
			@for($i=0; $i < count($timeSheet);$i++)
			
				<option value="{{ $timeSheet[$i]->id }}" style="font-weight:bold; color:{{ getWorkingHourColor($timeSheet[$i]->official_working_hour, $timeSheet[$i]->break_time) }};">{{  convertDisplayTime($timeSheet[$i]->opening_time) }} ～ {{ convertDisplayTime($timeSheet[$i]->closing_time) }}　 {{convertWorkingHourDisplayTime($timeSheet[$i]->official_working_hour) }}　 {{convertBreakDisplayTime($timeSheet[$i]->break_time)}}</option>
				
			@endfor
		</select>
		</div>
	</div>

	<div class="form-group">
      <label class="control-label col-md-2">勤怠管理: </label>
      <div class="col-md-8">
	  
        <input type="radio" name="attendance_flg" value="1" checked> する&nbsp;
		<input type="radio" name="attendance_flg" value="0"> しない
		
      </div>
    </div>
	
	<div class="form-group">
      <label class="control-label col-md-2">メール送信: </label>
      <div class="col-md-8">
	  
        <input type="radio" name="send_mail_flg" value="1" checked> する&nbsp;
		<input type="radio" name="send_mail_flg" value="0"> しない
		
      </div>
    </div>
	
    <div class="form-group">
      <label class="control-label col-md-2">パスワード: </label>
      <div class="col-md-8">
        <input type="password" class="form-control" name="password">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-2">パスワード確認: </label>
      <div class="col-md-8">
        <input type="password" class="form-control" name="password_confirmation">
      </div>
    </div>

    <div class="row text-center">
      <input type="submit" class="col-md-2 col-md-offset-3 btn btn-primary submitButton" value="追加">
      <input type="reset" class="col-md-2 col-md-offset-1 btn btn-default resetButton" value="キャンセル">
    </div>
  </form>
</div>

<div id="flashcontent"></div>

<script>

/**
 * キャンセルボタンのイベント
 */
$('.resetButton').click(function(e) {
	window.location.href = "/admin/employee"
})

</script>
@endsection
