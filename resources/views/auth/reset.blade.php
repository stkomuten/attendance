@extends('layout')
@section('content')

<div class="modal-header">
  <h3 id="ModalLabel">パスワード再設定</h3>
</div>


<div class="form-horizontal">
  <form role="form" method="POST" action="{{ $postURL }}">
    <ul>
      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <strong>エラー！</strong>入力値にエラーがあります。<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
    </ul>

    {{-- CSRF対策--}}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    @if (!empty($mail))
      <input type="hidden" name="mail" value="{{ $mail }}">
    @endif


    <div class="form-group">
      <label class="col-md-2 control-label">パスワード:</label>
      <div class="col-md-10">
        <input type="password" class="form-control" name="password">
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-2 control-label">パスワード確認:</label>
      <div class="col-md-10">
        <input type="password" class="form-control" name="password_confirmation">
      </div>
    </div>

    <div class="row text-center col-md-offset-5">
      <input type="submit" class="col-md-2 btn btn-primary submitButton" value="更新">
      <input type="reset" class="col-md-2 col-md-offset-1 btn btn-default resetButton" value="キャンセル">
    </div>
  </form>
</div>


<script type="text/javascript">

/**
 * キャンセルボタンのイベント
 */
$('.resetButton').click(function(e) {
	window.location.href = "/admin"
})

</script>

@endsection
