@extends('public_layout')
@section('content')

<div class="modal-header">
  <h3 id="ModalLabel">パスワード設定完了</h3>
</div>

<div class="form-horizontal">
  <form role="form" method="GET" action="/admin">
    <ul></ul>

    <div class="form-group">
      <div class="col-md-10">
        パスワードをリセットしました。
      </div>
    </div>

    <div class="row text-center col-md-offset-5">
      <input type="submit" class="col-md-3 btn btn-primary submitButton" value="ログイン画面へ戻る">
    </div>
  </form>
</div>

@endsection
