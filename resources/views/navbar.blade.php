{{-- resouces/views/navbar.blade.php --}}

<style>

.navbar-toggle {
    background-color: #FFF;
}

.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus {
    color: #FFCC00;
}

</style>


<script>
    window.addEventListener("load", function() {
      MODE_MOCK = "mock"
      MODE_VIEW = "view"

      var page_count = $.cookie("page_count")
      if (!page_count) {
          page_count = 30
      }

      _editmode = $.cookie("change_mode")
      if (!_editmode || (_editmode != MODE_VIEW && _editmode != MODE_MOCK)) {
          _editmode = MODE_VIEW
      }

      var now_text    = (_editmode == MODE_VIEW) ? "通常モード" : "モックモード"
      var change_text = (_editmode == MODE_VIEW) ? "モックモードに変更" : "通常モードに変更"
      $(".change_mode").text(change_text)

      if (_editmode == MODE_MOCK) {
          $("#mode_text").text("　※モックモード(resources/mock_views/のテンプレートを表示しています)");
      }
    }, false);

    $(function($) {
        $('.page_count').on('click', function(e){
            $.cookie("page_count", e.target.innerHTML, {
                expires: 30
                , path: '/'
            });

            e.preventDefault();
            e.stopPropagation();

            location.reload()
            return false;
        });

        $('.change_mode').on('click', function(e){
            if (!_editmode || (_editmode != MODE_VIEW && _editmode != MODE_MOCK)) {
                _editmode = MODE_VIEW
            }

            _editmode       = (_editmode == MODE_VIEW) ? MODE_MOCK : MODE_VIEW
            var now_text    = (_editmode == MODE_VIEW) ? "通常モード" : "モックモード"
            var change_text = (_editmode == MODE_VIEW) ? "モックモードに変更" : "通常モードに変更"

            $.cookie("change_mode", _editmode, {
                expires: 30
                , path: '/'
            });

            e.preventDefault();
            e.stopPropagation();

            location.reload()
            return false;
        });
    });
</script>

<div id="mode_text" style="background-color:#FFCC00; font-size:25px;"></div>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <!-- スマホやタブレットで表示した時のメニューボタン -->
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <nav class="nav">
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        @if (!Auth::guest())

        {{-- ログインしている時 --}}
        <ul class="nav navbar-nav">
          <li><a href="/admin/attendance"><i class="glyphicon glyphicon-list-alt"></i> 勤怠記録</a></li>

          @if (\Auth::user()->haveSystemAdministratorRole() || \Auth::user()->haveManagimentTeamRole())
          <li><a href="/admin/employee"><i class="glyphicon glyphicon-user"></i> 社員情報</a></li>
          <li><a href="/admin/department"><i class="glyphicon glyphicon-th-large"></i> 部署情報</a></li>
          <!--li><a href="/admin/upload/holiday"><i class="glyphicon glyphicon-transfer"></i> 入出力</a></li-->
			  <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				  <i class="glyphicon glyphicon-transfer"></i> 入出力<span class="caret"></span>
				</a>
	
				<ul class="dropdown-menu" role="menu">
				  <li><a href="/admin/upload">アップロード</a></li>
				  <li><a href="/admin/download/employees">ダウンロード</a></li>
				</ul>
			  </li>
          @endif
        </ul>

        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              表示件数
              <span class="caret"></span>
            </a>

		<ul class="dropdown-menu" role="menu">
			<li><a class="page_count">10</a></li>
			<li><a class="page_count">20</a></li>
			<li><a class="page_count">30</a></li>
			<li><a class="page_count">40</a></li>
			<li><a class="page_count">60</a></li>
			<li><a class="page_count">80</a></li>
			<li><a class="page_count">100</a></li>
			<li><a class="page_count">120</a></li>
			<li><a class="page_count">140</a></li>
		</ul>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <!-- ドロップダウンメニュー -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              {{ Auth::user()->last_name }} {{ Auth::user()->first_name }}
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">

              @if (Auth::user()->haveSystemAdministratorRole())
              <li><a class="change_mode">モックモードに変更</a></li>
              @endif

              <li><a href="/admin/user/password/reset">パスワード再設定</a></li>
              <li><a href="/auth/logout">ログアウト</a></li>
            </ul>
          </li>
        </ul>

		<!-- ヘルプ -->
		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
					ヘルプ
					<span class="caret"></span>
				</a>

				<ul class="dropdown-menu" role="menu">
					
					<li><a href="/help/card_reader.html" target="help">社員証での勤怠の記録</a></li>
					<li><a href="/help/change_password.html" target="help">パスワードの変更</a></li>
					<li><a href="/help/change_password_by_mail.html" target="help">パスワードの再設定(メール)</a></li>
					@if (Auth::user()->haveNormalRole())
					<li><a href="/help/edit_attendance_info.html" target="help">勤怠情報の編集(一般社員)</a></li>
					@endif
					<li><a href="/help/show_attendance.html" target="help">勤怠記録の閲覧</a></li>
				</ul>
			</li>
		</ul>
		
        @else

        <ul class="nav navbar-nav">
          <h4 style="color:#FFF;vertical-align: middle;padding-top: 5px;">STK 勤怠管理システム</h4>
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li><a href="/password/email">パスワード再設定</a></li>
        </ul>



		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
					ヘルプ
					<span class="caret"></span>
				</a>

				<ul class="dropdown-menu" role="menu">
					<li><a href="/help/change_password_by_mail.html" target="help">パスワードの再設定(メール)</a></li>					
				</ul>
			</li>
		</ul>


        @endif

      </div><!-- /.navbar-collapse -->
    </nav>
  </div><!-- /.container-fluid -->
</nav>
