<!DOCTYPE HTML>
<html lang="ja">
	<head>
		<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=no">
		<title>STK 勤怠管理システム</title>

	    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
	    <script src="/js/datepicker-ja.js"></script>
	</head>
	<style>

		html {
			position: relative;
			min-height: 100%;
		}

		body {
			/* Margin bottom by footer height */
			margin-bottom: 60px;
		}

		#wrap {
			min-height: 100%;
			height: auto;
			/* フッターの高さ分だけ、ネガティブインデントを指定 */
			margin: 0 auto -30px;
			/* フッターの高さ分だけ、パディングを指定 */
			padding: 20px 0 30px;
		}

		.footer {
			position: absolute;
			bottom: 0;
			width: 100%;
			/* Set the fixed height of the footer here */
			height: 60px;
		}

		.img-responsive-overwrite{
			margin: 0 auto;
		}
	</style>
	<link rel="stylesheet" href="/css/bootstrap.css">
	<body>

		{{-- ナビゲーションバーの Partial を使用 --}}
		@include('navbar')
		<div id="wrap">
			<div class="container">
				@yield('content')
			</div>
		</div>
		<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"-->



		<footer class="footer">
			<div class="container-block">
				<img src="/images/icon.png" class="img-responsive img-responsive-overwrite"/>
				<p class="text-center">START TODAY ENGINEERING Co.,Ltd.</p>
			</div>
		</footer>
	</body>
</html>