<?php
//暗号化方式をRIJNDAELの128bitブロック長、暗号化方式をCBC
$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);  

//Initial Vectorの大きさ
echo "iv size:" . $iv_size . "\n";
// iv size:16

//Initial Vector生成
$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
echo "iv orign value:" . $iv . "\n";
echo "iv value:" . base64_encode( $iv ) . "\n";
// iv value:bj4OWQybnPsIVKBEWcK75A==

//共通鍵
$key = "VNS5HS6D6C6N9BQ4X4T2L28YE3AWHA5Q";

//暗号化したい平文
$text = "felica_id=12345678901234567890&type=in&hash=185BBC0854EC039FEC0D9ACCAAD149AE8C740D8AD51A6871F094B983F5D66548";
echo "PlainText Value:" . $text . "\n";
// PlainText Value:This is plain text

//暗号化方式をRIJNDAELの128bitブロック長、暗号化方式をCBC
$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv);
echo "Crypt Value:" . base64_encode( $crypttext ) . "\n";
// Crypt Value:mdJXSTJvugvrYpDSlpQcQEbIbjFVyGiVtiu/rZYz28s=

echo "Request Value:" . base64_encode( $iv ) . "@@@@@" . base64_encode( $crypttext ) . "\n";
